/********************************************************************************
** Form generated from reading UI file 'clock.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLOCK_H
#define UI_CLOCK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Clock
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *lbl_time;
    QLabel *lbl_date;

    void setupUi(QWidget *Clock)
    {
        if (Clock->objectName().isEmpty())
            Clock->setObjectName(QStringLiteral("Clock"));
        Clock->resize(150, 40);
        Clock->setMaximumSize(QSize(150, 40));
        verticalLayout = new QVBoxLayout(Clock);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        lbl_time = new QLabel(Clock);
        lbl_time->setObjectName(QStringLiteral("lbl_time"));
        QFont font;
        font.setFamily(QStringLiteral("Ubuntu Mono"));
        font.setPointSize(11);
        lbl_time->setFont(font);
        lbl_time->setFrameShape(QFrame::NoFrame);
        lbl_time->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout->addWidget(lbl_time);

        lbl_date = new QLabel(Clock);
        lbl_date->setObjectName(QStringLiteral("lbl_date"));
        lbl_date->setFont(font);
        lbl_date->setFrameShape(QFrame::NoFrame);
        lbl_date->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout->addWidget(lbl_date);


        retranslateUi(Clock);

        QMetaObject::connectSlotsByName(Clock);
    } // setupUi

    void retranslateUi(QWidget *Clock)
    {
        Clock->setWindowTitle(QApplication::translate("Clock", "Form", nullptr));
        lbl_time->setText(QApplication::translate("Clock", "{time}", nullptr));
        lbl_date->setText(QApplication::translate("Clock", "{date}", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Clock: public Ui_Clock {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLOCK_H
