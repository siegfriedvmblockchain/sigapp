#include "mainwindow.h"
#include <QFontDatabase>
#include <QGridLayout>
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{


    QFontDatabase::addApplicationFont(":/ttf/fonts/OpenSans-Regular.ttf");

    QFontDatabase::addApplicationFont(":/ttf/fonts/OpenSans-Semibold.ttf");

    ThemeLoader(":design/style.qss");
    AppConfigInit();
    VMServerInit();

    //AppController initialization

    controller = new AppController(appcfg->Configuration);

    AppControllerInit(AppControllerEnum::StartWallet);

    isMiner=appcfg->Configuration->AppMinerMode;
    connect (appcfg,SIGNAL(ConfigurationError(ErrorMsgEnum,QString*)),this,SLOT(OnConfigError(ErrorMsgEnum,QString*)));




    this->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint |Qt::WindowMinimizeButtonHint|Qt::WindowCloseButtonHint);
    ui->setupUi(this);

    ui->MenuWidget->Init(isMiner);
    hlayout = new QVBoxLayout;
    hlayout->setGeometry(QRect(0,0,720,398));
    hlayout->setContentsMargins(0,0,0,0);
    hlayout->setObjectName("Wlayout");

    if (isMiner){
        miner = new Miner();
        connect(miner,SIGNAL(startNodeAction()),this,SLOT(OnNodeStartedAction()));
        connect(this,SIGNAL(NodeStartedOk()),miner,SLOT(on_NodeStarted()));
        connect(miner,SIGNAL(stopNodeAction()),this,SLOT(OnNodeStopedAction()));
        connect(this,SIGNAL(NodeStopeddOk()),miner,SLOT(on_NodeStoped()));
        ApplyWidget(miner);
    }else{
        perfomance = new Perfomance();
        ApplyWidget(perfomance);
    }

    rancher =  new Rancher();
    settings = new Settings();

    connect(ui->MenuWidget,SIGNAL(menuAction(int)),this,SLOT(OnMenuAction(int)));
}

void MainWindow::OnNodeStopedAction(){

       // controller->~AppController();
    controller->KillNode();
        emit NodeStopeddOk();
        bNodeConnected=false;
}


void MainWindow::OnNodeStartedAction(){
        AppControllerInit();
}

void MainWindow::OnInput(int* cmd){



}
void MainWindow::OnConfigError(ErrorMsgEnum, QString *){

}

void MainWindow::AppControllerInit(AppControllerEnum app){

    auto *args= new AppController::AppArgs();
    args->user=appcfg->Configuration->WalletUser;
    args->password=appcfg->Configuration->WalletPass;
    args->tls=appcfg->Configuration->TLS;
    if (appcfg->Configuration->IsTestNetwork) args->commands<<"testnet";



    switch (app) {
    case AppControllerEnum::StartNode:{
             connect(controller,SIGNAL(onNodeProcessStarted()),this,SLOT(OnNodeProcessStarted()));
        controller->RunNode(args);
    }
        break;
    case AppControllerEnum::StartWallet:{
           connect(controller,SIGNAL(onWalletProcessStarted()),this,SLOT(OnWalletProcessStarted()));
        if (appcfg->Configuration->WalletPath.isEmpty()){

            controller->CreateWallet(args);
            appcfg->Reload();
            qDebug()<<args;
            args->commands.removeLast();
            controller->RunWallet(args);

        }else{
            controller->RunWallet(args);
        }
    }
        break;
    default:
        break;
    }



}
void MainWindow::AppConfigInit(){
    //gets application configuration from ini file
    appcfg= new AppConfig(this);
}
void MainWindow::ThemeLoader(QString theme){
    QFile f(theme);
    if (!f.exists())
    {
        qDebug()<< "Unable to set stylesheet, file not found\n";
    }
    else
    {
        f.open(QFile::ReadOnly | QFile::Text);
        QTextStream ts(&f);
        qApp->setStyleSheet(ts.readAll());
        qDebug()<< QString("Theme %1 loaded correctly").arg(theme.split('/')[1]);
    }
}
void MainWindow::VMServerInit(){

    QString host=QString("%1:%2").arg(appcfg->Configuration->VMHost).arg(appcfg->Configuration->VMPort);
    qDebug()<<"vm host:"<<host;
    //starts vm rpc server
    vmServer = new SigVmServer(host);

    vmserver_thread = new QThread;
    vmServer->moveToThread(vmserver_thread);
    QObject::connect(vmserver_thread, SIGNAL(started()), vmServer, SLOT(process()));
    QObject::connect(vmserver_thread, SIGNAL(destroyed()), vmServer, SLOT(stop()));

    vmserver_thread->start();
}
void MainWindow::OnWalletProcessStarted(){
    int pos=controller->walletOutput.length()-1;
    qDebug()<<controller->walletOutput.at(pos);
}
void MainWindow::OnNodeProcessStarted(){
    if (!bNodeConnected){
        emit NodeStartedOk();
        bNodeConnected=true;
    }
    int pos=controller->nodeOutput.length()-1;
    qDebug()<<controller->nodeOutput.at(pos);
}
void MainWindow::OnMenuAction(int act){
    auto val = static_cast<MainMenuActionEnums>(act);


    //TODO: do some action
    switch(val){
    case MainMenuActionEnums::MiningAction:{
        if (hlayout->count()>0)
        {
            qDeleteAll(hlayout->children());
            qDeleteAll(ui->MainWidget->children());

        }
        miner= new Miner();
        ApplyWidget(miner);
        break;
    }

    case MainMenuActionEnums::PerfomanceAction:{

        if (hlayout->count()>0)
        {
            qDeleteAll(hlayout->children());
            qDeleteAll(ui->MainWidget->children());

        }
        perfomance= new Perfomance();
        ApplyWidget(perfomance);
        break;
    }

    case MainMenuActionEnums::WalletAction:{
        if (hlayout->count()>0)
        {
            qDeleteAll(hlayout->children());
            qDeleteAll(ui->MainWidget->children());
        }
        ApplyWidget();
        break;
    }
    case MainMenuActionEnums::SettingsAction:{
        if (hlayout->count()>0)
        {
            qDeleteAll(hlayout->children());
            qDeleteAll(ui->MainWidget->children());
        }
        settings = new Settings();
        ApplyWidget(settings);
        break;
    }
    case MainMenuActionEnums::FaqAction:{
        if (hlayout->count()>0)
        {
            qDeleteAll(hlayout->children());
            qDeleteAll(ui->MainWidget->children());

        }
        rancher = new Rancher();
        ApplyWidget(rancher);
        break;
    }
    default:
        break;
    }
}

void MainWindow::ApplyWidget(QWidget* widget){
    hlayout = new QVBoxLayout;
    hlayout->setGeometry(QRect(0,0,720,398));
    hlayout->setContentsMargins(0,0,0,0);
    hlayout->setObjectName("Wlayout");
    if (widget!=nullptr)
    {
        qDebug()<<widget->accessibleName();
        hlayout->insertWidget(0,widget);
        ui->MainWidget->setLayout(hlayout);
    }
}
MainWindow::~MainWindow()
{
    vmserver_thread->exit();
    delete ui;
    delete controller;


}

