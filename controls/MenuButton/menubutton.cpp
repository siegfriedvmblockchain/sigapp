 #include "menubutton.h"
#include <QPainter>
#include<QDebug>
MenuButton::MenuButton(QWidget* parent) : QPushButton(parent)
{
}

MenuButton::~MenuButton()
{
}

QSize MenuButton::sizeHint() const
{
    const auto parentHint = QPushButton::sizeHint();
    // add margins here if needed
    return QSize(parentHint.width() + pixmap.width(), std::max(parentHint.height(), pixmap.height()));
}

void MenuButton::setPixmap(const QPixmap& _pixmap)
{
    pixmap = _pixmap;
}
void MenuButton::setIcon(const QIcon& _icon)
{
    icon = _icon;
}
void MenuButton::setText(const QString &_text){

    text=_text;
}
void MenuButton::paintEvent(QPaintEvent* e)
{
    QPushButton::paintEvent(e);


    if (!icon.isNull()){
        pixmap = icon.pixmap(icon.actualSize(QSize(32, 32)));
    }
    if (!pixmap.isNull())
    {
        const int y = (height() - pixmap.height()) / 2; // add margin if needed
        QPainter painter(this);
        painter.drawPixmap(16, y, pixmap); // hardcoded horizontal margin

    }

    int f=16+12+pixmap.width();
    QPainter painter(this);
    //some magic
    painter.drawText(f,height()/2+5,text);
}

