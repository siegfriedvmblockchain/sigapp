#ifndef MENUBUTTON_H
#define MENUBUTTON_H
#pragma onc
#include <QPushButton>

class MenuButton : public QPushButton
{
public:
    explicit MenuButton(QWidget* parent = nullptr);
    virtual ~MenuButton();

    void setPixmap(const QPixmap& pixmap);
    void setIcon(const QIcon& icon);
    void setText(const QString& text="SIGButton");
    virtual QSize sizeHint() const override;

protected:
    virtual void paintEvent(QPaintEvent* e) override;

private:
    QPixmap pixmap;
    QIcon icon;
    QString text;

};

#endif // MENUBUTTON_H
