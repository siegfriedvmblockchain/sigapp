#include "switch.h"

Switch::Switch(QWidget *parent) : QAbstractButton(parent),
    _height(25),
    _opacity(0.000),
    _switch(true),
    _margin(0),
    _thumb("#df3a36"),
    _anim(new QPropertyAnimation(this, "offset", this))
{
    setOffset(_height / 2);
    _y = _height / 2;
    setBrush(QColor("#df3a36"));

}

Switch::Switch(const QBrush &brush, QWidget *parent) : QAbstractButton(parent),
    _height(25),
    _switch(false),
    _opacity(0.000),
    _margin(0),
    _thumb("#df3a36"),
    _anim(new QPropertyAnimation(this, "offset", this))
{
    setOffset(_height / 2);
  //  setOffset(40 - _height);
    //  _y = _height / 2;
    setBrush(brush);

}

void Switch::paintEvent(QPaintEvent *e) {
    QPainter p(this);
    p.setPen(Qt::NoPen);
    p.setFont(QFont("times",9,QFont::Bold));
    if (isEnabled()) {
        p.setBrush(_switch ? brush() : Qt::black);
        p.setOpacity(_switch ? 0.5 : 0.38);
        p.setBrush(QColor("#e7e7e7"));
        p.setRenderHint(QPainter::Antialiasing, true);
        p.drawRoundedRect(QRect(_margin, _margin,48 - 2 * _margin, height() ), 1.0,1.0);
        if (state){
            p.setBrush(_thumb);
            p.setOpacity(1.0);
            p.drawRect(QRectF(offset() - (_height / 2), _y - (_height / 2),25, 25));
            p.setPen(QPen(Qt::white));
            p.drawText(QRectF(offset() - (_height / 2-4), _y - (_height / 2)+5,25, 25),"on");
        }else
        {
            p.setBrush(QColor("#a7a7a7"));
            p.setOpacity(1.0);
            p.drawRect(QRectF(offset() - (_height / 2), _y - (_height / 2),25, 25));
            p.setPen(QPen(Qt::white));
            p.drawText(QRectF(offset() - (_height / 2-4), _y - (_height / 2)+5,25, 25),"off");
        }

    } else {

        p.setBrush(Qt::black);
        p.setOpacity(0.12);
        p.drawRoundedRect(QRect(_margin, _margin, 48 - 2 * _margin, height() ), 1.0,1.0);
        p.setOpacity(1.0);
        p.setBrush(QColor("#e7e7e7"));
        p.drawRect(QRectF(offset() - (_height / 2), _y - (_height / 2), 25, 25));
        p.setPen(QPen(Qt::white));
        p.drawText( QRectF(offset() - (_height / 2), _y - (_height / 2), 25, 25),"off");
    }
}

void Switch::mouseReleaseEvent(QMouseEvent *e) {
    if (e->button() & Qt::LeftButton) {
        _switch = _switch ? false : true;
        _thumb = _switch ? _brush : QBrush("#ee3934");
        if (!_switch) {
            _anim->setStartValue(_height / 2);
            _anim->setEndValue(60 - _height);
            _anim->setDuration(120);
            _anim->start();
            state=false;
            emit onStateChanged(state);
            this->repaint();

        } else {
            _anim->setStartValue(offset());
            _anim->setEndValue(_height / 2);
            _anim->setDuration(120);
            _anim->start();

            state=true;
            emit onStateChanged(state);
            this->repaint();
        }
    }
    QAbstractButton::mouseReleaseEvent(e);
}

void Switch::enterEvent(QEvent *e) {
    setCursor(Qt::PointingHandCursor);
    QAbstractButton::enterEvent(e);
}

QSize Switch::sizeHint() const {
    return QSize(2 * (_height + _margin), _height + 2 * _margin);
}
