/********************************************************************************
** Form generated from reading UI file 'wallet.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WALLET_H
#define UI_WALLET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Wallet
{
public:
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_3;
    QGridLayout *gridLayout;
    QLabel *labelB;
    QLabel *labelSIGBalance;
    QLabel *labelA;
    QLabel *labelUSD;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout_2;
    QLabel *labelCurrency;
    QLabel *labelTime;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *WalletRequestButton;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *WalletSendButton;

    void setupUi(QWidget *Wallet)
    {
        if (Wallet->objectName().isEmpty())
            Wallet->setObjectName(QStringLiteral("Wallet"));
        Wallet->resize(720, 72);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Wallet->sizePolicy().hasHeightForWidth());
        Wallet->setSizePolicy(sizePolicy);
        Wallet->setMaximumSize(QSize(720, 300));
        Wallet->setStyleSheet(QStringLiteral(""));
        horizontalLayoutWidget_2 = new QWidget(Wallet);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(0, 0, 721, 74));
        horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(0);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetFixedSize);
        gridLayout->setContentsMargins(16, 5, -1, 0);
        labelB = new QLabel(horizontalLayoutWidget_2);
        labelB->setObjectName(QStringLiteral("labelB"));
        QFont font;
        font.setFamily(QStringLiteral("Arial"));
        font.setPointSize(11);
        labelB->setFont(font);
        labelB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        labelB->setMargin(0);

        gridLayout->addWidget(labelB, 1, 1, 1, 1);

        labelSIGBalance = new QLabel(horizontalLayoutWidget_2);
        labelSIGBalance->setObjectName(QStringLiteral("labelSIGBalance"));
        labelSIGBalance->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);
        labelSIGBalance->setMargin(0);

        gridLayout->addWidget(labelSIGBalance, 0, 2, 1, 1);

        labelA = new QLabel(horizontalLayoutWidget_2);
        labelA->setObjectName(QStringLiteral("labelA"));
        labelA->setFont(font);
        labelA->setAlignment(Qt::AlignCenter);
        labelA->setMargin(0);

        gridLayout->addWidget(labelA, 0, 1, 1, 1);

        labelUSD = new QLabel(horizontalLayoutWidget_2);
        labelUSD->setObjectName(QStringLiteral("labelUSD"));
        QFont font1;
        font1.setKerning(true);
        labelUSD->setFont(font1);
        labelUSD->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        labelUSD->setMargin(0);

        gridLayout->addWidget(labelUSD, 1, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(2, 30, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_2, 1, 0, 1, 1);


        horizontalLayout_3->addLayout(gridLayout);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetFixedSize);
        gridLayout_2->setHorizontalSpacing(2);
        gridLayout_2->setVerticalSpacing(0);
        gridLayout_2->setContentsMargins(16, 5, 80, 5);
        labelCurrency = new QLabel(horizontalLayoutWidget_2);
        labelCurrency->setObjectName(QStringLiteral("labelCurrency"));
        labelCurrency->setFont(font);
        labelCurrency->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        labelCurrency->setMargin(0);

        gridLayout_2->addWidget(labelCurrency, 0, 1, 1, 1);

        labelTime = new QLabel(horizontalLayoutWidget_2);
        labelTime->setObjectName(QStringLiteral("labelTime"));
        labelTime->setFont(font);
        labelTime->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        labelTime->setMargin(0);

        gridLayout_2->addWidget(labelTime, 1, 1, 1, 1);

        verticalSpacer_3 = new QSpacerItem(2, 30, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer_3, 1, 0, 1, 1);


        horizontalLayout_3->addLayout(gridLayout_2);

        verticalSpacer = new QSpacerItem(1, 72, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_3->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(0);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, 0, 16, 0);
        WalletRequestButton = new QPushButton(horizontalLayoutWidget_2);
        WalletRequestButton->setObjectName(QStringLiteral("WalletRequestButton"));
        QFont font2;
        font2.setFamily(QStringLiteral("Narkisim"));
        WalletRequestButton->setFont(font2);
        WalletRequestButton->setFlat(true);

        horizontalLayout->addWidget(WalletRequestButton);

        horizontalSpacer_2 = new QSpacerItem(16, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        WalletSendButton = new QPushButton(horizontalLayoutWidget_2);
        WalletSendButton->setObjectName(QStringLiteral("WalletSendButton"));
        QFont font3;
        font3.setFamily(QStringLiteral("Nyala"));
        WalletSendButton->setFont(font3);
        WalletSendButton->setFlat(true);

        horizontalLayout->addWidget(WalletSendButton);


        horizontalLayout_3->addLayout(horizontalLayout);


        retranslateUi(Wallet);

        QMetaObject::connectSlotsByName(Wallet);
    } // setupUi

    void retranslateUi(QWidget *Wallet)
    {
        Wallet->setWindowTitle(QApplication::translate("Wallet", "Form", nullptr));
        labelB->setText(QApplication::translate("Wallet", "USD:", nullptr));
        labelSIGBalance->setText(QApplication::translate("Wallet", "--", nullptr));
        labelA->setText(QApplication::translate("Wallet", "SIG balance:", nullptr));
        labelUSD->setText(QApplication::translate("Wallet", "--", nullptr));
        labelCurrency->setText(QApplication::translate("Wallet", "1 SIG = 123.4 USD", nullptr));
        labelTime->setText(QApplication::translate("Wallet", "{Time}", nullptr));
        WalletRequestButton->setText(QApplication::translate("Wallet", "Request", nullptr));
        WalletSendButton->setText(QApplication::translate("Wallet", "Send", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Wallet: public Ui_Wallet {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WALLET_H
