#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QThread>
#include <QMainWindow>
#include <QVBoxLayout>
#include <controller/appcontroller.h>
#include <widgets/Miner/miner.h>
#include <widgets/Rancher/rancher.h>
#include <widgets/Clock/clock.h>
#include <widgets/Settings/settings.h>
#include <widgets/Perfomance/perfomance.h>
#include <network/vmserver/sigvmserver.h>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum MainMenuActionEnums{MiningAction,PerfomanceAction,WalletAction,SettingsAction,FaqAction};
    enum AppControllerEnum{StartNode,StartWallet};

private:
    Ui::MainWindow *ui;
    Miner *miner;
    Perfomance *perfomance;
    Rancher *rancher;
    Settings *settings;
    QVBoxLayout *hlayout;
    AppConfig *appcfg;
    AppController *controller;
    SigVmServer *vmServer;
    QThread *vmserver_thread;
    bool bNodeConnected=false;
    bool isMiner=false;
    void ApplyWidget(QWidget* widget=nullptr);
    void ThemeLoader(QString theme);
    void AppConfigInit();
    void VMServerInit();
    void AppControllerInit(AppControllerEnum app=StartNode);
private slots:
     void OnMenuAction(int act);
     void OnConfigError(ErrorMsgEnum,QString*);
     void OnWalletProcessStarted();
     void OnNodeProcessStarted();
     void OnInput(int*);
     void OnNodeStartedAction();
     void OnNodeStopedAction();

signals:
    void NodeStartedOk();
    void NodeStopeddOk();
};

#endif // MAINWINDOW_H
