#include "appcontroller.h"
AppController::AppController(SIGConfig *cfg):
appcfg(cfg)
{

}
void AppController::CreateWallet(AppArgs *arg)
{

    arg->commands<<"create";
    QString program= ProrgamStartCmd(AppEnum::WalletApp,arg);
    if(program.isNull()) {
        qDebug()<<"Not found";
    }else{
      system(program.toStdString().c_str());
    }
}

QString AppController::ProrgamStartCmd(AppEnum app,AppArgs *args){
    QString appArgs;
    if (args->password!=nullptr && args->user!=nullptr){
        appArgs=QString("-u %1 -P %2 ").arg(args->user).arg(args->password);
    }
 if(!args->commands.isEmpty()){
//deprecated
//    if(!args->tls){
//        switch  (app){
//        case AppEnum::NodeApp:{
//            args->commands.removeOne("noservertls");
//            args->commands<<"notls";
//            break;
//        }
//        case AppEnum::WalletApp:{
//           args->commands.removeOne("notls");
//         //   args->commands<<"noservertls";

//            break;
//        }
//        }
//    }

    foreach (QString itm, args->commands) {
       appArgs.append(QString(" --%1").arg(itm));
    }
 }
    QString re=GetFile(app);
    if (re.isEmpty()) return nullptr;

    QString program=QDir::toNativeSeparators(QString("%1%2%3 %4").arg(appcfg->AppPath).arg(QDir::separator()).arg(re));
    program = program.arg(appArgs);
    return program;
}
QString AppController::GetFile(AppEnum app){
    QDir *directory = new QDir;
    directory->setFilter(QDir::Files);
    directory->setCurrent(appcfg->AppPath);
    directory->setNameFilters({GetAppName(app)});
    QStringList res= directory->entryList();
    int idx;
    switch(app){
    case AppEnum::WalletApp:
    {
#if defined(Q_OS_WIN32)
        auto  ep=QString("^%1.+").arg(SIGAPP_WALLET);
#elif defined(Q_OS_LINUX)
        auto  ep=QString("^%1").arg(SIGAPP_WALLET);
#endif
        qDebug()<< ep;
        idx=res.indexOf(QRegExp(ep));
                qDebug()<< idx;
        break;
    }
    case AppEnum::NodeApp:
    {
#if defined(Q_OS_WIN32)
        auto  ep=QString("^%1.+").arg(SIGAPP_NODE);
#elif defined(Q_OS_LINUX)
        auto  ep=QString("^%1").arg(SIGAPP_NODE);
#endif
                qDebug()<< ep;
        idx=res.indexOf(QRegExp(ep));
                qDebug()<< idx;
        break;
    }
    }
    if (idx<0) return "";
    return res.at(idx);
}

void AppController::RunWallet(AppArgs *arg){
    QString program= ProrgamStartCmd(WalletApp,arg);
    if(program.isNull()) {
        qDebug()<<"Not found";
    }else{
        if (walletProcess)
        {
            walletProcess->setEnvironment( QProcess::systemEnvironment() );
            walletProcess->setProcessChannelMode(QProcess::MergedChannels);

            walletProcess->start(program, QProcess::Unbuffered | QProcess::ReadWrite);
            walletProcess->waitForStarted();
           connect( walletProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(WalletReadOut()) );
           connect( walletProcess, SIGNAL(readyReadStandardError()), this, SLOT(WalletReadErr()) );//

        }
    }

}
void AppController::RunNode(AppArgs *arg){
    QString program= ProrgamStartCmd(NodeApp,arg);
    if(program.isNull()) {
        qDebug()<<"Not found";
    }else{
        if (nodeProcess)
        {
            nodeProcess->setEnvironment( QProcess::systemEnvironment() );
            nodeProcess->setProcessChannelMode(QProcess::MergedChannels);

            nodeProcess->start(program, QProcess::Unbuffered | QProcess::ReadWrite);
            nodeProcess->waitForStarted();
           connect( nodeProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(NodeReadOut()) );
           connect( nodeProcess, SIGNAL(readyReadStandardError()), this, SLOT(NodeReadErr()) );//

        }
    }


}
void AppController::NodeReadOut(){
  QProcess *p = dynamic_cast<QProcess *>( sender() );
 QString msg=nodeProcess->readAllStandardOutput();
    if (p){

        nodeOutput.append(msg); // p->readAllStandardError()

    if(msg.contains("Enter private passphrase"))   {

                p->write("12345\n");
    }




      emit onNodeProcessStarted();


    }

//p->close();

}
void AppController::NodeReadErr(){
    QProcess *p = dynamic_cast<QProcess *>( sender() );

    if (p)
        qDebug()<< p->readAllStandardError();//p->readAllStandardOutput();

}
void AppController::WalletReadOut(){
  QProcess *p = dynamic_cast<QProcess *>( sender() );
 QString msg=walletProcess->readAllStandardOutput();
    if (p){

        walletOutput.append(msg); // p->readAllStandardError()

    if(msg.contains("Enter private passphrase"))   {

                p->write("12345\n");
    }




      emit onWalletProcessStarted();


    }

//p->close();

}

void AppController::WalletReadErr(){
    QProcess *p = dynamic_cast<QProcess *>( sender() );

    if (p)
        qDebug()<< p->readAllStandardError();//p->readAllStandardOutput();

}
QString AppController::GetAppName(AppEnum app){
    QString AppMask;
#if defined(Q_OS_WIN32)
    AppMask="%1.exe";
#elif defined(Q_OS_MACX)
    AppMask="%1";
#elif defined(Q_OS_LINUX)
    AppMask="%1";
#endif
    QString appName=AppNames[app];
    QString runName=AppMask.arg(appName);
    qDebug()<<runName;
    return runName;
}
bool AppController::CheckInstallPath(){

    QString curName=QApplication::applicationDisplayName();
    QString path = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation).at(0);

    QString userdir=QDir::toNativeSeparators(path.remove(curName));
    foreach (QString itm , AppNames) {
        QString appPath= QString("%1%2%3").arg(userdir).arg(itm).arg(QDir::separator());
        QDir dir(path);
        if (!dir.exists()) {
            return false;
        };

        Path.append(appPath);
        continue;
    }

    qDebug()<<Path;
    return true;
}
void AppController::KillWallet(){
    if (walletProcess->state() != QProcess::ProcessState::NotRunning){
    if (walletProcess->isOpen()){
        walletProcess->kill();
        walletProcess->waitForFinished(-1);
        QString pid=walletProcess->program();

        QString cmd;

    #if defined( Q_OS_WIN32 )
        cmd=QString("process -k %1").arg(pid);
        qDebug()<<cmd;
    #elif (defined( Q_OS_MACX ))
        cmd=QString("pkill %1").arg(pid);
    #elif  (defined(Q_OS_LINUX))
        cmd=QString("pkill %1").arg(pid);
        qDebug()<<"cmd:"<<cmd;
    #endif
        system(cmd.toStdString().c_str());
     qDebug()<<"kill wallet state:"<<walletProcess->state();
    }
    }
}
void AppController::KillNode(){
    if (nodeProcess->state() != QProcess::ProcessState::NotRunning){
    if (nodeProcess->isOpen()){
        nodeProcess->kill();
        nodeProcess->waitForFinished(-1);

        QString pid=nodeProcess->program();

        QString cmd;

    #if defined(Q_OS_WIN32)
        cmd=QString("process -k %1").arg(pid);
        qDebug()<<cmd;
    #elif defined(Q_OS_MACX)
        cmd=QString("pkill %1").arg(pid);
    #elif   defined(Q_OS_LINUX)
        cmd=QString("pkill %1").arg(pid);
        qDebug()<<"cmd:"<<cmd;
    #endif
        system(cmd.toStdString().c_str());
        qDebug()<<"kill node state:"<<nodeProcess->state();
    }
    }
}
AppController::~AppController(){
    KillNode();
    KillWallet();
}
