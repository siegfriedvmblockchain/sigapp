#ifndef APPCONTROLLER_H
#define APPCONTROLLER_H

#include <QObject>
#include <QProcess>
#include <QApplication>
#include <QDir>
#include <QStandardPaths>
#include <QString>
#include <QList>
#include <QDebug>
#include <QSysInfo>
#include <QMetaEnum>
#include <QDirIterator>
#include "default.h"
class AppController: public QObject
{
    Q_OBJECT
public:
   enum AppEnum {NodeApp,WalletApp};
   struct AppArgs{
       QString user;
       QString password;
       bool tls;
       QStringList commands{};
   };
    AppController(SIGConfig *cfg);

   ~AppController();


 void CreateWallet(AppArgs *arg);
 void RunWallet(AppArgs *arg);
 void KillWallet();
 void RunNode(AppArgs *arg);
 void KillNode();
    QStringList nodeOutput;
        QStringList walletOutput;
private:
 QList<QString> AppNames{"dcrd","dcrwallet"};
  bool CheckInstallPath();
  QString GetAppName(AppEnum);
 AppEnum currentApp;
 QList<QString> Path;
 QProcess *nodeProcess=new QProcess(this);
  QProcess *walletProcess=new QProcess(this);
 SIGConfig *appcfg;
 QString ProrgamStartCmd(AppEnum app,AppArgs*);
  QString GetFile(AppEnum fn);


signals:
 void onWalletProcessStarted();
  void onNodeProcessStarted();
private slots:
 void WalletReadOut();
 void WalletReadErr();
 void NodeReadOut();
 void NodeReadErr();
};

#endif // APPCONTROLLER_H
