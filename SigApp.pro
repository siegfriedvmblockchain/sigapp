#-------------------------------------------------
#
# Project created by QtCreator 2018-03-14T10:15:52
#
#-------------------------------------------------

QT       += core gui  websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = SIGApp
TEMPLATE = app

CONFIG +=   c++11
CONFIG +=   link_pkgconfig

CONFIG(debug, release|debug):DEFINES += _DEBUG
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH+= $$PWD/include/ \
        $$PWD/widgets/Clock/ \
        $$PWD/widgets/Wallet/ \
        $$PWD/widgets/Rancher/ \
        $$PWD/widgets/MainMenu/ \
        $$PWD/widgets/Miner/ \
        $$PWD/widgets/Perfomance/ \
        $$PWD/controls/MenuButton/ \
        $$PWD/controls/Switch/ \



RESOURCES +=  $$PWD/design/style.qrc \

win32:VERSION = 0.1.0.0 # major.minor.patch.build
else:VERSION = 0.1.0    # major.minor.patch


android {
    androidmanifestupdate.commands =  sed -i \'\' -E -e \'s/(versionName=)(\"([0-9]\.?)+\")/\\1\"$$VERSION\"/g\' $$ANDROID_PACKAGE_SOURCE_DIR/AndroidManifest.xml
    QMAKE_EXTRA_TARGETS += androidmanifestupdate
    PRE_TARGETDEPS += androidmanifestupdate
}

ios {
    plistupdate.commands = /usr/libexec/PlistBuddy -c \"Set :CFBundleShortVersionString $$VERSION\" $$QMAKE_INFO_PLIST
    QMAKE_EXTRA_TARGETS += plistupdate
    PRE_TARGETDEPS += plistupdate
}
unix: PKGCONFIG += grpc++ protobuf

win32: LIBS += -L$$PWD/lib/ -llibprotobuf
unix:!macx: LIBS += -L$$PWD/lib/ -lprotobuf

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

unix:!macx: PRE_TARGETDEPS += $$PWD/lib/libprotobuf.a

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/libprotobuf.lib

win32: LIBS += -L$$PWD/lib/ -lgpr

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/gpr.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgpr.a

win32: LIBS += -L$$PWD/lib/ -lzlib

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/zlib.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libzlib.a

win32: LIBS += -L$$PWD/lib/ -lcares

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/cares.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libcares.a

win32: LIBS += -L$$PWD/lib/ -lgrpc_cronet

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc_cronet.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc_cronet.a

win32: LIBS += -L$$PWD/lib/ -lgrpc++_cronet

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc++_cronet.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc++_cronet.a

win32: LIBS += -L$$PWD/lib/ -lcrypto

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/crypto.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libcrypto.a

win32: LIBS += -L$$PWD/lib/ -lssl

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/ssl.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libssl.a

win32: LIBS += -L$$PWD/lib/ -lrand_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/rand_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/librand_extra.a

win32: LIBS += -L$$PWD/lib/ -lbn_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/bn_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libbn_extra.a

win32: LIBS += -L$$PWD/lib/ -lchacha

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/chacha.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libchacha.a

win32: LIBS += -L$$PWD/lib/ -lcurve25519

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/curve25519.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libcurve25519.a

win32: LIBS += -L$$PWD/lib/ -lpem

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/pem.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libpem.a

win32: LIBS += -L$$PWD/lib/ -lbase64

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/base64.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libbase64.a

win32: LIBS += -L$$PWD/lib/ -lcmac

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/cmac.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libcmac.a

win32: LIBS += -L$$PWD/lib/ -lcrypto_base

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/crypto_base.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libcrypto_base.a

win32: LIBS += -L$$PWD/lib/ -lbuf

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/buf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libbuf.a

win32: LIBS += -L$$PWD/lib/ -lpkcs7

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/pkcs7.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libpkcs7.a

win32: LIBS += -L$$PWD/lib/ -lx509

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/x509.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libx509.a

win32: LIBS += -L$$PWD/lib/ -lengine

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32: LIBS += -L$$PWD/lib/ -lbytestring

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32: LIBS += -L$$PWD/lib/ -ldigest_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/digest_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libdigest_extra.a

win32: LIBS += -L$$PWD/lib/ -lpoly1305

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/poly1305.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libpoly1305.a

win32: LIBS += -L$$PWD/lib/ -lobj

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/obj.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libobj.a

win32: LIBS += -L$$PWD/lib/ -lec_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/ec_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libec_extra.a

win32: LIBS += -L$$PWD/lib/ -lpkcs8_lib

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/pkcs8_lib.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libpkcs8_lib.a

win32: LIBS += -L$$PWD/lib/ -lrc4

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/rc4.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/librc4.a

win32: LIBS += -L$$PWD/lib/ -lerr

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/err.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/liberr.a

win32: LIBS += -L$$PWD/lib/ -lecdsa_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32: LIBS += -L$$PWD/lib/ -levp

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/evp.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libevp.a


win32: LIBS += -L$$PWD/lib/ -ldsa

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/dsa.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libdsa.a

win32: LIBS += -L$$PWD/lib/ -lpool

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/pool.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libpool.a

win32: LIBS += -L$$PWD/lib/ -ldh

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/dh.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libdh.a

win32: LIBS += -L$$PWD/lib/ -lecdh

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/ecdh.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libecdh.a

win32: LIBS += -L$$PWD/lib/ -lbio

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/bio.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libbio.a

win32: LIBS += -L$$PWD/lib/ -lhkdf

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/hkdf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libhkdf.a

win32: LIBS += -L$$PWD/lib/ -lfipsmodule

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/fipsmodule.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libfipsmodule.a

win32: LIBS += -L$$PWD/lib/ -lconf

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/conf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libconf.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/lib/ -lrsa_extra
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/lib/ -lrsa_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/librsa_extra.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/librsa_extra.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/lib/rsa_extra.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/lib/rsa_extra.lib

win32: LIBS += -L$$PWD/lib/ -llhash

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/lhash.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/liblhash.a

win32: LIBS += -L$$PWD/lib/ -lx509v3

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/x509v3.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libx509v3.a

win32: LIBS += -L$$PWD/lib/ -lstack

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/stack.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libstack.a

win32: LIBS += -L$$PWD/lib/ -lcipher_extra

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/cipher_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libcipher_extra.a

win32: LIBS += -L$$PWD/lib/ -lasn1

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/asn1.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libasn1.a

win32: LIBS += -L$$PWD/lib/ -lgrpc++

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc++.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc++.a

win32: LIBS += -L$$PWD/lib/ -lgrpc
INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc.a


win32: LIBS += -L$$PWD/lib/ -lgrpc_unsecure

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc_unsecure.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc_unsecure.a

win32: LIBS += -L$$PWD/lib/ -lgrpc++_unsecure

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc++_unsecure.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc++_unsecure.a

win32: LIBS += -L$$PWD/lib/ -lgrpc++_error_details

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc++_error_details.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc++_error_details.a

win32: LIBS += -L$$PWD/lib/ -lgrpc++_reflection

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc++_reflection.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc++_reflection.a

win32: LIBS += -L$$PWD/lib/ -lgrpc_plugin_support

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/grpc_plugin_support.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libgrpc_plugin_support.a

#win32: LIBS += -L$$PWD/lib/ -lfipsmodule

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/fipsmodule.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libfipsmodule.a

#win32: LIBS += -L$$PWD/lib/ -llibprotoc

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/libprotoc.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/liblibprotoc.a

#win32: LIBS += -L$$PWD/lib/ -lzlibstatic

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/zlibstatic.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libzlibstatic.a

#win32: LIBS += -L$$PWD/lib/ -ldecrepit

#INCLUDEPATH += $$PWD/include
#DEPENDPATH += $$PWD/include

#win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/decrepit.lib
#else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libdecrepit.a

win32: LIBS += -L$$PWD/lib/ -lAdvAPI32

INCLUDEPATH += $$PWD/include
DEPENDPATH += $$PWD/include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/lib/AdvAPI32.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/lib/libAdvAPI32.a
##end Win32

HEADERS += \
    include/google/protobuf/compiler/cpp/cpp_generator.h \
    include/google/protobuf/compiler/csharp/csharp_generator.h \
    include/google/protobuf/compiler/csharp/csharp_names.h \
    include/google/protobuf/compiler/java/java_generator.h \
    include/google/protobuf/compiler/java/java_names.h \
    include/google/protobuf/compiler/javanano/javanano_generator.h \
    include/google/protobuf/compiler/js/js_generator.h \
    include/google/protobuf/compiler/js/well_known_types_embed.h \
    include/google/protobuf/compiler/objectivec/objectivec_generator.h \
    include/google/protobuf/compiler/objectivec/objectivec_helpers.h \
    include/google/protobuf/compiler/php/php_generator.h \
    include/google/protobuf/compiler/python/python_generator.h \
    include/google/protobuf/compiler/ruby/ruby_generator.h \
    include/google/protobuf/compiler/code_generator.h \
    include/google/protobuf/compiler/command_line_interface.h \
    include/google/protobuf/compiler/importer.h \
    include/google/protobuf/compiler/parser.h \
    include/google/protobuf/compiler/plugin.h \
    include/google/protobuf/compiler/plugin.pb.h \
    include/google/protobuf/io/coded_stream.h \
    include/google/protobuf/io/gzip_stream.h \
    include/google/protobuf/io/printer.h \
    include/google/protobuf/io/strtod.h \
    include/google/protobuf/io/tokenizer.h \
    include/google/protobuf/io/zero_copy_stream.h \
    include/google/protobuf/io/zero_copy_stream_impl.h \
    include/google/protobuf/io/zero_copy_stream_impl_lite.h \
    include/google/protobuf/stubs/atomic_sequence_num.h \
    include/google/protobuf/stubs/atomicops.h \
    include/google/protobuf/stubs/atomicops_internals_arm64_gcc.h \
    include/google/protobuf/stubs/atomicops_internals_arm_gcc.h \
    include/google/protobuf/stubs/atomicops_internals_arm_qnx.h \
    include/google/protobuf/stubs/atomicops_internals_generic_c11_atomic.h \
    include/google/protobuf/stubs/atomicops_internals_generic_gcc.h \
    include/google/protobuf/stubs/atomicops_internals_mips_gcc.h \
    include/google/protobuf/stubs/atomicops_internals_power.h \
    include/google/protobuf/stubs/atomicops_internals_ppc_gcc.h \
    include/google/protobuf/stubs/atomicops_internals_solaris.h \
    include/google/protobuf/stubs/atomicops_internals_tsan.h \
    include/google/protobuf/stubs/atomicops_internals_x86_gcc.h \
    include/google/protobuf/stubs/atomicops_internals_x86_msvc.h \
    include/google/protobuf/stubs/bytestream.h \
    include/google/protobuf/stubs/callback.h \
    include/google/protobuf/stubs/casts.h \
    include/google/protobuf/stubs/common.h \
    include/google/protobuf/stubs/fastmem.h \
    include/google/protobuf/stubs/hash.h \
    include/google/protobuf/stubs/logging.h \
    include/google/protobuf/stubs/macros.h \
    include/google/protobuf/stubs/mutex.h \
    include/google/protobuf/stubs/once.h \
    include/google/protobuf/stubs/platform_macros.h \
    include/google/protobuf/stubs/port.h \
    include/google/protobuf/stubs/scoped_ptr.h \
    include/google/protobuf/stubs/shared_ptr.h \
    include/google/protobuf/stubs/singleton.h \
    include/google/protobuf/stubs/status.h \
    include/google/protobuf/stubs/stl_util.h \
    include/google/protobuf/stubs/stringpiece.h \
    include/google/protobuf/stubs/template_util.h \
    include/google/protobuf/stubs/type_traits.h \
    include/google/protobuf/util/delimited_message_util.h \
    include/google/protobuf/util/field_comparator.h \
    include/google/protobuf/util/field_mask_util.h \
    include/google/protobuf/util/json_util.h \
    include/google/protobuf/util/message_differencer.h \
    include/google/protobuf/util/time_util.h \
    include/google/protobuf/util/type_resolver.h \
    include/google/protobuf/util/type_resolver_util.h \
    include/google/protobuf/any.h \
    include/google/protobuf/any.pb.h \
    include/google/protobuf/api.pb.h \
    include/google/protobuf/arena.h \
    include/google/protobuf/arena_impl.h \
    include/google/protobuf/arenastring.h \
    include/google/protobuf/descriptor.h \
    include/google/protobuf/descriptor.pb.h \
    include/google/protobuf/descriptor_database.h \
    include/google/protobuf/duration.pb.h \
    include/google/protobuf/dynamic_message.h \
    include/google/protobuf/empty.pb.h \
    include/google/protobuf/extension_set.h \
    include/google/protobuf/field_mask.pb.h \
    include/google/protobuf/generated_enum_reflection.h \
    include/google/protobuf/generated_enum_util.h \
    include/google/protobuf/generated_message_reflection.h \
    include/google/protobuf/generated_message_table_driven.h \
    include/google/protobuf/generated_message_util.h \
    include/google/protobuf/has_bits.h \
    include/google/protobuf/map.h \
    include/google/protobuf/map_entry.h \
    include/google/protobuf/map_entry_lite.h \
    include/google/protobuf/map_field.h \
    include/google/protobuf/map_field_inl.h \
    include/google/protobuf/map_field_lite.h \
    include/google/protobuf/map_type_handler.h \
    include/google/protobuf/message.h \
    include/google/protobuf/message_lite.h \
    include/google/protobuf/metadata.h \
    include/google/protobuf/metadata_lite.h \
    include/google/protobuf/reflection.h \
    include/google/protobuf/reflection_ops.h \
    include/google/protobuf/repeated_field.h \
    include/google/protobuf/service.h \
    include/google/protobuf/source_context.pb.h \
    include/google/protobuf/struct.pb.h \
    include/google/protobuf/text_format.h \
    include/google/protobuf/timestamp.pb.h \
    include/google/protobuf/type.pb.h \
    include/google/protobuf/unknown_field_set.h \
    include/google/protobuf/wire_format.h \
    include/google/protobuf/wire_format_lite.h \
    include/google/protobuf/wire_format_lite_inl.h \
    include/google/protobuf/wrappers.pb.h \
    include/grpc/impl/codegen/atm.h \
    include/grpc/impl/codegen/atm_gcc_atomic.h \
    include/grpc/impl/codegen/atm_gcc_sync.h \
    include/grpc/impl/codegen/atm_windows.h \
    include/grpc/impl/codegen/byte_buffer.h \
    include/grpc/impl/codegen/byte_buffer_reader.h \
    include/grpc/impl/codegen/compression_types.h \
    include/grpc/impl/codegen/connectivity_state.h \
    include/grpc/impl/codegen/fork.h \
    include/grpc/impl/codegen/gpr_slice.h \
    include/grpc/impl/codegen/gpr_types.h \
    include/grpc/impl/codegen/grpc_types.h \
    include/grpc/impl/codegen/port_platform.h \
    include/grpc/impl/codegen/propagation_bits.h \
    include/grpc/impl/codegen/slice.h \
    include/grpc/impl/codegen/status.h \
    include/grpc/impl/codegen/sync.h \
    include/grpc/impl/codegen/sync_custom.h \
    include/grpc/impl/codegen/sync_generic.h \
    include/grpc/impl/codegen/sync_posix.h \
    include/grpc/impl/codegen/sync_windows.h \
    include/grpc/support/alloc.h \
    include/grpc/support/atm.h \
    include/grpc/support/atm_gcc_atomic.h \
    include/grpc/support/atm_gcc_sync.h \
    include/grpc/support/atm_windows.h \
    include/grpc/support/cpu.h \
    include/grpc/support/log.h \
    include/grpc/support/log_windows.h \
    include/grpc/support/port_platform.h \
    include/grpc/support/string_util.h \
    include/grpc/support/sync.h \
    include/grpc/support/sync_custom.h \
    include/grpc/support/sync_generic.h \
    include/grpc/support/sync_posix.h \
    include/grpc/support/sync_windows.h \
    include/grpc/support/thd_id.h \
    include/grpc/support/time.h \
    include/grpc/support/workaround_list.h \
    include/grpc/byte_buffer.h \
    include/grpc/byte_buffer_reader.h \
    include/grpc/census.h \
    include/grpc/compression.h \
    include/grpc/fork.h \
    include/grpc/grpc.h \
    include/grpc/grpc_cronet.h \
    include/grpc/grpc_posix.h \
    include/grpc/grpc_security.h \
    include/grpc/grpc_security_constants.h \
    include/grpc/load_reporting.h \
    include/grpc/slice.h \
    include/grpc/slice_buffer.h \
    include/grpc/status.h \
    include/grpc++/ext/health_check_service_server_builder_option.h \
    include/grpc++/ext/proto_server_reflection_plugin.h \
    include/grpc++/generic/async_generic_service.h \
    include/grpc++/generic/generic_stub.h \
    include/grpc++/impl/codegen/security/auth_context.h \
    include/grpc++/impl/codegen/async_stream.h \
    include/grpc++/impl/codegen/async_unary_call.h \
    include/grpc++/impl/codegen/byte_buffer.h \
    include/grpc++/impl/codegen/call.h \
    include/grpc++/impl/codegen/call_hook.h \
    include/grpc++/impl/codegen/channel_interface.h \
    include/grpc++/impl/codegen/client_context.h \
    include/grpc++/impl/codegen/client_unary_call.h \
    include/grpc++/impl/codegen/completion_queue.h \
    include/grpc++/impl/codegen/completion_queue_tag.h \
    include/grpc++/impl/codegen/config.h \
    include/grpc++/impl/codegen/config_protobuf.h \
    include/grpc++/impl/codegen/core_codegen.h \
    include/grpc++/impl/codegen/core_codegen_interface.h \
    include/grpc++/impl/codegen/create_auth_context.h \
    include/grpc++/impl/codegen/grpc_library.h \
    include/grpc++/impl/codegen/metadata_map.h \
    include/grpc++/impl/codegen/method_handler_impl.h \
    include/grpc++/impl/codegen/proto_utils.h \
    include/grpc++/impl/codegen/rpc_method.h \
    include/grpc++/impl/codegen/rpc_service_method.h \
    include/grpc++/impl/codegen/serialization_traits.h \
    include/grpc++/impl/codegen/server_context.h \
    include/grpc++/impl/codegen/server_interface.h \
    include/grpc++/impl/codegen/service_type.h \
    include/grpc++/impl/codegen/slice.h \
    include/grpc++/impl/codegen/status.h \
    include/grpc++/impl/codegen/status_code_enum.h \
    include/grpc++/impl/codegen/string_ref.h \
    include/grpc++/impl/codegen/stub_options.h \
    include/grpc++/impl/codegen/sync_stream.h \
    include/grpc++/impl/codegen/time.h \
    include/grpc++/impl/call.h \
    include/grpc++/impl/channel_argument_option.h \
    include/grpc++/impl/client_unary_call.h \
    include/grpc++/impl/grpc_library.h \
    include/grpc++/impl/method_handler_impl.h \
    include/grpc++/impl/rpc_method.h \
    include/grpc++/impl/rpc_service_method.h \
    include/grpc++/impl/serialization_traits.h \
    include/grpc++/impl/server_builder_option.h \
    include/grpc++/impl/server_builder_plugin.h \
    include/grpc++/impl/server_initializer.h \
    include/grpc++/impl/service_type.h \
    include/grpc++/impl/sync_cxx11.h \
    include/grpc++/impl/sync_no_cxx11.h \
    include/grpc++/security/auth_context.h \
    include/grpc++/security/auth_metadata_processor.h \
    include/grpc++/security/credentials.h \
    include/grpc++/security/server_credentials.h \
    include/grpc++/support/async_stream.h \
    include/grpc++/support/async_unary_call.h \
    include/grpc++/support/byte_buffer.h \
    include/grpc++/support/channel_arguments.h \
    include/grpc++/support/config.h \
    include/grpc++/support/error_details.h \
    include/grpc++/support/slice.h \
    include/grpc++/support/status.h \
    include/grpc++/support/status_code_enum.h \
    include/grpc++/support/string_ref.h \
    include/grpc++/support/stub_options.h \
    include/grpc++/support/sync_stream.h \
    include/grpc++/support/time.h \
    include/grpc++/test/mock_stream.h \
    include/grpc++/test/server_context_test_spouse.h \
    include/grpc++/alarm.h \
    include/grpc++/channel.h \
    include/grpc++/client_context.h \
    include/grpc++/completion_queue.h \
    include/grpc++/create_channel.h \
    include/grpc++/create_channel_posix.h \
    include/grpc++/grpc++.h \
    include/grpc++/health_check_service_interface.h \
    include/grpc++/resource_quota.h \
    include/grpc++/server.h \
    include/grpc++/server_builder.h \
    include/grpc++/server_context.h \
    include/grpc++/server_posix.h \
    widgets/Clock/clock.h \
    widgets/Wallet/api/api.pb.h \
    widgets/Wallet/wallet.h \
    mainwindow.h \
    widgets/Wallet/api/api.grpc.pb.h \
    include/default.h \
    api/api_walletrpc.h \
    api/api_wallethttp.h \
    common/appconfig.h \
    api/api_rancher.h \
    common/helper.h \
    include/rapidjson/error/en.h \
    include/rapidjson/error/error.h \
    include/rapidjson/internal/biginteger.h \
    include/rapidjson/internal/diyfp.h \
    include/rapidjson/internal/dtoa.h \
    include/rapidjson/internal/ieee754.h \
    include/rapidjson/internal/itoa.h \
    include/rapidjson/internal/meta.h \
    include/rapidjson/internal/pow10.h \
    include/rapidjson/internal/regex.h \
    include/rapidjson/internal/stack.h \
    include/rapidjson/internal/strfunc.h \
    include/rapidjson/internal/strtod.h \
    include/rapidjson/internal/swap.h \
    include/rapidjson/msinttypes/inttypes.h \
    include/rapidjson/msinttypes/stdint.h \
    include/rapidjson/allocators.h \
    include/rapidjson/cursorstreamwrapper.h \
    include/rapidjson/document.h \
    include/rapidjson/encodedstream.h \
    include/rapidjson/encodings.h \
    include/rapidjson/filereadstream.h \
    include/rapidjson/filewritestream.h \
    include/rapidjson/fwd.h \
    include/rapidjson/istreamwrapper.h \
    include/rapidjson/memorybuffer.h \
    include/rapidjson/memorystream.h \
    include/rapidjson/ostreamwrapper.h \
    include/rapidjson/pointer.h \
    include/rapidjson/prettywriter.h \
    include/rapidjson/rapidjson.h \
    include/rapidjson/reader.h \
    include/rapidjson/schema.h \
    include/rapidjson/stringbuffer.h \
    include/rapidjson/writer.h \
    common/rapidjson_wrapper.h \
    common/timeout.h \
    widgets/Wallet/command.h \
    model/walletdatamodel.h \
    widgets/Rancher/rancher.h \
    model/vmdatamodel.h \
    widgets/Rancher/vm_commands_masks.h \
    widgets/Rancher/vm_commands.h \
    widgets/MainMenu/mmenu.h \
    controls/MenuButton/menubutton.h \
    widgets/Miner/miner.h \
    widgets/Miner/resourcesdialog.h\
    widgets/Settings/settings.h \
    widgets/Perfomance/perfomance.h \
    controls/Switch/switch.h \
    widgets/Miner/addnodedialog.h \
    widgets/Miner/blocksigdetails.h \
    widgets/Wallet/walletrequestdialog.h \
    widgets/Wallet/walletsenddialog.h \
    widgets/Wallet/datastructs.h \
    controller/appcontroller.h \
    model/nodemodel.h \
    network/vmserver/sigvmserver.h \
    network/vmserver/vmserver.grpc.pb.h \
    network/vmserver/vmserver.pb.h \
    network/vmserver/sigvmserverimpl.h \
    network/sighttp.h \
    network/sigrpc.h \
    network/sigws.h \
    network/vmclient/sigvmclient.h \
    common/vmdata.h

SOURCES += \
    widgets/Clock/clock.cpp \
    widgets/Wallet/wallet.cpp \
    main.cpp \
    mainwindow.cpp \
    widgets/Wallet/api/api.grpc.pb.cc \
    widgets/Wallet/api/api.pb.cc \
    api/api_walletrpc.cpp \
    api/api_wallethttp.cpp \
    common/appconfig.cpp \
    api/api_rancher.cpp \
    common/helper.cpp \
    common/timeout.cpp \
    widgets/Wallet/command.cpp \
    widgets/Rancher/rancher.cpp \
    widgets/Rancher/vm_commands.cpp \
    widgets/MainMenu/mmenu.cpp \
    controls/MenuButton/menubutton.cpp \
    widgets/Miner/miner.cpp \
    widgets/Miner/resourcesdialog.cpp \
    widgets/Settings/settings.cpp \
    controls/Switch/switch.cpp\
    widgets/Perfomance/perfomance.cpp \
    widgets/Miner/addnodedialog.cpp \
    widgets/Miner/blocksigdetails.cpp \
    widgets/Wallet/walletrequestdialog.cpp \
    widgets/Wallet/walletsenddialog.cpp \
    controller/appcontroller.cpp \
    network/vmserver/vmserver.grpc.pb.cc \
    network/vmserver/vmserver.pb.cc \
    network/vmserver/sigvmserver.cpp \
    network/vmserver/sigvmserverimpl.cpp \
    network/sighttp.cpp \
    network/sigrpc.cpp \
    network/sigws.cpp \
    network/vmclient/sigvmclient.cpp \
    common/vmdata.cpp




FORMS += \
    widgets/Clock/clock.ui \
    widgets/Wallet/wallet.ui \
    mainwindow.ui \
    widgets/Rancher/rancher.ui \
    widgets/MainMenu/mmenu.ui \
    widgets/Miner/miner.ui \
    widgets/Miner/miner.ui \
    widgets/Miner/resourcesdialog.ui \
    widgets/Perfomance/perfomance.ui \
    widgets/Settings/settings.ui \
    widgets/Miner/addnodedialog.ui \
    widgets/Miner/blocksigdetails.ui \
    widgets/Wallet/walletrequestdialog.ui \
    widgets/Wallet/walletsenddialog.ui

DISTFILES += \
    win32/Qt5Cored.dll \
    win32/Qt5Guid.dll \
    win32/Qt5Widgetsd.dll \
    win32/zlibd.dll \
    app.ini \
    design/sig/icon_perfomance.png \
    design/sig/icon_perfomance_dark.png \
    app/dcrd.exe \
    app/dcrwallet.exe \
    protos/vmserver.proto




