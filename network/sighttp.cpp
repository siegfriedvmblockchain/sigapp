#include "sighttp.h"
//Base Http client


SIGHttpClient::SIGHttpClient(QObject *p, SIGConfig* cfg,AuthentificationCredentialsEnum creds):
    QObject(p),
    nam(new QNetworkAccessManager(this)),
    reply(0)
{
    this->Credentials=creds;
    if (cfg==nullptr){
        this->config=new SIGConfig();
    }else{
        this->config=cfg;
    }


    connect(nam,SIGNAL(authenticationRequired(QNetworkReply*,QAuthenticator*)),
                this,
                SLOT(onAuthenticationRequest(QNetworkReply*,QAuthenticator*)) );


}


void SIGHttpClient::SetSslCfg(QSslConfiguration sslcfg){
    TLS=true;
    SSLCfg=sslcfg;
}

void SIGHttpClient::SIGHttpGet(QNetworkRequest &request) {

    this->reply = nam->get(request);
    connect(this->reply, SIGNAL(finished()),this, SLOT(slotFinished()));
    connect(this->reply, SIGNAL(error(QNetworkReply::NetworkError)), this,SLOT(slotError(QNetworkReply::NetworkError)));
    connect(this->reply, SIGNAL(sslErrors(QList<QSslError>)),this, SLOT(sslErrors(QList<QSslError>)));



}

void SIGHttpClient::SIGHttpPost(QNetworkRequest &request,QByteArray &data) {

    request.setHeader(QNetworkRequest::ContentLengthHeader, QByteArray::number(data.size()));
    this->reply = this->nam->post(request,data);
    connect(this->reply, SIGNAL(finished()),this, SLOT(slotFinished()));
    connect(this->reply, SIGNAL(error(QNetworkReply::NetworkError)), this,SLOT(slotError(QNetworkReply::NetworkError)));
    connect(this->reply, SIGNAL(sslErrors(QList<QSslError>)),this, SLOT(sslErrors(QList<QSslError>)));

    if(this->TLS) this->reply->setSslConfiguration(this->SSLCfg);
    ReplyTimeout::set(this->reply, 200);




}
void SIGHttpClient::SIGHttpPost(QNetworkRequest &request,QHttpMultiPart *multiPart) {

    this->reply = nam->post(request,multiPart);
connect(this->reply, SIGNAL(finished()),this, SLOT(slotFinished()));
connect(this->reply, SIGNAL(error(QNetworkReply::NetworkError)), this,SLOT(slotError(QNetworkReply::NetworkError)));
connect(this->reply, SIGNAL(sslErrors(QList<QSslError>)),this, SLOT(sslErrors(QList<QSslError>)));


}

void SIGHttpClient::onAuthenticationRequest(QNetworkReply * reply, QAuthenticator * aAuthenticator){
    qDebug() << Q_FUNC_INFO;
    switch (this->Credentials){
    case AuthentificationCredentialsEnum::WalletAuth:{

        aAuthenticator->setUser(config->User);
        aAuthenticator->setPassword(config->Pass);
        break;
    };
    case AuthentificationCredentialsEnum::RancherAuth:{

        aAuthenticator->setUser(config->RancherUser);
        aAuthenticator->setPassword(config->RancherPass);
        break;
    };
    };
}



void SIGHttpClient::sslErrors(QList<QSslError> errors){

}
void SIGHttpClient::slotError(QNetworkReply::NetworkError code) {
    qDebug() << Q_FUNC_INFO << "SIGApp-> Error" << code;
    QString message = reply->errorString();
    QList<QByteArray> headerList = this->reply->rawHeaderList();
    foreach(QByteArray head, headerList) {
        qDebug() << head << ":" << reply->rawHeader(head);
    }
    emit requestError(code,&message);
}

void SIGHttpClient::namFinished(QNetworkReply *reply) {
    qDebug() << Q_FUNC_INFO << reply;
}

void SIGHttpClient::slotFinished() {
    qDebug() << Q_FUNC_INFO << "SIGApp-> Error" << reply->error();
    QByteArray ba = reply->readAll();
    qDebug() << Q_FUNC_INFO << "SIGApp-> Bytes read" << ba.size();
    reply->deleteLater();
    reply = 0;
    QString data(QString::fromLatin1(ba)); (&ba);
    //reply->~QNetworkReply();
    qDebug()<<"SIGApp->"<<data;
    if (ba.capacity()!=0)
        emit requestResult(&data);

}

SIGHttpClient::~SIGHttpClient()
{
    delete nam;
}


