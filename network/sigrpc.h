#ifndef SIGRPC_H
#define SIGRPC_H
#include <grpc++/grpc++.h>
#include "widgets/Wallet/api/api.pb.h"
#include "widgets/Wallet/api/api.grpc.pb.h"
#include <QObject>
#include <sys/types.h>
#include <QProcess>
#include <QApplication>
#include <QDir>
#include <QStandardPaths>
#include <QDebug>
#include <QString>
#include <QSettings>
#include <QFileInfo>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <default.h>

#if (defined (Q_OS_WIN) || defined (Q_OS_WIN32) || defined (Q_OS_WIN64))
#define MAX_USERNAME 128
#endif
#if (defined (Q_OS_LINUX))
#include <pwd.h>
#endif

#define SIGAPP_WALLET_NAME "DcrWallet"

using namespace grpc;
using namespace walletrpc;

class SIGRpcClient : public QObject
{
    Q_OBJECT
public:
    explicit SIGRpcClient(QObject *parent = nullptr,SIGConfig *cfg = nullptr);
    ~SIGRpcClient();
    std::shared_ptr<ChannelCredentials> ssl_channel_credentials;


    SIGConfig *config;
    void SetSSLCredentials();
    void SetSSLChannelStub(QString url);
    std::unique_ptr<WalletService::Stub> stub;
signals:

public slots:
private:

    grpc::string ReadFile(QString file_path);

    std::shared_ptr<Channel> channel;



};

#endif // SIGRPC_H
