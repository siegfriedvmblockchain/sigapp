#ifndef SIGWS_H
#define SIGWS_H
#include <QWebSocket>
#include <QObject>
#include <default.h>
//Bas WebSockets Class
class SIGWsClient : public QObject
{
    Q_OBJECT
public:
    explicit SIGWsClient(QObject *parent);
    ~SIGWsClient();
    void WsConnect(QUrl url);
    void Request(QString data);
    void Request(QByteArray data);
private:
    QWebSocket *sigws;
    QUrl ws_url;
    bool ws_debug;
signals:
    void Response(QString *data);
    void Status(WSStatusEnum status);
public slots:
    void OnConnected();
    void Closed();
    void OnResponcseReceived(QString message);
    void OnError(QAbstractSocket::SocketError);
};

#endif // SIGWS_H
