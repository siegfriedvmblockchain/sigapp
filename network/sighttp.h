#ifndef SIGHTTP_H
#define SIGHTTP_H
#include <QtNetwork>
#include <default.h>
//Base HTTP client class
class SIGHttpClient : public QObject
{
    Q_OBJECT
public:
    explicit SIGHttpClient(QObject *p = 0, SIGConfig* cfg = nullptr,AuthentificationCredentialsEnum cred=AuthentificationCredentialsEnum::WalletAuth);
    ~SIGHttpClient();
    void SetSslCfg(QSslConfiguration sslcfg);
    void SIGHttpGet(QNetworkRequest &request);
//    void SIGHttpPost(QNetworkRequest &request,QIODevice *data);
    void SIGHttpPost(QNetworkRequest &request,QByteArray &data);
    void SIGHttpPost(QNetworkRequest &request,QHttpMultiPart *multiPart);

    QNetworkAccessManager *nam;
    QNetworkReply *reply;
    SIGConfig *config;
private:
    bool TLS=false;
    QSslConfiguration SSLCfg;
    AuthentificationCredentialsEnum Credentials;
public slots:
    void slotError(QNetworkReply::NetworkError code);
    void sslErrors(QList<QSslError>);
    void namFinished(QNetworkReply *reply);
    void slotFinished();
signals:
    void requestError(int code,QString *message);
    void requestResult(QString* data);
private:

private slots:
    void onAuthenticationRequest(QNetworkReply * reply, QAuthenticator * aAuthenticator);

};


#endif // SIGHTTP_H
