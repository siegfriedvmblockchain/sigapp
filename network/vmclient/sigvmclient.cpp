#include "sigvmclient.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
SigVmClient::SigVmClient(std::shared_ptr<Channel> channel)
    : stub_(vmserverapiSrv::NewStub(channel)) {

}

QString SigVmClient::NewSession(NewSessionRequestModel req_model){

    NewSessionRequest request;
    NewSessionResponse reply;
    ClientContext context;

    QJsonObject jsonObj;
    jsonObj.insert("sig_app", req_model.sig_api);
    jsonObj.insert("timestamp", req_model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ"));
    QJsonObject jsonObj2;


    jsonObj2.insert("destination_id", QJsonValue(req_model.blockchain.destination_id));
    jsonObj2.insert("source_id", QJsonValue(req_model.blockchain.source_id));


    jsonObj.insert(QString("blockchain"), QJsonValue(jsonObj2));
    QJsonDocument doc(jsonObj);
    QString strJson(doc.toJson(QJsonDocument::Compact));
    qDebug()<<"client: req->"<<jsonObj;
    request.set_request(strJson.toStdString());
    Status status = stub_->NewSession(&context, request, &reply);
    if (status.ok()) {
        auto rt=QString::fromStdString(reply.response());
        qDebug()<<rt;
        return rt;
    } else {
        std::cout << status.error_code() << ": " << status.error_message()
                  << std::endl;
        return "RPC failed";
    }

}

