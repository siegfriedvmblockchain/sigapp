#ifndef SIGVMCLIENT_H
#define SIGVMCLIENT_H
#include <default.h>
#include <../../model/nodemodel.h>
#include <QDebug>
#include <grpc++/grpc++.h>
#include "../../network/vmserver/vmserver.grpc.pb.h"
#include "../../network/vmserver/vmserver.pb.h"

using grpc::Channel;
using namespace std;
using namespace grpc;
using namespace vmserverapi;
class SigVmClient
{
public:
    SigVmClient(std::shared_ptr<Channel> channel);
    QString NewSession( NewSessionRequestModel model);
private:
    unique_ptr<vmserverapiSrv::Stub> stub_;
};
#endif // SIGVMCLIENT_H
