#include "sigrpc.h"


#if (defined (Q_OS_LINUX))
#include <stdlib.h>
#include </usr/include/unistd.h>
#endif

//Base RPC client
SIGRpcClient::SIGRpcClient(QObject *parent,SIGConfig *cfg) : QObject(parent)
{

    if (cfg==nullptr){
     this->config=new SIGConfig();
    }else{
        this->config=cfg;
    }


    QProcessEnvironment env =  QProcessEnvironment();
#if (defined (Q_OS_WIN) || defined (Q_OS_WIN32) || defined (Q_OS_WIN64))
    if (!env.contains("GRPC_SSL_CIPHER_SUITES")){
        int code=QProcess::execute("set GRPC_SSL_CIPHER_SUITES=HIGH+ECDSA");
    }
#endif
#if (defined (Q_OS_LINUX))
    setenv("GRPC_SSL_CIPHER_SUITES","HIGH+ECDSA", 1);

#endif


    if(cfg->TLS){
        SetSSLCredentials();
        SetSSLChannelStub(this->config->Host);
    }
}

void SIGRpcClient::SetSSLCredentials(){

    // Gets Certificate
    QString wallet_tls_cert_file = []{
#if (defined (Q_OS_WIN) || defined (Q_OS_WIN32) || defined (Q_OS_WIN64))

        QString curName=QApplication::applicationDisplayName();
        QString path = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation).at(0);
        path=QDir::toNativeSeparators(path.remove(curName).append(SIGAPP_WALLET_NAME).append(QDir::separator()).append("rpc.cert"));
        return path;
#endif
#if (defined (Q_OS_LINUX))
        auto pw = getpwuid(getuid());
        if (pw == nullptr || pw->pw_dir == nullptr) {
            //throw NoHomeDirectoryException{};
            return pw->pw_dir;// + "/.dcrwallet/rpc.cert";
        }
#endif
    }();
    //Sets SSL Options
    auto server_ca_pem = ReadFile(wallet_tls_cert_file);
    SslCredentialsOptions opt;
    opt.pem_root_certs=server_ca_pem;

    ssl_channel_credentials = SslCredentials(opt);
}
//
// Sets ChannelStub
//
void SIGRpcClient::SetSSLChannelStub(QString url){
         channel = grpc::CreateChannel(url.toStdString(), ssl_channel_credentials);
         stub = walletrpc::WalletService::NewStub(channel);
}



//
//Reads file data
//
grpc::string SIGRpcClient::ReadFile(QString const file_path) {
    QFile file(file_path);
    QFile inputFile(file_path);
    QString res;
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        res  = in.readAll();
        inputFile.close();
    }
    return res.toStdString();
}

SIGRpcClient::~SIGRpcClient(){

}
