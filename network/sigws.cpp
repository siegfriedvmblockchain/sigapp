#include "sigws.h"

//Base WS client
SIGWsClient::SIGWsClient(QObject *parent) : QObject(parent)
{
    sigws = new QWebSocket();
    connect(sigws, SIGNAL(connected()), this, SLOT(OnConnected()));
    connect(sigws, SIGNAL(disconnected()), this, SLOT(Closed()));
    connect(sigws, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(OnError(QAbstractSocket::SocketError)));
    connect(sigws, SIGNAL(textMessageReceived(QString)),this, SLOT(OnResponcseReceived(QString)));
}
void SIGWsClient::OnError(QAbstractSocket::SocketError error){
    qDebug()<<error;
}
void SIGWsClient::WsConnect(QUrl url)
{
    #if _DEBUG
            qDebug() << "WebSocket server :" << url;
    #endif;

      //  connect(sigws, SIGNAL(binaryMessageReceived(QByteArray&)),this, SLOT(OnResponcseReceived(QByteArray*)));
        sigws->open(url);
}
void SIGWsClient::OnConnected()
{
#if _DEBUG
        qDebug() << "Connected";
#endif;
    emit Status(WSStatusEnum::Connected);

}

void SIGWsClient::Request(QString data){
    //TODO: Add data validation
    sigws->sendTextMessage(data);
}
void SIGWsClient::Request(QByteArray data){
    //TODO: Add data validation
    sigws->sendBinaryMessage(data);
}

void SIGWsClient::OnResponcseReceived(QString message)
{
#if _DEBUG
        qDebug() << "Message received:" << message;
#endif;
  emit Response(&message);
}
void SIGWsClient::Closed()
{

    emit Status(WSStatusEnum::Disconnected);
}
SIGWsClient::~SIGWsClient(){

sigws->disconnect();

}
