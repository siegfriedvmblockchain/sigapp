#include "sigvmserver.h"

SigVmServer::SigVmServer(QString address):
    srvAddress(&address)
{
    builder=new ServerBuilder();

    std::string server_address=srvAddress->toStdString().c_str();
    service= new SigVmServerImpl();

    builder->AddListeningPort(server_address, grpc::InsecureServerCredentials());

    builder->RegisterService(service);

    qDebug() << "VM Server " << QString::fromStdString(server_address)<<"registered\n";

}
void SigVmServer::updateVMData(BaseVmModel *bvmm){
    service->update(bvmm);
}
void SigVmServer::process(){


    server=builder->BuildAndStart();

    qDebug() << "VM Server ready\n";
    server->Wait();
}

void SigVmServer::stop(){


    server->Shutdown();
    // Always shutdown the completion queue after the server.
}
SigVmServer::~SigVmServer(){
    stop();
}

