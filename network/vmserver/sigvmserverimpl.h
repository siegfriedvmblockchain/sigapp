#ifndef SIGVMSERVERIMPL_H
#define SIGVMSERVERIMPL_H
#include "vmserver.grpc.pb.h"
#include "model/nodemodel.h"
#include "model/vmdatamodel.h"
#include "common/vmdata.h"
using namespace vmserverapi;

using grpc::Status;
using grpc::ServerContext;

class SigVmServerImpl final : public vmserverapiSrv::Service {
private:
    BaseVmModel *_bvmm;
public:

    void update(BaseVmModel *bvmm){
        _bvmm=bvmm;
    }
    Status NewSession(ServerContext* context, const NewSessionRequest* request,
                      NewSessionResponse* reply) override {
      VmData vd;
      auto in= vd.ParseNewSessionRequest(QString::fromStdString(request->request()));
        qDebug()<< QString::fromStdString(request->request().c_str());
      auto out = vd.CreateNewSessionRequestJson(in);
      reply->set_response(out.toStdString());
        return Status::OK;
    }
    Status GetNodeStatus(grpc::ServerContext *context, const GetNodeStatusRequest *request, GetNodeStatusResponse *response)override{
        return Status::OK;
    }
    Status GetVMList(grpc::ServerContext *context, const GetVMListRequest *request, GetVMListResponse *response) override{
        return Status::OK;
    }

    Status RunVM(grpc::ServerContext *context, const RunVMRequest *request, RunVMResponse *response) override{
        return Status::OK;
    }
    Status StopVM(grpc::ServerContext *context, const StopVMRequest *request, StopVMResponse *response) override{
        return Status::OK;
    }
    Status DeleteVM(grpc::ServerContext *context, const DeleteVMRequest *request, DeleteVMResponse *response) override{
        return Status::OK;
    }
    Status CreateVM(grpc::ServerContext *context, const CreateVMRequest *request, CreateVMResponse *response) override{
        return Status::OK;
    }
    Status GetVMStatus(grpc::ServerContext *context, const GetVMStatusLinkRequest *request, GetVMStatusLinkResponse *response) override{
        return Status::OK;
    }

};
#endif // SIGVMSERVERIMPL_H
