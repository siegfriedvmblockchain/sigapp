#ifndef SIGVMSERVER_H
#define SIGVMSERVER_H
#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>
#include <string>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/security/server_credentials.h>
#include "sigvmserverimpl.h"
#include "model/vmdatamodel.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;
using grpc::ServerCompletionQueue;
using std::chrono::system_clock;
#include <QObject>
#include <QDebug>
class SigVmServer: public QObject
{
    Q_OBJECT
public:
    explicit SigVmServer(QString address);
    ~SigVmServer();
    void updateVMData(BaseVmModel *bvmm);
public slots:
    void process();
    void stop();
private:
    QString *srvAddress;
    SigVmServerImpl *service;
    ServerBuilder *builder;
   std::unique_ptr<ServerCompletionQueue> cq;
   std::unique_ptr<Server>  server;
};
#endif // SIGVMSERVER_H
