#ifndef DEFAULT_H
#define DEFAULT_H
#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include <cstdio>
#include <QDebug>
#include <common/timeout.h>
#include <QUrl>

#define SIGAPP_NODE "dcrd"
#define SIGAPP_WALLET "dcrwallet"

using namespace rapidjson;
using namespace std;

const QString configPath = "app.ini";
struct SIGConfig{
    bool Debug;
    bool AppMinerMode;
    bool TLS;
    bool IsTestNetwork;
    QString User;
    QString Pass;
    QString AppPath;
    QString RancherUser;
    QString RancherPass;
    QString RancherHost;
    QString RancherPort;
    QString Host;
    QString WalletPath;
    QString NodePath;
    QString WalletCfgPath;
    QString WalletHost;
    QString WalletPort;
    QString WalletUser;
    QString WalletPass;
    QString NodeCfgPath;
    QString NodePort;
    QString VMPort;
    QString VMHost;


SIGConfig(){
        TLS=false,
        Host  = "127.0.0.1:9110";
       }
};

enum  WSStatusEnum {Connected,Disconnected};
enum ErrorMsgEnum {FatalError,Error};
enum ResponseTypeEnum {WS,HTTP};
enum AppNamesEnum {DCRNode,DCRWallet};
enum AuthentificationCredentialsEnum {WalletAuth,RancherAuth};


#endif // DEFAULT_H

