#include "clock.h"
#include "ui_clock.h"
Clock::Clock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Clock)
{
    ui->setupUi(this);
    this->isStopped=false;
    this->time= QTime::currentTime();
    this->date= QDate::currentDate();
    ui->lbl_time->setText(this->time.toString("hh:mm:ss"));
    ui->lbl_date->setText(this->date.toString("ddd MMMM dd yy"));

    timer = new QTimer;


    QObject::connect(timer,SIGNAL(timeout()), this, SLOT(update()));


    timer->start(1000);
}
void Clock::update()
{
    this->time= QTime::currentTime();
    QDate tdate=QDate::currentDate();
    if (tdate!=this->date) {
        this->date=tdate;
            ui->lbl_date->setText(this->date.toString("ddd MMMM d yy"));
    }

    ui->lbl_time->setText(this->time.toString("hh:mm:ss"));

}

Clock::~Clock()
{
    delete ui;
}
