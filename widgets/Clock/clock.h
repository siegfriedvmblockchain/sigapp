#ifndef CLOCK_H
#define CLOCK_H

#include <QWidget>
#include <QDateTime>
#include <QTimer>
namespace Ui {
class Clock;
}

class Clock : public QWidget
{
    Q_OBJECT

public:
    explicit Clock(QWidget *parent = 0);
    ~Clock();

private:
    Ui::Clock *ui;
    QDate date;
    QTime time;
    bool isStopped;
    QTimer *timer;
private slots:
    void update();

};

#endif // CLOCK_H
