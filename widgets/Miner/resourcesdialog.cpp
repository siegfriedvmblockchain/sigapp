#include "resourcesdialog.h"
#include "ui_resourcesdialog.h"
#include <QDebug>
ResourcesDialog::ResourcesDialog(QWidget *parent, Params *prm) :
    QWidget(parent),
    ui(new Ui::ResourcesDialog)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::Tool|Qt::CustomizeWindowHint);
    params=prm;
    ui->sliderCpu->setMinimum(1);
    ui->sliderDisk->setMinimum(1);
    ui->sliderMemory->setMinimum(1);
    ui->lblCpu->setText(CPUMask.arg(params->cpu.selected));
    ui->lblMemory->setText(MemoryMask.arg(params->memory.selected));
    ui->lblDisk->setText(DiskMask.arg(params->disk.selected));
    ui->sliderCpu->setMaximum(params->cpu.total);
    ui->sliderDisk->setMaximum(params->disk.total);
    ui->sliderMemory->setMaximum(params->memory.total);
    ui->sliderCpu->setValue(params->cpu.selected);
    ui->sliderDisk->setValue(params->disk.selected);
    ui->sliderMemory->setValue(params->memory.selected);

    if(!params->IsNodeConnected){
        ui->sliderCpu->setEnabled(false);
        ui->sliderDisk->setEnabled(false);
        ui->sliderMemory->setEnabled(false);
        ui->labelResourcesInfo->setVisible(true);
    }else{
        ui->sliderCpu->setEnabled(true);
        ui->sliderDisk->setEnabled(true);;
        ui->sliderMemory->setEnabled(true);;
        ui->labelResourcesInfo->setVisible(false);
    }

}

ResourcesDialog::~ResourcesDialog()
{
    delete ui;
}


void ResourcesDialog::on_BtnClose_clicked()
{
    this->close();
}

void ResourcesDialog::on_sliderCpu_sliderMoved(int position)
{
    ui->lblCpu->setText(CPUMask.arg(position));
    params->cpu.selected=position;
    emit settingsUpdated(params);

}

void ResourcesDialog::on_sliderMemory_sliderMoved(int position)
{
    ui->lblMemory->setText(MemoryMask.arg(position));
    params->memory.selected=position;
    emit settingsUpdated(params);

}
void ResourcesDialog::on_sliderDisk_sliderMoved(int position)
{
    ui->lblDisk->setText(DiskMask.arg(position));
    params->disk.selected=position;
    emit settingsUpdated(params);
}
