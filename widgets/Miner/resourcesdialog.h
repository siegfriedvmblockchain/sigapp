#ifndef RESOURCESDIALOG_H
#define RESOURCESDIALOG_H

#include <QWidget>

namespace Ui {
class ResourcesDialog;
}

class ResourcesDialog : public QWidget
{
    Q_OBJECT

public:
    struct Data{
        int total; int selected;
    };

    struct Params{
        Data cpu;
        Data memory;
        Data disk;
        bool IsNodeConnected;
    };

    explicit ResourcesDialog(QWidget *parent = 0, Params *prm=new Params() );

    ~ResourcesDialog();

private slots:
    void on_BtnClose_clicked();

    void on_sliderCpu_sliderMoved(int position);

    void on_sliderMemory_sliderMoved(int position);

    void on_sliderDisk_sliderMoved(int position);

private:
    Ui::ResourcesDialog *ui;
    Params *params;
    QString CPUMask="%1 Core(s)";
        QString MemoryMask="%1 GB";
                QString DiskMask="%1 GB";
signals:
      void settingsUpdated(ResourcesDialog::Params*);
};

#endif // RESOURCESDIALOG_H
