#include "blocksigdetails.h"
#include "ui_blocksigdetails.h"

BlockSigDetails::BlockSigDetails(QWidget *parent,BlockData *data) :
    params(data),
    QWidget(parent),
    ui(new Ui::BlockSigDetails)
{
    this->setWindowFlags(Qt::Tool|Qt::CustomizeWindowHint);
    ui->setupUi(this);
    ui->labelSignedBlock->setText(QString::number(params->lastBlockSigned));

    auto tm=params->date.time().toString("hh:mm:ss");
    auto dt=params->date.date().toString("MMMM d, yyyy");

    ui->labelDate->setText(dt);
    ui->labelTime->setText(tm);
    ui->labelTotalSignedBlocks->setText(QString::number(params->totalSigned));
}
BlockSigDetails::~BlockSigDetails()
{
    delete ui;
}

void BlockSigDetails::on_BtnClose_clicked()
{
    this->close();
}
