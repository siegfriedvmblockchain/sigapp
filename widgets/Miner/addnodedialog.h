#ifndef ADDNODEDIALOG_H
#define ADDNODEDIALOG_H

#include <QWidget>

namespace Ui {
class AddNodeDialog;
}

class AddNodeDialog : public QWidget
{
    Q_OBJECT

public:
    explicit AddNodeDialog(QWidget *parent = 0);
    ~AddNodeDialog();

private slots:
    void on_BtnClose_clicked();

    void on_ButtonSaveISO_clicked();

    void on_ButtonNetworkBootServer_clicked();

private:
    Ui::AddNodeDialog *ui;
};

#endif // ADDNODEDIALOG_H
