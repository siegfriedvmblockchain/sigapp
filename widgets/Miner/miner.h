#ifndef MINER_H
#define MINER_H

#include <QWidget>
#include <QPushButton>
#include <QEvent>
#include <QDebug>
#include <widgets/Miner/resourcesdialog.h>
#include <widgets/Miner/addnodedialog.h>
#include <widgets/Miner/blocksigdetails.h>
namespace Ui {
class Miner;
}

class Miner : public QWidget
{
    Q_OBJECT

public:
    explicit Miner(QWidget *parent = 0);

    ~Miner();

private slots:
    void on_ButtonStart_clicked();

    void on_ButtonStop_clicked();

    void on_ButtonSetA_clicked();

private:
    Ui::Miner *ui;
    enum DialogEnum{ResourcesPopUp,AddNodePopUp,InfoPopUp};
    void SetBtn(QPushButton *button,bool action);
    void ShowDialog(DialogEnum);
    bool isStartPressed=true;
    void paintEvent(QPaintEvent *);
    bool eventFilter(QObject * watched, QEvent * event);
    ResourcesDialog::Params NodeSettings;
    BlockSigDetails::BlockData BlockDetails;
private slots:
    void onSettingsUpdated(ResourcesDialog::Params*);
    void on_ButtonAdd_clicked();
    void on_ButtonInfo_clicked();
    void on_NodeStarted();
    void on_NodeStoped();
signals:
     void startNodeAction();
     void stopNodeAction();

};

#endif // MINER_H
