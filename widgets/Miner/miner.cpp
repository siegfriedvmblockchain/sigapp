#include "miner.h"
#include "ui_miner.h"
#include <QPainter>
#include <QThread>
#include <QMdiArea>
#include <QVBoxLayout>
#include <QMdiSubWindow>
#include <QDateTime>
Miner::Miner(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Miner)
{
    ui->setupUi(this);

    ui->ButtonSetA->installEventFilter(this);
    ui->ButtonSetB->installEventFilter(this);
    ui->ButtonAdd->installEventFilter(this);
    ui->ButtonInfo->installEventFilter(this);
    ui->ButtonStop->setVisible(false);

    NodeSettings.cpu.total=2;
    NodeSettings.cpu.selected=1;
    NodeSettings.memory.total=8;
    NodeSettings.memory.selected=2;
    NodeSettings.disk.total=64;
    NodeSettings.disk.selected=30;

    BlockDetails.date=QDateTime::currentDateTime();
    BlockDetails.lastBlockSigned=5410842;
    BlockDetails.totalSigned=4;
}
void Miner::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}
bool Miner::eventFilter(QObject * watched, QEvent * event)
{
    QPushButton * button = qobject_cast<QPushButton*>(watched);
    if (!button) {
        return false;
    }
    QString objName=QString(button->objectName());



    if (event->type() == QEvent::Enter || event->type() == QEvent::MouseButtonRelease) {
        // The push button is hovered by mouse
        qDebug()<<QString::compare(objName,"ButtonSetA");
        qDebug()<<QString::compare(objName,"ButtonSetB");

        if(QString::compare(objName,"ButtonSetA")==0 ||
                QString::compare(objName,"ButtonSetB")==0){

            button->setIcon(QIcon(":/sig_icons/sig/icon_settings_dark_hover.png"));
            return true;
        }


        if(QString::compare(objName,"ButtonAdd")==0){
            button->setIcon(QIcon(":/sig_icons/sig/icon_add_hover.png"));
            return true;
        }
        if(QString::compare(objName,"ButtonInfo")==0){
            button->setIcon(QIcon(":/sig_icons/sig/icon_info_hover.png"));
            return true;
        }
    }

    if (event->type() == QEvent::Leave){
        // The push button is not hovered by mouse
        if(QString::compare(objName,"ButtonSetA")==0 ||
                QString::compare(objName,"ButtonSetB")==0)
        {
            button->setIcon(QIcon(":/sig_icons/sig/icon_settings_dark.png"));
            return true;
        }

        if(QString::compare(objName,"ButtonAdd")==0){
            button->setIcon(QIcon(":/sig_icons/sig/icon_add.png"));
            return true;
        }
        if(QString::compare(objName,"ButtonInfo")==0){
            button->setIcon(QIcon(":/sig_icons/sig/icon_info.png"));
            return true;
        }

    }

    if (event->type() == QEvent::MouseButtonPress){
        // The push button is not hovered by mouse
        if(QString::compare(objName,"ButtonSetA")==0 ){
            button->setIcon(QIcon(":/sig_icons/sig/icon_settings_dark_pressed.png"));
            ShowDialog(DialogEnum::ResourcesPopUp);

            return true;
        }

        if(QString::compare(objName,"ButtonSetB")==0)
        {
            button->setIcon(QIcon(":/sig_icons/sig/icon_settings_dark_pressed.png"));
            //    ShowDialog(DialogEnum::r);

            return true;
        }
        qDebug()<<QString::compare(objName,"ButtonAdd");
        if(QString::compare(objName,"ButtonAdd")==0){
            button->setIcon(QIcon(":/sig_icons/sig/icon_add_pressed.png"));
            ShowDialog(DialogEnum::AddNodePopUp);
            return true;
        }
        if(QString::compare(objName,"ButtonInfo")==0){
            button->setIcon(QIcon(":/sig_icons/sig/icon_info_pressed.png"));
            ShowDialog(DialogEnum::InfoPopUp);
            return true;
        }

    }

    return false;
}
Miner::~Miner()
{
    delete ui;
}
void Miner::ShowDialog(DialogEnum dialog){
    switch(dialog){
    case DialogEnum::ResourcesPopUp:
    {
        ResourcesDialog *dialog = new ResourcesDialog(this, &NodeSettings);
        dialog->setWindowModality(Qt::ApplicationModal);
        connect(dialog,SIGNAL(settingsUpdated(ResourcesDialog::Params*)),this,SLOT(onSettingsUpdated(ResourcesDialog::Params*)));
        dialog->show();
        break;
    }
    case DialogEnum::AddNodePopUp:
    {
        AddNodeDialog *dialog = new AddNodeDialog(this);
        dialog->setWindowModality(Qt::ApplicationModal);
        dialog->show();
        break;
    }
    case DialogEnum::InfoPopUp:
    {
        BlockSigDetails *dialog = new BlockSigDetails(this,&BlockDetails);
        dialog->setWindowModality(Qt::ApplicationModal);
        dialog->show();
        break;
    }
    }

}

void Miner::SetBtn(QPushButton *button,bool action){
    if (action){
        ui->lblNodeInfo->setText("Loading... Please Wait");
        button->setText("");
        button->setIcon(QIcon(":/sig_icons/sig/icon_loading.png"));
        QSize sz(20, 20);
        sz.scale(60, 60, Qt::KeepAspectRatioByExpanding);
        button->setIconSize(sz);
        isStartPressed=true;
        //todo: add action;
        emit startNodeAction();
        button->setVisible(true);


        ui->lblNodeInfo->setText("Connected to Network");

    }else{
        ui->ButtonStop->setVisible(false);
        ui->lblNodeInfo->setText("Not Connected");
        ui->ButtonStart->setIcon(QIcon());
        ui->ButtonStart->setVisible(true);

        ui->ButtonStart->setText("Start");
        NodeSettings.IsNodeConnected=false;

    }
}

void Miner::on_NodeStarted(){
    ui->ButtonStart->setVisible(false);
    NodeSettings.IsNodeConnected=true;
    ui->ButtonStop->setVisible(true);
}

void Miner::on_NodeStoped(){
  //  SetBtn(ui->ButtonStart,false);
    NodeSettings.IsNodeConnected=false;
}

void Miner::onSettingsUpdated(ResourcesDialog::Params *params){

    this->NodeSettings.cpu=params->cpu;
    this->NodeSettings.disk=params->disk;
    this->NodeSettings.memory=params->memory;
    qDebug()<<this->NodeSettings.cpu.total;
}
void Miner::on_ButtonStart_clicked()
{

    SetBtn(ui->ButtonStart,true);

}

void Miner::on_ButtonStop_clicked()
{

    SetBtn(ui->ButtonStart,false);
    emit stopNodeAction();
}

void Miner::on_ButtonSetA_clicked()
{

}

void Miner::on_ButtonAdd_clicked()
{
    ShowDialog(DialogEnum::AddNodePopUp);
}

void Miner::on_ButtonInfo_clicked()
{

}
