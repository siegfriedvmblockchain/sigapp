#ifndef BLOCKSIGDETAILS_H
#define BLOCKSIGDETAILS_H

#include <QWidget>
#include <QDateTime>
namespace Ui {
class BlockSigDetails;
}

class BlockSigDetails : public QWidget
{
    Q_OBJECT

public:
    struct BlockData{
        int lastBlockSigned;
        QDateTime date;
        int totalSigned;
    };
    explicit BlockSigDetails(QWidget *parent = 0, BlockData *data=new BlockData() );
    ~BlockSigDetails();

private slots:
    void on_BtnClose_clicked();

private:
    Ui::BlockSigDetails *ui;
    BlockData *params;
};

#endif // BLOCKSIGDETAILS_H
