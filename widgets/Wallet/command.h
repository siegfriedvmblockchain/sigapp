#ifndef COMMAND_H
#define COMMAND_H
#include "common/rapidjson_wrapper.h"
#include "default.h"
#include <cstdio>
#include <string>
#include <vector>
#include <model/walletdatamodel.h>
#include <QObject>

class Command : public QObject
{
    Q_OBJECT
public:
    explicit Command(QObject *parent = nullptr);
    enum class WalletCommandsEnum {Balance,Unlock,NewAddress,SendTo,StakeInfo,Bulk};
    QByteArray Request(Command::WalletCommandsEnum cmd,QList<QString> params=QList<QString>());
    bool IsValidResponse(QString resp);
    WalletCommandsEnum Current;
    BalanceModel ParseBalanceResponse(QString jsonrpc);
    ErrorModel ParseErrorResponse(QString jsonrpc);
    SendToModel ParseSendToResponse(QString jsonrpc);
    NewAddressModel ParseNewAddressResponse(QString jsonrpc);
    StakeInfoModel ParseStakeInfoResponse(QString jsonrpc);
signals:
    void commandChanged(Command::WalletCommandsEnum cmd);
public slots:
private:
       QByteArray JsonRpcSerialize(QString jsonrpc,QString method,uint id,QList<QString> params=QList<QString>());

};

#endif // COMMAND_H
