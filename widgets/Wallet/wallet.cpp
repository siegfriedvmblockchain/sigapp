#include "wallet.h"
#include "ui_wallet.h"
#include <QMessageBox>
#include <QPainter>


QString dt;
QString tm0;
Wallet::Wallet(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Wallet)
{
    this->setAccessibleName("Wallet");
    ui->setupUi(this);
    helper->SetFont(this);
    app= new AppConfig(this);
    app->Configuration->TLS=false;
    connect (app,SIGNAL(ConfigurationError(ErrorMsgEnum,QString*)),this,SLOT(OnConfigError(ErrorMsgEnum,QString*)));


    httpClient= new WalletHttpAPI(this,app->Configuration);
    connect(httpClient,SIGNAL(requestError(int,QString*)),this,SLOT(OnError(int,QString*)));

    connect(httpClient,SIGNAL(requestResult(BalanceModel*)),this,SLOT(OnResult(BalanceModel*)));
    connect(httpClient,SIGNAL(requestResult(SendToModel*)),this,SLOT(OnResult(SendToModel*)));
    connect(httpClient,SIGNAL(requestResult(NewAddressModel*)),this,SLOT(OnResult(NewAddressModel*)));
    connect(httpClient,SIGNAL(requestResult(StakeInfoModel*)),this,SLOT(OnResult(StakeInfoModel*)));
    connect(httpClient,SIGNAL(requestResult(ErrorModel*)),this,SLOT(OnErrorResult(ErrorModel*)));

    this->isStopped=false;
    this->time= QTime::currentTime();
    this->date= QDate::currentDate();
    tm0=this->time.toString("hh:mm:ss");
    dt=this->date.toString("MMMM d, yyyy");

    timer = new QTimer;


    QObject::connect(timer,SIGNAL(timeout()), this, SLOT(update()));


    timer->start(1000);



    ui->labelCurrency->setText(QString("%1 SIG = %2 USD").arg("1").arg("124.5"));
    params.address="1jU493508Kfwo48sjfj4hFg403j";
    params.balance.sig= 120.69;
    params.balance.dcr= 23.98;
    params.balance.dcr_usdRate= 12;
    params.balance.sig_usdRate= 67.8;
    params.balance.feepersent=1.5;
}
void Wallet::OnConfigError(ErrorMsgEnum, QString *message){
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setText(*message);
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Ok);
    int ret = msgBox.exec();
}
void Wallet::update()
{
    this->time= QTime::currentTime();
    QDate tdate=QDate::currentDate();

    if (tdate!=this->date) {
        this->date=tdate;
        dt=this->date.toString("MMMM d, yyyy");
    }

    tm0=this->time.toString("HH:mm:ss");
    ui->labelTime->setText(QString("%1, %2").arg(dt).arg(tm0));

}
void Wallet::OnError(int code, QString *message){
    qDebug()<<"Wallet"<< message;
}
void Wallet::OnResult(BalanceModel *data){

    qDebug()<< data->result.blockhash;

}
void Wallet::OnResult(SendToModel *data){

    qDebug()<< data->result;

}
void Wallet::OnResult(NewAddressModel *data){

    qDebug()<< data->result;

}
void Wallet::OnResult(StakeInfoModel *data){

    qDebug()<< data->result;

}
void Wallet::OnErrorResult(ErrorModel *result){
     qDebug()<< QString("Got Wallet Error %1 : %2").arg(result->error.code).arg(result->error.message);
}


void Wallet::OnWSStatus(WSStatusEnum){

}
void Wallet::OnRancherResponse(ResponseTypeEnum, QString *){

}

void Wallet::paintEvent(QPaintEvent *)
 {
     QStyleOption opt;
     opt.init(this);
     QPainter p(this);
     style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
 }
Wallet::~Wallet()
{
    delete ui;
    delete httpClient;
}

void Wallet::on_WalletRequestButton_clicked()
{

    WalletRequestDialog *dialog = new WalletRequestDialog(this,&params);
        dialog->setWindowModality(Qt::ApplicationModal);
        dialog->show();
        httpClient->Ballance();
}

void Wallet::on_WalletSendButton_clicked()
{
  //  httpClient->Ballance();

    WalletSendDialog *dialog = new WalletSendDialog(this,&params);
        dialog->setWindowModality(Qt::ApplicationModal);
        dialog->show();

}
