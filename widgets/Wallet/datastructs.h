#ifndef DATASTRUCTS_H
#define DATASTRUCTS_H
#include <QString>
struct Balance{
    double sig;
    double dcr;
    double dcr_usdRate;
    double sig_usdRate;
    double feepersent;
};
struct CurrencyParams{
    QString address;
    Balance balance;
};
#endif // DATASTRUCTS_H
