#include "walletrequestdialog.h"
#include "ui_walletrequestdialog.h"
#include <QClipboard>
WalletRequestDialog::WalletRequestDialog(QWidget *parent,CurrencyParams  *prm) :
    params(prm),
    QWidget(parent),
    ui(new Ui::WalletRequestDialog)
{
    this->setWindowFlags(Qt::Tool|Qt::CustomizeWindowHint);
    ui->setupUi(this);

    ui->labelAddress->setText(prm->address);
    double rate=prm->balance.sig_usdRate*prm->balance.sig;
    ui->labelBalance->setText(BalanceMask.arg(prm->balance.sig).arg(rate));
}

WalletRequestDialog::~WalletRequestDialog()
{
    delete ui;
}

void WalletRequestDialog::on_BtnClose_clicked()
{
    this->close();
}

void WalletRequestDialog::on_ButtonCopy_clicked()
{
    QClipboard *clipboard = QApplication::clipboard();

    clipboard->setText(params->address);
}



void WalletRequestDialog::on_radioButtonDCR_clicked()
{

     double rate=params->balance.dcr_usdRate*params->balance.dcr;
     ui->labelBalance->setText(BalanceMask.arg(params->balance.dcr).arg(rate));
}

void WalletRequestDialog::on_radioButtonSIG_clicked()
{

    double rate=params->balance.sig_usdRate*params->balance.sig;
    ui->labelBalance->setText(BalanceMask.arg(params->balance.sig).arg(rate));
}
