#ifndef WALLET_H
#define WALLET_H

#include <QWidget>
#include <QDateTime>
#include <QTimer>
#include <api/api_walletrpc.h>
#include <api/api_wallethttp.h>
#include <walletrequestdialog.h>
#include <walletsenddialog.h>
#include <common/appconfig.h>
#include <common/helper.h>
#include <command.h>
#include <stdlib.h>

namespace Ui {
class Wallet;
}

class Wallet : public QWidget
{
    Q_OBJECT

public:
    explicit Wallet(QWidget *parent = 0);
    ~Wallet();
   WalletHttpAPI *httpClient;
   QDate date;
   QTime time;
   bool isStopped;
   QTimer *timer;
   CurrencyParams params;
private slots:
    void update();
    void OnError(int code, QString *message);
    void OnConfigError (ErrorMsgEnum,QString *message);

    void OnRancherResponse(ResponseTypeEnum,QString*);
    void OnWSStatus(WSStatusEnum);

    // Wallet API
      void OnResult(BalanceModel *result);
      void OnResult(NewAddressModel *result);
      void OnResult(SendToModel *result);
      void OnResult(StakeInfoModel *result);
      void OnErrorResult(ErrorModel*);
      void on_WalletRequestButton_clicked();
      void on_WalletSendButton_clicked();

private:
    Ui::Wallet *ui;
    AppConfig *app;
    SIGHelper *helper;
    void paintEvent(QPaintEvent *);



};

#endif // WALLET_H
