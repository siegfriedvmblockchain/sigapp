#ifndef WALLETREQUESTDIALOG_H
#define WALLETREQUESTDIALOG_H

#include <QWidget>
#include <datastructs.h>
namespace Ui {
class WalletRequestDialog;
}

class WalletRequestDialog : public QWidget
{
    Q_OBJECT

public:

    explicit WalletRequestDialog(QWidget *parent = 0,CurrencyParams *prm=new CurrencyParams());
    ~WalletRequestDialog();
    CurrencyParams *params;
private slots:
    void on_BtnClose_clicked();

    void on_ButtonCopy_clicked();


    void on_radioButtonDCR_clicked();

    void on_radioButtonSIG_clicked();

private:
    Ui::WalletRequestDialog *ui;
    QString BalanceMask="Your balance: %1 (USD %2)";
};

#endif // WALLETREQUESTDIALOG_H
