#include "command.h"
#include <QJsonArray>
#include <QJsonObject>
Command::Command(QObject *parent) : QObject(parent)
{

}

QByteArray Command::JsonRpcSerialize(QString jsonrpc,QString method,uint id,QList<QString> params){

    rapidjson::Serialize s;
    s.startObject();
    s<<"jsonrpc"<<jsonrpc;
    s<<"method"<<method;
    s.startArray("params");
    foreach(QString itm, params ){
        s<<itm;
    }
    s.endArray();
    s<<"id"<< id;
    s.endObject();
    qDebug()<<"JSON:"<< s.getString();
    //According to jsonrpc
    QByteArray res;
    res.append(s.getString());

    return res;
}





//Parses Error JSON Response
ErrorModel Command::ParseErrorResponse(QString jsonrpc){
    ErrorModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.id=d.value("id").toInt();
    res.result=d.value("result").toString();
    qDebug()<<res.result;
    QJsonArray error=d["error"].toJsonArray();
    QJsonValue v=error.at(0);
    res.error.code=v["code"].toInt();
    res.error.message=v["message"].toString();

    return res;
}

// Parses Response JSON for StakeInfo request
StakeInfoModel  Command::ParseStakeInfoResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    StakeInfoModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.id=d.value("id").toInt();
    QJsonArray error=d["error"].toJsonArray();
    QJsonValue v=error.at(0);
    res.error.code=v["code"].toInt();
    res.error.message=v["message"].toString();
   res.result=d["result"].toString();
    return res;
}

// Parses Response JSON for NewAddress request
NewAddressModel  Command::ParseNewAddressResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    NewAddressModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.id=d.value("id").toInt();
    QJsonArray error=d["error"].toJsonArray();
    QJsonValue v=error.at(0);
    res.error.code=v["code"].toInt();
    res.error.message=v["message"].toString();
    res.result=d["result"].toString();
    return res;
}

// Parses Response JSON for SendTo request
SendToModel  Command::ParseSendToResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    SendToModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.id=d.value("id").toInt();
    QJsonArray error=d["error"].toJsonArray();
    QJsonValue v=error.at(0);
    res.error.code=v["code"].toInt();
    res.error.message=v["message"].toString();
   res.result=d["result"].toString();
    return res;
}

// Parses Response JSON for Ballance request
BalanceModel  Command::ParseBalanceResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    BalanceModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.id=d.value("id").toInt();
    QJsonArray error=d["error"].toJsonArray();
    QJsonValue v=error.at(0);
    res.error.code=v["code"].toInt();
    res.error.message=v["message"].toString();
    QMap<QString,QVariant> result=d["result"].toMap();
    res.result.blockhash= result["blockhash"].toString();
    QJsonArray balances=result["balances"].toJsonArray();
    foreach (const QJsonValue & value, balances) {
        QJsonObject obj = value.toObject();
        BalancesModel blnc;
        blnc.accountname=obj["accountname"].toString();
        blnc.immaturecoinbaserewards=obj["immaturecoinbaserewards"].toDouble();
        blnc.immaturestakegeneration=obj["immaturestakegeneration"].toDouble();
        blnc.lockedbytickets=obj["lockedbytickets"].toInt();
        blnc.spendable=obj["spendable"].toDouble();
        blnc.total=obj["total"].toDouble();
        blnc.unconfirmed=obj["unconfirmed"].toDouble();
        blnc.votingauthority=obj["votingauthority"].toBool();
        res.result.balances.append(blnc);
    }
    return res;
}

bool Command::IsValidResponse(QString resp){
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(resp.toUtf8());
    QMap<QString,QVariant> result=d["result"].toMap();
    if (result.empty()) return false;
    return true;
}
//
//Creates JSONRPC request
//
QByteArray Command::Request(WalletCommandsEnum cmd,QList<QString> params){
    QByteArray res;
    QString ver="1.0";
    int id=1;
    switch (cmd){
    case WalletCommandsEnum::Balance:
        res=JsonRpcSerialize(ver,"getbalance",id);
        break;
    case WalletCommandsEnum::Unlock:
        if(params.length()==0) return nullptr;
        res=JsonRpcSerialize(ver,"walletpassphrase",id,params);
        break;
    case WalletCommandsEnum::NewAddress:
        res=JsonRpcSerialize(ver,"getnewaddress",id);
        break;
    case WalletCommandsEnum::SendTo:
        if(params.length()==0) return nullptr;
        res=JsonRpcSerialize(ver,"sendtoaddress",id,params);
        break;
    case WalletCommandsEnum::StakeInfo:
        res=JsonRpcSerialize(ver,"getstakeinfo",id);
        break;
    }
    Current=cmd;

    return res;
}
