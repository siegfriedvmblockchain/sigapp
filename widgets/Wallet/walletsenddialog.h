#ifndef WALLETSENDDIALOG_H
#define WALLETSENDDIALOG_H

#include <QWidget>
#include <QDebug>
#include <datastructs.h>
namespace Ui {
class WalletSendDialog;
}

class WalletSendDialog : public QWidget
{
    Q_OBJECT

public:
    explicit WalletSendDialog(QWidget *parent = 0,CurrencyParams *prm=new CurrencyParams());
    ~WalletSendDialog();

private slots:
    void on_BtnClose_clicked();
    void on_textSIG_textChanged();
    void on_textUSD_textChanged();

    void on_radioButtonSIG_clicked();
    void on_radioButtonDCR_clicked();

private:
    Ui::WalletSendDialog *ui;
    CurrencyParams *params;
   QString BalanceMask="Your balance: %1 SIG (USD %2)";
   QString FeeMask="Transaction Fee: %1 SIG (USD %2)";
};

#endif // WALLETSENDDIALOG_H
