#include "walletsenddialog.h"
#include "ui_walletsenddialog.h"
bool changed=false;
WalletSendDialog::WalletSendDialog(QWidget *parent,CurrencyParams *prm) :
    params(prm),
    QWidget(parent),
    ui(new Ui::WalletSendDialog)
{
    this->setWindowFlags(Qt::Tool|Qt::CustomizeWindowHint);
    ui->setupUi(this);

    double rate=prm->balance.sig_usdRate*prm->balance.sig;
    ui->labelBalance->setText(BalanceMask.arg(prm->balance.sig).arg(rate));
}

WalletSendDialog::~WalletSendDialog()
{
    delete ui;
}

void WalletSendDialog::on_BtnClose_clicked()
{
    this->close();
}


void WalletSendDialog::on_textSIG_textChanged()
{
    double amount;
   if(!changed){
     amount=ui->textSIG->toPlainText().toDouble();
    auto calc=amount*params->balance.sig_usdRate;
     changed=true;
    ui->textUSD->setText(QString::number(calc));
    double fee = amount*params->balance.feepersent/100;
    double usdfee=fee*params->balance.sig_usdRate;
    ui->labelTrasactionFee->setText(FeeMask.arg(fee).arg(usdfee));
   }

   changed=false;
}


void WalletSendDialog::on_textUSD_textChanged()
{
    double amount;
    if(!changed){
     amount=ui->textUSD->toPlainText().toDouble();
        double calc=amount/params->balance.sig_usdRate;
        changed=true;
        ui->textSIG->setText(QString::number(calc));
        double fee = calc*params->balance.feepersent/100;
        double usdfee=fee*params->balance.sig_usdRate;
        ui->labelTrasactionFee->setText(FeeMask.arg(fee).arg(usdfee));
    }

   changed=false;
}


void WalletSendDialog::on_radioButtonSIG_clicked()
{
    double rate=params->balance.sig_usdRate*params->balance.sig;
    ui->labelBalance->setText(BalanceMask.arg(params->balance.sig).arg(rate));
}


void WalletSendDialog::on_radioButtonDCR_clicked()
{
    double rate=params->balance.dcr_usdRate*params->balance.dcr;
    ui->labelBalance->setText(BalanceMask.arg(params->balance.dcr).arg(rate));
}
