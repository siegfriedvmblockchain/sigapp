#ifndef SWAIN_H
#define SWAIN_H

#include <QWidget>
#include <api/api_rancher.h>
#include <common/appconfig.h>
#include <stdlib.h>
#include <network/sighttp.h>

namespace Ui {
class Swain;
}

class Swain : public QWidget
{
    Q_OBJECT

public:
    explicit Swain(QWidget *parent = 0);
    ~Swain();

private slots:
    void on_pushButton_clicked();

    void OnError(int code, QString *message);
    void OnResult(QString *result);
    void OnRancherResponse(ResponseTypeEnum,QString*);
    void OnWSStatus(WSStatusEnum);

private:
    Ui::Swain *ui;
    AppConfig *app;
};

#endif // SWAIN_H
