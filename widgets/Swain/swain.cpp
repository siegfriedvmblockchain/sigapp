#include "swain.h"
#include "ui_swain.h"

Swain::Swain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Swain)
{
    ui->setupUi(this);
    app= new AppConfig(this);
    app->Configuration->TLS=false;
    connect (app,SIGNAL(Error(ErrorMsgEnum,QString)),this,SLOT(OnConfigError(ErrorMsgEnum,QString)));
    qDebug() << app->Configuration->NodeCfgPath;
}

Swain::~Swain()
{
    delete ui;
}


void Swain::OnError(int code, QString *message)
{
    ui->textBrowser->append(QStringLiteral("Error code %1").arg(code));
    ui->textBrowser->append(*message);
}
void Swain::OnResult(QString *result)
{
    ui->textBrowser->append(*result);
}


void Swain::on_pushButton_clicked()
{
    ui->textBrowser->append("Click");


    RancherAPI *http= new RancherAPI (this);
    connect(http,SIGNAL(requestError(int,QString*)),this,SLOT(OnError(int,QString*)));
    connect(http,SIGNAL(requestResult(QString*)),this,SLOT(OnResult(QString*)));

    QNetworkRequest *request = new QNetworkRequest(QUrl("http://dion.top:8020/v2-beta/projects/1a5/hosts"));
    request->setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0 "));
    request->setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request->setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg("admin").arg("password").toLocal8Bit()).toBase64());
    http->GET(QUrl("") , *request);
}
