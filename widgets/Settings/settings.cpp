#include "settings.h"
#include "ui_settings.h"

Settings::Settings(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

   connect(ui->widget,SIGNAL(onStateChanged(bool)),this,SLOT(onSwitchStateChanged(bool)));
}
void Settings::paintEvent(QPaintEvent *)
 {
     QStyleOption opt;
     opt.init(this);
     QPainter p(this);
     style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
 }
void Settings::onSwitchStateChanged(bool state){
qDebug()<<state;
}
Settings::~Settings()
{
    delete ui;
}



