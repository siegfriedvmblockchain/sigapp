#include "perfomance.h"
#include "ui_perfomance.h"
#include <QPainter>
Perfomance::Perfomance(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Perfomance)
{
    ui->setupUi(this);
}

Perfomance::~Perfomance()
{
    delete ui;
}

void Perfomance::paintEvent(QPaintEvent *)
 {
     QStyleOption opt;
     opt.init(this);
     QPainter p(this);
     style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
 }

void Perfomance::on_BtnSent_clicked()
{
    if (ClientStarted){
    NewSessionRequestModel *req_model= new NewSessionRequestModel();
    req_model->sig_api="1.0";
    req_model->timestamp=QDateTime::currentDateTime();//.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    req_model->blockchain.destination_id="a92c52a99f70f3680f8867731a8027516e06e7e05b9cc17bef7ebb";
    req_model->blockchain.source_id="f954d198370b046b5d54a474e157cfcca3ae24f03db66b2f954dfe";
    qDebug()<<req_model->blockchain.destination_id;
    try{
        auto msg= client->NewSession(*req_model);
        QString response=msg;
        ui->textBrowser->clear();
        ui->textBrowser->setText(response);
    }catch(...) {
        string res = grpc::Status().error_message().c_str();
        qDebug()<<"FATAL Grpc call:"<<QString::fromStdString(res);
    };

   }
}

void Perfomance::on_BtnStartClient_clicked()
{
    if (!ClientStarted){
        client= new SigVmClient(CreateChannel("127.0.0.1:6060",grpc::InsecureChannelCredentials()));
        StatusCode sc  = grpc::Status().error_code();


        ui->BtnStartClient->text()="Stop client";
        ui->BtnStartClient->repaint();
        ui->textBrowser->setText("Client started");

        ClientStarted=true;
        }
    else{

        ui->BtnStartClient->text()="Start client";
        ui->BtnStartClient->repaint();
        ui->textBrowser->setText("Client stopped");
        ClientStarted=false;
        //~client;
    }
}
