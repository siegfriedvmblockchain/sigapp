#ifndef PERFOMANCE_H
#define PERFOMANCE_H

#include <QWidget>
#include <QDebug>
#include "network/vmclient/sigvmclient.h"

namespace Ui {
class Perfomance;
}

class Perfomance : public QWidget
{
    Q_OBJECT

public:
    explicit Perfomance(QWidget *parent = 0);
    ~Perfomance();

private slots:
    void on_BtnSent_clicked();

    void on_BtnStartClient_clicked();

private:
    Ui::Perfomance *ui;
    SigVmClient *client;
    bool ClientStarted=false;
    void paintEvent(QPaintEvent *);
};

#endif // PERFOMANCE_H
