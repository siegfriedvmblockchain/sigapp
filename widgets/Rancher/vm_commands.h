#ifndef VMCOMMAND_H
#define VMCOMMAND_H
#include "common/rapidjson_wrapper.h"
#include "default.h"
#include <cstdio>
#include <string>
#include <vector>
#include <model/vmdatamodel.h>
#include <QObject>

class VMCommand : public QObject
{
    Q_OBJECT
public:
    explicit VMCommand(QObject *parent = nullptr);
    ContainerStatsUrlModel ParseContainerStatsUrlResponse(QString json);
    ContainerStatsModel ParseContainerStatsResponse(QString json);

    ProjectsListModel ParseProjectsListResponse(QString json);
    ProjectDataModel ParseProjectDataResponse(QString json);
    ContainersListModel ParseContinersListResponse(QString json);
    ContainerDataModel ParseContainerDataResponse(QString json);
    HostsListModel ParseHostsListResponse(QString json);
    HostDataModel ParseHostDataResponse(QString json);
    ServicesListModel ParseServicesListResponse(QString json);
    ServiceDataModel ParseServiceDataResponse(QString json);
private:

signals:

public slots:
};

#endif // VMCOMMAND_H
