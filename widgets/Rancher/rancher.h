#ifndef RANCHER_H
#define RANCHER_H

#include <QWidget>
#include <vm_commands.h>
#include <api/api_rancher.h>
#include <common/appconfig.h>
#include <common/helper.h>
namespace Ui {
class Rancher;
}

class Rancher : public QWidget
{
    Q_OBJECT

public:
    explicit Rancher(QWidget *parent = 0);
    ~Rancher();

private slots:
    void on_pushButton_clicked();
    void OnError(int,QString*);
    void OnResult(ResponseTypeEnum,RancherAPIEnum, QString*);
    void OnConfigError(ErrorMsgEnum,QString*);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

private:
    Ui::Rancher *ui;
    AppConfig *app;
    RancherAPI *client;
    SIGHelper *helper;
    void paintEvent(QPaintEvent *);
    //Data
    ContainerStatsUrlModel containerStatsUrl;
    ContainerStatsModel containerStats;
    ProjectsListModel projectsList;
    ProjectDataModel projectData;
    ContainersListModel containersList;
    ContainerDataModel containerData;
    HostsListModel hostsList;
    HostDataModel hostData;
    ServicesListModel servicesList;
    ServiceDataModel serviceData;
};

#endif // RANCHER_H
