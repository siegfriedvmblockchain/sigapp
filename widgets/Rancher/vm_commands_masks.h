#ifndef VMCOMMANDS_H
#define VMCOMMANDS_H
#include <QString>

//Url Masks
QString ContainerStatsCmd = "/v2-beta/projects/%1/containers/%2/stats";
QString ProjectsCmd ="/v2-beta/projects";
QString ProjectCmd ="/v2-beta/projects/%1";
QString HostsCmd ="/v2-beta/projects/%1/hosts";
QString HostCmd ="/v2-beta/projects/%1/hosts/%2";
QString ServicesCmd ="/v2-beta/projects/%1/services";
QString ServiceCmd ="/v2-beta/projects/%1/services/%2";
QString ContainersCmd ="/v2-beta/projects/%1/containers";
QString ContainerCmd ="/v2-beta/projects/%1/containers/%2";

#endif // VMCOMMANDS_H
