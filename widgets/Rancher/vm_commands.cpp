#include "vm_commands.h"
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>

VMCommand::VMCommand(QObject *parent) : QObject(parent)
{

}


//Parses Container Stats Url JSON Response
ContainerStatsUrlModel VMCommand::ParseContainerStatsUrlResponse(QString json){
    ContainerStatsUrlModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
        res.token=d.value("token").toString();
        res.url=d.value("url").toString();
        res.type=d.value("type").toString();
    return res;
}
//Parses Container Stats  JSON Response
ContainerStatsModel VMCommand::ParseContainerStatsResponse(QString json){
    ContainerStatsModel res;
    rapidjson::Wrapper w;
    //Below lines json Normalize
    json=json.remove(0,1);
    json=json.remove(json.length()-1,1);
    //
    QVariantMap d=w.parse(json.toUtf8());

       res.id=d.value("id").toString();
       res.resourceType=d.value("resourcetype").toString();
       res.timeStamp=d.value("timestamp").toString();
       res.memLimit=d.value("memlimit").toInt();
       QJsonObject cpu=d["cpu"].toJsonObject();
       QJsonObject usage=cpu["usage"].toObject();
           res.cpu.system=usage["system"].toDouble();
           res.cpu.total=usage["total"].toDouble();
           res.cpu.user=usage["user"].toDouble();
       QJsonObject network=d["network"].toJsonObject();
           res.network.name=usage["name"].toString();
           res.network.rx_bytes=usage["rx_bytes"].toInt();
           res.network.rx_dropped=usage["rx_dropped"].toInt();
           res.network.rx_errors=usage["rx_errors"].toInt();
           res.network.rx_packets=usage["rx_packets"].toInt();
           res.network.tx_bytes=usage["tx_bytes"].toInt();
           res.network.tx_dropped=usage["tx_dropped"].toInt();
           res.network.tx_errors=usage["tx_errors"].toInt();
           res.network.tx_packets=usage["tx_packets"].toInt();
       QJsonObject diskio=d["diskio"].toJsonObject();
      //
        QJsonObject memory=d["memory"].toJsonObject();
            res.memoryUsage=memory["memoryusage"].toInt();
    return res;
}


ProjectsListModel VMCommand::ParseProjectsListResponse(QString json){
    ProjectsListModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.type=d.value("type").toString();
    res.resourceType=d.value("resourceType").toString();
    QJsonArray data=d["data"].toJsonArray();
    foreach (const QJsonValue & value, data) {
        QJsonObject obj = value.toObject();
        ProjectDataModel prj;
        prj.id=obj["id"].toString();
        qDebug()<<"id:"<<prj.id;
        prj.name=obj["name"].toString();
        prj.state=obj["state"].toString();
        prj.created=obj["created"].toString();
        prj.defaultNetworkId=obj["defaultNetworkId"].toString();
        prj.projectTemplateId=obj["projectTemplateId"].toString();
        prj.virtualMachine=obj["virtualMachine"].toBool();
        prj.uuid=obj["uuid"].toString();
        prj.removeTime=obj["removeTime"].toString();
        prj.removed=obj["removed"].toString();
        QJsonObject links=obj["links"].toObject();
            prj.links.accountLinks=links["accountLinks"].toString();
            prj.links.hosts=links["hosts"].toString();
            prj.links.services=links["services"].toString();
            prj.links.containers=links["containers"].toString();
            prj.links.ipAddresses=links["ipAddresses"].toString();
            qDebug()<<"ipAddresses:"<<prj.links.ipAddresses;
        QJsonObject actions=obj["actions"].toObject();
            prj.actions.update=actions["update"].toString();
            prj.actions.upgrade=actions["upgrade"].toString();
            prj.actions.detele=actions["delete"].toString();
            prj.actions.deactivate=actions["deactivate"].toString();
            prj.actions.defaultNetworkId=actions["defaultNetworkId"].toString();
            qDebug()<<"defaultNetworkId:"<<prj.actions.defaultNetworkId;

        res.data.append(prj);

    }
    return res;
};

ProjectDataModel VMCommand::ParseProjectDataResponse(QString json){
    ProjectDataModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.id=d.value("id").toString();
    res.name=d.value("name").toString();

    res.state=d.value("state").toString();

    res.created=d.value("created").toString();
    res.defaultNetworkId=d.value("defaultNetworkId").toString();
    res.projectTemplateId=d.value("projectTemplateId").toString();
    res.virtualMachine=d.value("virtualMachine").toBool();
    res.uuid=d.value("uuid").toString();
    res.removeTime=d.value("removeTime").toString();
    res.removed=d.value("removed").toString();
    QJsonObject links=d["links"].toJsonObject();
        res.links.accountLinks=links["accountLinks"].toString();
        res.links.hosts=links["hosts"].toString();
        res.links.services=links["services"].toString();
        res.links.containers=links["containers"].toString();
        res.links.ipAddresses=links["ipAddresses"].toString();


        // ??
        //res.links.rregister=links["rregister"].toString();

    QJsonObject actions=d["actions"].toJsonObject();
        res.actions.update=actions["update"].toString();
        res.actions.upgrade=actions["upgrade"].toString();
        res.actions.detele=actions["delete"].toString();
        res.actions.deactivate=actions["deactivate"].toString();
        res.actions.defaultNetworkId=actions["defaultNetworkId"].toString();

    return res;

};

ContainersListModel VMCommand::ParseContinersListResponse(QString json){
    ContainersListModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.type=d.value("type").toString();
    res.resourceType=d.value("resourceType").toString();
    QJsonArray data=d["data"].toJsonArray();
    foreach (const QJsonValue & value, data) {
        QJsonObject obj = value.toObject();
        ContainerDataModel cont;
        cont.id=obj.value("id").toString();
        cont.name=obj.value("name").toString();
        cont.state=obj.value("state").toString();
        cont.created=obj.value("created").toString();
        cont.createdTS=obj.value("createdTS").toDouble();
        cont.uuid=obj.value("uuid").toString();
        cont.deploymentUnitUuid=obj.value("deploymentUnitUuid").toString();
        cont.externalId=obj.value("externalId").toString();
        cont.firstRunning=obj.value("firstRunning").toString();
        cont.firstRunningTS=obj.value("firstRunningTS").toDouble();
        cont.hostId=obj.value("hostId").toString();
        cont.imageUuid=obj.value("imageUuid").toString();
        cont.nativeContainer=obj.value("nativeContainer").toBool();
        cont.networkMode=obj.value("networkMode").toBool();
        cont.startCount=obj.value("startCount").toInt();
        cont.startOnCreate=obj.value("startOnCreate").toBool();
        cont.stdinOpen=obj.value("stdinOpen").toBool();
        cont.system=obj.value("system").toBool();
        cont.tty=obj.value("tty").toBool();
        cont.uuid=obj.value("uuid").toString();
        QJsonObject links=obj["links"].toObject();
            cont.links.accounts=links["accounts"].toString();
            cont.links.credentials=links["credentials"].toString();
            cont.links.healthcheckInstanceHostMaps=links["healthcheckInstanceHostMaps"].toString();
            cont.links.hosts=links["hosts"].toString();
            cont.links.instanceLabels=links["instanceLabels"].toString();
            cont.links.instanceLinks=links["instanceLinks"].toString();
            cont.links.instances=links["instances"].toString();
            cont.links.mounts=links["mounts"].toString();
            cont.links.ports=links["ports"].toString();
            cont.links.serviceEvents=links["serviceEvents"].toString();
            cont.links.serviceLogs=links["serviceLogs"].toString();
            cont.links.services=links["services"].toString();
            cont.links.stats=links["stats"].toString();
            cont.links.targetInstanceLinks=links["targetInstanceLinks"].toString();
            cont.links.volumes=links["volumes"].toString();
            cont.links.containerStats=links["containerStats"].toString();

        QJsonObject actions=obj["actions"].toObject();
            cont.actions.update=actions["update"].toString();
            cont.actions.stop=actions["stop"].toString();
            cont.actions.migrate=actions["migrate"].toString();
            cont.actions.logs=actions["logs"].toString();
            cont.actions.execute=actions["execute"].toString();
            cont.actions.proxy=actions["proxy"].toString();
            cont.actions.console=actions["console"].toString();

        qDebug()<<"container id:"<<cont.id;
        qDebug()<<"container name:"<<cont.name;
        qDebug()<<"Container console:"<<cont.actions.console;
        res.data.append(cont);
    }
    return res;

};

ContainerDataModel VMCommand::ParseContainerDataResponse(QString json){
    ContainerDataModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.id=d.value("id").toString();
    res.name=d.value("name").toString();
    res.state=d.value("state").toString();
    res.created=d.value("created").toString();
    res.createdTS=d.value("createdTS").toDouble();
    res.uuid=d.value("uuid").toString();
    res.deploymentUnitUuid=d.value("deploymentUnitUuid").toString();
    res.externalId=d.value("externalId").toString();
    res.firstRunning=d.value("firstRunning").toString();
    res.firstRunningTS=d.value("firstRunningTS").toDouble();
    res.hostId=d.value("hostId").toString();
    res.imageUuid=d.value("imageUuid").toString();
    res.nativeContainer=d.value("nativeContainer").toBool();
    res.networkMode=d.value("networkMode").toBool();
    res.startCount=d.value("startCount").toInt();
    res.startOnCreate=d.value("startOnCreate").toBool();
    res.stdinOpen=d.value("stdinOpen").toBool();
    res.system=d.value("system").toBool();
    res.tty=d.value("tty").toBool();
    res.uuid=d.value("uuid").toString();

    QJsonObject links=d["links"].toJsonObject();
        res.links.accounts=links["accounts"].toString();
        res.links.credentials=links["credentials"].toString();
        res.links.healthcheckInstanceHostMaps=links["healthcheckInstanceHostMaps"].toString();
        res.links.hosts=links["hosts"].toString();
        res.links.instanceLabels=links["instanceLabels"].toString();
        res.links.instanceLinks=links["instanceLinks"].toString();
        res.links.instances=links["instances"].toString();
        res.links.mounts=links["mounts"].toString();
        res.links.ports=links["ports"].toString();
        res.links.serviceEvents=links["serviceEvents"].toString();
        res.links.serviceLogs=links["serviceLogs"].toString();
        res.links.services=links["services"].toString();
        res.links.stats=links["stats"].toString();
        res.links.targetInstanceLinks=links["targetInstanceLinks"].toString();
        res.links.volumes=links["volumes"].toString();
        res.links.containerStats=links["containerStats"].toString();

    QJsonObject actions=d["actions"].toJsonObject();
        res.actions.update=actions["update"].toString();
        res.actions.stop=actions["stop"].toString();
        res.actions.migrate=actions["migrate"].toString();
        res.actions.logs=actions["logs"].toString();
        res.actions.execute=actions["execute"].toString();
        res.actions.proxy=actions["proxy"].toString();
        res.actions.console=actions["console"].toString();

    qDebug()<<"Container id:"<<res.id;
    qDebug()<<"Container console:"<<res.actions.console;
    return res;

};

HostsListModel VMCommand::ParseHostsListResponse(QString json){
    HostsListModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.type=d.value("type").toString();
    res.resourceType=d.value("resourceType").toString();
    QJsonArray data=d["data"].toJsonArray();
    foreach (const QJsonValue & value, data) {
        QJsonObject obj = value.toObject();
        HostDataModel host;
        host.id=obj["id"].toString();
        host.type=obj.value("type").toString();
        host.baseType=obj.value("baseType").toString();
        host.name=obj.value("name").toString();
        host.state=obj.value("state").toString();
        host.accountId=obj.value("accountId").toString();
        host.agentIpAddress=obj.value("agentIpAddress").toString();
        host.agentState=obj.value("agentState").toString();
        host.computeTotal=obj.value("computeTotal").toString();
        host.created=obj.value("created").toString();
        host.createdTS=obj.value("createdTS").toDouble();
        host.hostname=obj.value("hostname").toString();
        host.kind=obj.value("kind").toString();
        host.localStorageMb=obj.value("localStorageMb").toDouble();
        host.memory=obj.value("memory").toDouble();
        host.milliCpu=obj.value("milliCpu").toInt();
        host.physicalHostId=obj.value("physicalHostId").toString();
        host.uuid=obj.value("uuid").toString();

        QJsonObject links=obj["links"].toObject();
            host.links.healthcheckInstanceHostMaps=links["healthcheckInstanceHostMaps"].toString();
            host.links.account=links["account"].toString();
            host.links.clusters=links["clusters"].toString();
            host.links.containerEvents=links["containerEvents"].toString();
            host.links.containerStats=links["containerStats"].toString();
            host.links.healthcheckInstanceHostMaps=links["healthcheckInstanceHostMaps"].toString();
            host.links.hostLabels=links["hostLabels"].toString();
            host.links.hosts=links["hosts"].toString();
            host.links.hostStats=links["hostStats"].toString();
            host.links.instances=links["instances"].toString();
            host.links.ipAddresses=links["ipAddresses"].toString();
            host.links.physicalHost=links["physicalHost"].toString();
            host.links.serviceEvents=links["serviceEvents"].toString();
            host.links.storagePools=links["storagePools"].toString();
            host.links.stats=links["stats"].toString();
            host.links.volumes=links["volumes"].toString();

            QJsonObject actions=obj["actions"].toObject();
                host.actions.update=actions["update"].toString();
                host.actions.upgrade=actions["upgrade"].toString();
                host.actions.evacuate=actions["evacuate"].toString();
                host.actions.dockersocket=actions["dockersocket"].toString();
                host.actions.deactivate=actions["deactivate"].toString();
                host.actions.hdelete=actions["hdelete"].toString();

            QJsonObject info=obj["info"].toObject();
                QJsonObject memInfo=info["memoryInfo"].toObject();
                    host.info.memoryInfo.active=memInfo["active"].toDouble();
                    host.info.memoryInfo.buffers=memInfo["buffers"].toDouble();
                    host.info.memoryInfo.cached=memInfo["cached"].toDouble();
                    host.info.memoryInfo.inactive=memInfo["inactive"].toDouble();
                    host.info.memoryInfo.memAvailable=memInfo["memAvailable"].toDouble();
                    host.info.memoryInfo.memFree=memInfo["memFree"].toDouble();
                    host.info.memoryInfo.memTotal=memInfo["memTotal"].toDouble();
                    host.info.memoryInfo.swapCached=memInfo["swapCached"].toDouble();
                    host.info.memoryInfo.swapfree=memInfo["swapfree"].toDouble();
                    host.info.memoryInfo.swaptotal=memInfo["swaptotal"].toDouble();
                QJsonObject osInfo=obj["osInfo"].toObject();
                    host.info.osInfo.dockerVersion=osInfo["dockerVersion"].toString();
                    host.info.osInfo.kernelVersion=osInfo["kernelVersion"].toString();
                    host.info.osInfo.operatingSystem=osInfo["operatingSystem"].toString();
                QJsonObject diskInfo=obj["diskInfo"].toObject();
                    host.info.diskInfo.capacity=diskInfo["capacity"].toDouble();
                    host.info.diskInfo.free=diskInfo["free"].toDouble();
                    host.info.diskInfo.percentage=diskInfo["percentage"].toDouble();
                    host.info.diskInfo.total=diskInfo["total"].toDouble();
                    host.info.diskInfo.used=diskInfo["used"].toDouble();
                QJsonObject hostKey=obj["hostKey"].toObject();
                    host.info.hostKey.data=hostKey["data"].toString();
                QJsonObject cpuInfo=obj["cpuInfo"].toObject();
                    host.info.cpuInfo.count=cpuInfo["count"].toInt();
                    host.info.cpuInfo.mhz=cpuInfo["mhz"].toInt();
                    host.info.cpuInfo.modelName=cpuInfo["modelName"].toInt();
                    QJsonArray data=obj["cpuCoresPercentages"].toArray();

                    foreach (const QJsonValue & value, data) {
                        float i;
                        i=1.1;
                        // host.info.cpuInfo.cpuCoresPercentages.append(value.to);
                    }



        qDebug()<<"host id:"<<host.id;
        qDebug()<<"host name:"<<host.name;
        qDebug()<<"host stats:"<<host.links.hostStats;
        qDebug()<<"host update actions:"<<host.actions.update;
        qDebug()<<"host memory active:"<<host.info.memoryInfo.active;
        qDebug()<<"host OS name:"<<host.info.osInfo.operatingSystem;
        qDebug()<<"host host key:"<<host.info.hostKey.data;
        qDebug()<<"host CPU Info model name:"<<host.info.cpuInfo.modelName;
        qDebug()<<"host CPU core percentage:"<<host.info.cpuInfo.cpuCoresPercentages;
        res.data.append(host);
     }
    return res;

};

HostDataModel VMCommand::ParseHostDataResponse(QString json){
    HostDataModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.id=d.value("id").toString();
    res.type=d.value("type").toString();
    res.baseType=d.value("baseType").toString();
    res.name=d.value("name").toString();
    res.state=d.value("state").toString();
    res.accountId=d.value("accountId").toString();
    res.agentIpAddress=d.value("agentIpAddress").toString();
    res.agentState=d.value("agentState").toString();
    res.computeTotal=d.value("computeTotal").toString();
    res.created=d.value("created").toString();
    res.createdTS=d.value("createdTS").toDouble();
    res.hostname=d.value("hostname").toString();
    res.kind=d.value("kind").toString();
    res.localStorageMb=d.value("localStorageMb").toDouble();
    res.memory=d.value("memory").toDouble();
    res.milliCpu=d.value("milliCpu").toInt();
    res.physicalHostId=d.value("physicalHostId").toString();
    res.uuid=d.value("uuid").toString();

    QJsonObject links=d["links"].toJsonObject();
        res.links.healthcheckInstanceHostMaps=links["healthcheckInstanceHostMaps"].toString();
        res.links.account=links["account"].toString();
        res.links.clusters=links["clusters"].toString();
        res.links.containerEvents=links["containerEvents"].toString();
        res.links.containerStats=links["containerStats"].toString();
        res.links.healthcheckInstanceHostMaps=links["healthcheckInstanceHostMaps"].toString();
        res.links.hostLabels=links["hostLabels"].toString();
        res.links.hosts=links["hosts"].toString();
        res.links.hostStats=links["hostStats"].toString();
        res.links.instances=links["instances"].toString();
        res.links.ipAddresses=links["ipAddresses"].toString();
        res.links.physicalHost=links["physicalHost"].toString();
        res.links.serviceEvents=links["serviceEvents"].toString();
        res.links.storagePools=links["storagePools"].toString();
        res.links.stats=links["stats"].toString();
        res.links.volumes=links["volumes"].toString();

    QJsonObject actions=d["actions"].toJsonObject();
        res.actions.update=actions["update"].toString();
        res.actions.upgrade=actions["upgrade"].toString();
        res.actions.evacuate=actions["evacuate"].toString();
        res.actions.dockersocket=actions["dockersocket"].toString();
        res.actions.deactivate=actions["deactivate"].toString();
        res.actions.hdelete=actions["hdelete"].toString();

    QJsonObject info=d["info"].toJsonObject();
        QJsonObject memInfo=info["memoryInfo"].toObject();
            res.info.memoryInfo.active=memInfo["active"].toDouble();
            res.info.memoryInfo.buffers=memInfo["buffers"].toDouble();
            res.info.memoryInfo.cached=memInfo["cached"].toDouble();
            res.info.memoryInfo.inactive=memInfo["inactive"].toDouble();
            res.info.memoryInfo.memAvailable=memInfo["memAvailable"].toDouble();
            res.info.memoryInfo.memFree=memInfo["memFree"].toDouble();
            res.info.memoryInfo.memTotal=memInfo["memTotal"].toDouble();
            res.info.memoryInfo.swapCached=memInfo["swapCached"].toDouble();
            res.info.memoryInfo.swapfree=memInfo["swapfree"].toDouble();
            res.info.memoryInfo.swaptotal=memInfo["swaptotal"].toDouble();
        QJsonObject osInfo=d["osInfo"].toJsonObject();
            res.info.osInfo.dockerVersion=osInfo["dockerVersion"].toString();
            res.info.osInfo.kernelVersion=osInfo["kernelVersion"].toString();
            res.info.osInfo.operatingSystem=osInfo["operatingSystem"].toString();
        QJsonObject diskInfo=d["diskInfo"].toJsonObject();
            res.info.diskInfo.capacity=diskInfo["capacity"].toDouble();
            res.info.diskInfo.free=diskInfo["free"].toDouble();
            res.info.diskInfo.percentage=diskInfo["percentage"].toDouble();
            res.info.diskInfo.total=diskInfo["total"].toDouble();
            res.info.diskInfo.used=diskInfo["used"].toDouble();
        QJsonObject hostKey=d["hostKey"].toJsonObject();
            res.info.hostKey.data=hostKey["data"].toString();
        QJsonObject cpuInfo=d["cpuInfo"].toJsonObject();
            res.info.cpuInfo.count=cpuInfo["count"].toInt();
            res.info.cpuInfo.mhz=cpuInfo["mhz"].toInt();  //
            res.info.cpuInfo.modelName=cpuInfo["modelName"].toInt();
            QJsonArray data=d["cpuCoresPercentages"].toJsonArray();
//            foreach (const JSonValue & i, data) {
//                res.info.cpuInfo.cpuCoresPercentages.append(i);
//            }

    return res;

};


ServicesListModel VMCommand::ParseServicesListResponse(QString json){
    ServicesListModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(json.toUtf8());
    res.type=d.value("type").toString();
    res.resourceType=d.value("resourceType").toString();
    QJsonArray data=d["data"].toJsonArray();
    foreach (const QJsonValue & value, data) {
        QJsonObject obj = value.toObject();
        ServiceDataModel serv;
        serv.id=obj["id"].toString();
        serv.name=obj["name"].toString();
        serv.accountId=obj["accountId"].toString();
        serv.assignServiceIpAddress=obj["assignServiceIpAddress"].toBool();
        serv.baseType=obj["baseType"].toString();
        serv.created=obj["created"].toString();
        serv.createdTS=obj["createdTS"].toDouble();
        serv.createIndex=obj["createIndex"].toInt();
        serv.currentScale=obj["currentScale"].toInt();
        serv.healthState=obj["healthState"].toString();
        serv.kind=obj["kind"].toString();
        serv.stackId=obj["stackId"].toString();
        serv.scale=obj["scale"].toInt();
        serv.startOnCreate=obj["startOnCreate"].toBool();
        serv.system=obj["system"].toBool();
        serv.uuid=obj["uuid"].toBool();

        QJsonObject links=obj["links"].toObject();
            serv.links.account=links["account"].toString();
            serv.links.consumedbyservices=links["consumedbyservices"].toString();
            serv.links.consumedservices=links["consumedservices"].toString();
            serv.links.containerStats=links["containerStats"].toString();
            serv.links.instances=links["instances"].toString();
            serv.links.networkDrivers=links["networkDrivers"].toString();
            serv.links.serviceExposeMaps=links["serviceExposeMaps"].toString();
            serv.links.serviceLogs=links["serviceLogs"].toString();
            serv.links.stack=links["stack"].toString();
            serv.links.storageDrivers=links["storageDrivers"].toString();

        QJsonObject actions=obj["actions"].toObject();
            serv.actions.update=actions["update"].toString();
            serv.actions.upgrade=actions["upgrade"].toString();
            serv.actions.restart=actions["restart"].toString();
            serv.actions.remove=actions["remove"].toString();
            serv.actions.deactivate=actions["deactivate"].toString();
            serv.actions.removeservicelink=actions["removeservicelink"].toString();
            serv.actions.addservicelink=actions["addservicelink"].toString();
            serv.actions.setservicelinks=actions["setservicelinks"].toString();

        QJsonObject lCfg=obj["launchConfig"].toObject();
            serv.launchConfig.tty=lCfg["tty"].toBool();
            serv.launchConfig.imageUuid=lCfg["imageUuid"].toString();

        res.data.append(serv);

        qDebug()<<"service id:"<<serv.id;
        qDebug()<<"service name:"<<serv.name;
        qDebug()<<"service links containerStats:"<<serv.links.containerStats;
        qDebug()<<"service actions setservicelinks:"<<serv.actions.setservicelinks;
        qDebug()<<"service launch config tty:"<<serv.launchConfig.tty;

    }
    return res;

};

ServiceDataModel VMCommand::ParseServiceDataResponse(QString json){
    ServiceDataModel res;
    rapidjson::Wrapper w;

    return res;

};
