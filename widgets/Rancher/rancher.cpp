#include "rancher.h"
#include "ui_rancher.h"
#include <QPainter>
Rancher::Rancher(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Rancher)
{
    ui->setupUi(this);

    helper->SetFont(this);

    app= new AppConfig(this);


    connect (app,SIGNAL(ConfigurationError(ErrorMsgEnum,QString*)),this,SLOT(OnConfigError(ErrorMsgEnum,QString*)));
            client= new RancherAPI (this,app->Configuration);
    connect(client,SIGNAL(Response(ResponseTypeEnum,RancherAPIEnum, QString*)),this,SLOT(OnResult(ResponseTypeEnum,RancherAPIEnum,QString*)));
    connect(client,SIGNAL(ApiError(int,QString*)),this,SLOT(OnError(int,QString*)));

}
void Rancher::paintEvent(QPaintEvent *)
 {
     QStyleOption opt;
     opt.init(this);
     QPainter p(this);
     style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
 }
Rancher::~Rancher()
{
    client->~RancherAPI();
    delete ui;
}

void Rancher::on_pushButton_clicked()
{
    ui->plainTextEdit->appendPlainText("Get projects list ===========>>>");
    client->GetProjects();
}

void Rancher::OnError(int cmd, QString *msg){
    auto txt=QString("Error %1 : %2").arg(cmd).arg(msg->toStdString().c_str());
    ui->plainTextEdit->appendPlainText(txt);
}

void Rancher::OnResult(ResponseTypeEnum type,RancherAPIEnum api,QString *msg)
{
    VMCommand *vmc= new VMCommand();
    QString json=msg->toStdString().c_str();

    if(type==ResponseTypeEnum::HTTP){
    switch (api) {
    case RancherAPIEnum::Container:
    {
        containerData= vmc->ParseContainerDataResponse(json);
    }
        break;
    case RancherAPIEnum::Containers:
    {
        containersList= vmc->ParseContinersListResponse(json);
    }
        break;
    case RancherAPIEnum::ContainerStats:
    {
      ContainerStatsUrlModel contStatsUrl= vmc->ParseContainerStatsUrlResponse(json);
       client->WsConnect(contStatsUrl.url,contStatsUrl.token);
      //  containerStatsUrl= vmc->ParseContainerStatsUrlResponse(json);
       // client->WsConnect(containerStatsUrl.url,containerStatsUrl.token);
    }
        break;
    case RancherAPIEnum::Services:
    {
        servicesList= vmc->ParseServicesListResponse(json);
    }
        break;
    case RancherAPIEnum::Hosts:
    {
        hostsList= vmc->ParseHostsListResponse(json);
    }
        break;
    case RancherAPIEnum::Project:
    {
        projectData= vmc->ParseProjectDataResponse(json);
    }
        break;
    case RancherAPIEnum::Projects:
    {
        projectsList= vmc->ParseProjectsListResponse(json);
    }
        break;
    case RancherAPIEnum::Host:
    {
        hostData= vmc->ParseHostDataResponse(json);
    }
        break;
    case RancherAPIEnum::Service:
    {
        serviceData= vmc->ParseServiceDataResponse(json);
    }
        break;
    default:
        break;
    }
    ui->plainTextEdit->appendPlainText(msg->toStdString().c_str());
    }
    if(type==ResponseTypeEnum::WS){
        containerStats = vmc->ParseContainerStatsResponse(json);
        qDebug()<<containerStats.id;
    }
}

void Rancher::on_pushButton_2_clicked()
{
    ui->plainTextEdit->appendPlainText("Get project ===========>>>");
    client->GetProject("1a5");
}

void Rancher::on_pushButton_3_clicked()
{
    ui->plainTextEdit->appendPlainText("Get containers list ===========>>>");
    client->GetContainers("1a5");
}

void Rancher::on_pushButton_4_clicked()
{
    ui->plainTextEdit->appendPlainText("Get hosts list ===========>>>");
    client->GetHosts("1a5");
}

void Rancher::on_pushButton_5_clicked()
{
    ui->plainTextEdit->appendPlainText("Get services list ===========>>>");
    client->GetServices("1a5");
}

void Rancher::on_pushButton_6_clicked()
{
    ui->plainTextEdit->appendPlainText("Get container ===========>>>");
    client->GetContainer("1a5","1i57");
}

void Rancher::on_pushButton_7_clicked()
{
    ui->plainTextEdit->appendPlainText("Get container stats ===========>>>");
    //client->GetContainerStats("1a5","1i241"); // Dokuwiki
    //client->GetContainerStats("1a5","1s1"); // Sheduler
    //client->GetContainerStats("1a5","1s2"); // Healthcheck - failed
    client->GetContainerStats("1a5","1i57");
}

void Rancher::OnConfigError(ErrorMsgEnum type, QString *msg){

}
