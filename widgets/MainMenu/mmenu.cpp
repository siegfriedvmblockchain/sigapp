#include "mmenu.h"
#include "ui_mmenu.h"
#include <QStyle>
#include <QPushButton>
#include <QStyle>

mMenu::mMenu(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mMenu)
{

    ui->setupUi(this);

}
void mMenu::Init(bool isminer){
    IsMiner=isminer;
    if (IsMiner){
        ui->buttonA->setIcon(QIcon(":/sig_icons/sig/icon_mining.png"));
        ui->buttonA->setText("Mining");
    }else{
        ui->buttonA->setIcon(QIcon(":/sig_icons/sig/icon_perfomance.png"));
        ui->buttonA->setText("Perfomace");
    }
        ui->buttonB->setIcon(QIcon(":/sig_icons/sig/icon_wallet.png"));
        ui->buttonB->setText("Wallet");
        ui->buttonC->setIcon(QIcon(":/sig_icons/sig/icon_settings.png"));
        ui->buttonC->setText("Settings");
        ui->buttonD->setIcon(QIcon(":/sig_icons/sig/icon_faq.png"));
        ui->buttonD->setText("FAQ");
        ui->labelVer->setText("Alpha 0.0.1");
}
void mMenu::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);

}


mMenu::~mMenu()
{
    delete ui;
}


void mMenu::on_buttonA_clicked()
{
    if (IsMiner){
      emit menuAction(0);
    }else{
       emit menuAction(1);
    }
}

void mMenu::on_buttonB_clicked()
{
     emit menuAction(2);
}

void mMenu::on_buttonC_clicked()
{
     emit menuAction(3);
}

void mMenu::on_buttonD_clicked()
{
     emit menuAction(4);
}

