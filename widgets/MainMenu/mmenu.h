#ifndef MMENU_H
#define MMENU_H
#include <QPainter>
#include <QWidget>
#include <QIcon>
#include <QString>

namespace Ui {
class mMenu;
}

class mMenu : public QWidget
{
    Q_OBJECT

public:
    explicit mMenu(QWidget *parent = 0);
    ~mMenu();
    struct Settings{
      QIcon icon;
      QString text;

    };
    void Init(bool);
    bool IsMiner;
private slots:

    void on_buttonA_clicked();

    void on_buttonB_clicked();

    void on_buttonC_clicked();

    void on_buttonD_clicked();
signals:
  void menuAction(int action);
private:
    Ui::mMenu *ui;
    void paintEvent(QPaintEvent *);
};

#endif // MMENU_H
