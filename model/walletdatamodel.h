#ifndef WALLETDATAMODEL_H
#define WALLETDATAMODEL_H
#include <QString>

struct ErrorDetails{
  int code;
  QString message;
};
struct ErrorModel{
    QString result;
    int id;
    ErrorDetails error;
};
struct BalancesModel{
   QString accountname;
   double immaturecoinbaserewards;
   double immaturestakegeneration;
   int lockedbytickets;
   double spendable;
   double total;
   double unconfirmed;
   bool votingauthority;
};
struct ResultsModel{
    QList<BalancesModel> balances;
    QString blockhash;
};
struct  BalanceModel{
    int id;
    ErrorDetails error;
    ResultsModel result;
};

struct NewAddressModel{
    int id;
    QString result;
    ErrorDetails error;
};
struct SendToModel{
    int id;
    QString result;
    ErrorDetails error;
};

struct StakeInfoModel{
    int id;
    QString result;
    ErrorDetails error;
};

#endif // WALLETDATAMODEL_H
