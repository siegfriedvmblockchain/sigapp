#ifndef VMDATAMODEL_H
#define VMDATAMODEL_H
#include <QString>
struct ContainerStatsUrlModel{
    QString type;
    QString token;
    QString url;
};
struct StatsCPU{
    double total;
    double user;
    double system;
};
struct StatsDisk{

};
struct StatsNetwork{
    QString name;
    int rx_bytes;
    int rx_packets;
    int rx_errors;
    int rx_dropped;
    int tx_bytes;
    int tx_packets;
    int tx_errors;
    int tx_dropped;
};
struct ContainerStatsModel{
    QString id;
    QString resourceType;
    int memLimit;
    QString timeStamp;
    StatsCPU cpu;
    StatsDisk diskIo;
    StatsNetwork network;
    int memoryUsage;
};

struct ProjectDataLinks{
    QString self;
    QString accountLinks;
    QString agents;
    QString auditLogs;
    QString backupTargets;
    QString backups;
    QString sertificates;
    QString configItemStatuses;
    QString containerEvents;
    QString credentials;
    QString externalEvents;
    QString genericObjects;
    QString healthcheckInstanceHostMaps;
    QString hostTemplates;
    QString hosts;
    QString images;
    QString instanceLinks;
    QString instances;
    QString ipAddresses;
    QString labels;
    QString mounts;
    QString networkDrivers;
    QString networks;
    QString physicalHosts;
    QString ports;
    QString processInstances;
    QString projectMembers;
    QString projectTemplate;
    QString projectTemplates;
    QString regions;
    QString scheduledUpgrades;
    QString secrets;
    QString serviceConsumeMaps;
    QString serviceEvents;
    QString serviceExposeMaps;
    QString serviceLogs;
    QString services;
    QString snapshots;
    QString stacks;
    QString storageDrivers;
    QString storagePools;
    QString subnets;
    QString userPreferences;
    QString volumeTemplates;
    QString volumes;
    QString accounts;
    QString apiKeys;
    QString azureadconfigs;
    QString clusterMemberships;
    QString composeProjects;
    QString composeServices;
    QString containers;
    QString databasechangeloglocks;
    QString configItems;
    QString databasechangelogs;
    QString dnsServices;
    QString extensionPoints;
    QString externalCredentials;
    QString externalDnsEvents;
    QString externalHandlerExternalHandlerProcessMaps;
    QString externalHandlerProcesses;
    QString externalHostEvents;
    QString externalServiceEvents;
    QString externalServices;
    QString externalStoragePoolEvents;
    QString externalVolumeEvents;
    QString haConfigInputs;
    QString haConfigs;
    QString hostApiProxyTokens;
    QString identities;
    QString kubernetesServices;
    QString loadBalancerServices;
    QString localAuthConfigs;
    QString machineDrivers;
    QString machines;
    QString networkDriverServices;
    QString networkPolicyRuleWithins;
    QString kubernetesStacks;
    QString openldapconfigs;
    QString passwords;
    QString processDefinitions;
    QString processExecutions;
    QString processPools;
    QString processSummary;
    QString projects;
    QString pullTasks;
    QString rregister; // register reserved
    QString registrationTokens;
    QString registries;
    QString registryCredentials;
    QString resourceDefinitions;
    QString schemas;
    QString serviceProxies;
    QString settings;
    QString storageDriverServices;
    QString taskInstances;
    QString tasks;
    QString typeDocumentations;
    QString virtualMachines;
    QString hostStats;
};

struct ProjectDataActions{
    QString upgrade;
    QString update;
    QString deactivate;
    QString setmembers;
    QString detele;
    QString defaultNetworkId;
};


struct ProjectDataModel{
    QString id;
    ProjectDataLinks links;
    ProjectDataActions actions;
    QString name;
    QString state;
    QString created;
    QString defaultNetworkId;
    QString projectTemplateId;
    bool virtualMachine;
    QString uuid;
    QString removeTime;
    QString removed;
};

struct ProjectsListModel{
    QString type;
    QString resourceType;
    QList<ProjectDataModel> data;
};

struct ContainerDataLinks{
    QString self;
    QString accounts;
    QString credentials;
    QString healthcheckInstanceHostMaps;
    QString hosts;
    QString instanceLabels;
    QString instanceLinks;
    QString instances;
    QString mounts;
    QString ports;
    QString serviceEvents;
    QString serviceLogs;
    QString services;
    QString targetInstanceLinks;
    QString volumes;
    QString stats;
    QString containerStats;
};

struct ContainerDataActions{
    QString restart;
    QString update;
    QString stop;
    QString migrate;
    QString logs;
    QString execute;
    QString proxy;
    // may be, its full set of actions..
    QString allocate;
    QString console;
    QString start;
    QString create;
    QString error;
    QString deallocate;
    QString purge;
    QString remove;
    QString updatehealthy;
    QString updatereinitializing;
    QString updateunhealthy;
};

struct ContainerDataModel{
    QString id;
    QString type;
    ContainerDataLinks links;
    ContainerDataActions actions;
    QString name;
    QString state;
    QString accountId;
    QList<QString> command;
    QString created;
    double createdTS;
    QList<QString> dataVolumes;
    QString deploymentUnitUuid;
    QString externalId;
    QString firstRunning;
    double firstRunningTS;
    QString hostId;

    QString imageUuid;
    // RancherLabels labels;
    // ContainerLogConfig logConfig;
    // ContainerMounts mounts;
    bool nativeContainer;
    QString networkMode;

    QList<QString> serviceIds;
    int startCount;
    bool startOnCreate;
    bool stdinOpen;
    bool system;
    bool tty;
    QString uuid;
};

struct ContainersListModel{
    QString type;
    QString resourceType;
    // ContainersLinks links;
    // ContainersCreateType createType;
    QList<ContainerDataModel> data;
    // ContainerSortLinks sortLinks;
    // ContainerPagination pagination;
    // ContainerFilters filters;
};

struct HostDataActions{
    QString upgrade;
    QString evacuate;
    QString dockersocket;
    QString update;
    QString hdelete;
    QString deactivate;
    //
    QString activate;
    QString create;
    QString error;
    QString provision;
    QString purge;
    QString remove;
};

struct HostDataLinks{
    QString self;
    QString account;
    QString clusters;
    QString containerEvents;
    QString healthcheckInstanceHostMaps;
    QString hostLabels;
    QString hosts;
    QString instances;
    QString ipAddresses;
    QString physicalHost;
    QString serviceEvents;
    QString storagePools;
    QString volumes;
    QString stats;
    QString hostStats;
    QString containerStats;
};

struct HostDataMemoryInfo{
    long active;
    long buffers;
    long cached;
    long inactive;
    long memAvailable;
    long memFree;
    long memTotal;
    long swapCached;
    long swapfree;
    long swaptotal;
};

struct HostDataOsInfo{
    QString dockerVersion;
    QString kernelVersion;
    QString operatingSystem;
};

struct HostDataHostKey{
    QString data;
};

struct HostDataDiskInfo{
    // ??
    double capacity;
    double free;
    double percentage;
    double total;
    double used;
};

struct HostDataCpuInfo{
    int count;
    // ??
    QList<float> cpuCoresPercentages;
    // ??
    QList<QString> loadAvg;
    int mhz;
    QString modelName;
};

struct HostDataInfo{
    HostDataMemoryInfo memoryInfo;
    HostDataOsInfo osInfo;
    HostDataHostKey hostKey;
    HostDataDiskInfo diskInfo;
    HostDataCpuInfo cpuInfo;
};

struct HostDataEndpoint{
    QString type;
    QString hostId;
    QString instanceId;
    QString ipAddress;
    int port;
    QString serviceId;
};

struct HostDataModel{
    QString id;
    QString type;
    HostDataActions actions;
    HostDataLinks links;
    QString baseType;
    QString name;
    QString state;
    QString accountId;
    QString agentIpAddress;
    QString agentState;
    QString computeTotal;
    QString created;
    QString createdTS;
    QString hostname;
    HostDataInfo info;
    QList<QString> instanceIds;
    QString kind;
    // HostDataLabels labels;
    double localStorageMb;
    double memory;
    int milliCpu;
    QString physicalHostId;
    QList<HostDataEndpoint> publicEndpoints;
    QString uuid;
};

struct HostsListModel{
    QString type;
    QString resourceType;
    // HostsLinks links;
    // HostsCreateType createType;
    QList<HostDataModel> data;
    // HostSortLinks sortLinks;
    // HostPagination pagination;
    // HostFilters filters;
};

struct ServiceDataLinks{
    QString self;
    QString account;
    QString consumedbyservices;
    QString consumedservices;
    QString instances;
    QString networkDrivers;
    QString serviceExposeMaps;
    QString serviceLogs;
    QString stack;
    QString storageDrivers;
    QString containerStats;
};

struct ServiceDataActions{
    QString upgrade;
    QString restart;
    QString update;
    QString remove;
    QString deactivate;
    QString removeservicelink;
    QString addservicelink;
    QString setservicelinks;
    // hidden
    QString actvate;
    QString cancelupgrade;
    QString continueupgrade;
    QString create;
    QString finishupgrade;
    QString rollback;
};

struct ServiceDataHealthCheck{
    QString type;
    int healthyThreshold;
    double initializingTimeout;
    double interval;
    int port;
    double reinitializingTimeout;
    double responseTimeout;
    QString strategy;
    int unhealthyThreshold;
};

struct ServiceDataLaunchConfig{
    QString type;
    QList<QString> command;
    ServiceDataHealthCheck healthCheck;
    QString imageUuid;
    QString instanceTriggeredStop;
    QString kind;
    // ServiceDataLabels labels;
    // ServiceDataLogConfig logConfig;
    QString networkMode;
    bool privileged;
    bool publishAllPorts;
    bool readOnly;
    bool runInit;
    bool startOnCreate;
    bool stdinOpen;
    bool system;
    bool tty;
    QString version;
    int vcpu;
    int drainTimeoutMs;
};

struct ServiceDataModel{
    QString id;
    QString type;
    ServiceDataLinks links;
    ServiceDataActions actions;
    QString baseType;
    QString name;
    QString state;
    QString accountId;
    bool assignServiceIpAddress;
    int createIndex;
    QString created;
    double createdTS;
    int currentScale;
    QString healthState;
    QString kind;
    ServiceDataLaunchConfig launchConfig;
    QString stackId;
    int scale;
    bool startOnCreate;
    bool system;
    QString uuid;
};




struct ServicesListModel{
    QString type;
    QString resourceType;
    // ServicesLinks links;
    // ServicesCreateType createType;
    QList<ServiceDataModel> data;
    // ServicesSortLinks sortLinks;
    // ServicesPagination pagination;
    // ServicesFilters filters;
};


#endif // VMDATAMODEL_H

