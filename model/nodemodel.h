#ifndef NODEMODEL_H
#define NODEMODEL_H
#include <QString>
#include <QList>
#include <QDateTime>

//New session data models
struct BlockChainModel{
    QString source_id;          // VN client blockchain id
    QString destination_id;     // Resource Owner blockchain id
};

struct NewSessionRequestModel{
    QString sig_api;            // API version
    BlockChainModel blockchain;
    QDateTime timestamp;
};

struct CpuInfoModel{
    int count;
    int mhz;
    QString modelName;
};

struct StorageInfoModel{
    int localStorageMb;
    int memoryMb;
};

struct ContainersInfoModel{
    int count;
    QList<QString> ids;
};

struct ProjectsInfoModel{
    int count;
    QList<QString> ids;
};

struct HostsInfoModel{
    int count;
    QList<QString> ids;
};

struct NewSessionResponseModel{
    QString sig_api;
    QString status;
    QString destination_id;
    QString session_id;
    QDateTime timestamp;
    CpuInfoModel cpuInfo;
    StorageInfoModel storageInfo;
    ContainersInfoModel containers;
};
//Node status data models
struct GetNodeStatusRequestModel{
    QString sig_api;
    QString session_id;
    QDateTime timestamp;
};

struct GetNodeStatusResponseModel{
    QString sig_api;
    QString status;
    QString destination_id;
    QDateTime timestamp;
    ProjectsInfoModel projects;
    HostsInfoModel hosts;
    ContainersInfoModel containers;
};
// VM list data models
struct GetVMListRequestModel{
    QString sig_api;
    QString session_id;
    QString host_id;
    QString container_id;
    QDateTime timestamp;
};
/*"restart",
"update",
"stop",
"logs",
"execute"*/
enum class ContainerActionsEnum {restartAction,updateAction,cStopAction,logAction,executeAction};

struct ContainersVMList{
    QString id;
    QList<ContainerActionsEnum> actions;
};

struct HostsVMList{
    QString id;
    QList<ContainersVMList> containers;
};

struct GetVMListResponseModel{
    QString sig_api;
    QList<HostsVMList> hosts;
    QDateTime timestamp;
};
//  OS Images data models
struct GetOSImageRequestModel{
    QString sig_api;
    QString session_id;
    QDateTime timestamp;
};

struct OSImageInfo{
    QString id;
    QString name;
    QString description;
};

struct GetOSImageResponseModel{
    QString sig_api;
    QList<OSImageInfo> osImages;
    QDateTime timestamp;
};
//
//VM Actions data model
//Run,stop,delete
enum vmActionsEnums {runAction,stopAction, deleteAction,createAction};
enum vmStateEnums {runningState, restorringState, removingState};
struct ActionVMRequestModel{
    QString sig_api;
    QString session_id;
    QString host_id;
    QString container_id;
    vmActionsEnums action;
    int timeout;
    QDateTime timestamp;
};

struct ActionVMResponseModel{
    QString sig_api;
    QString session_id;
    vmStateEnums state;
    QDateTime timestamp;
};

//Create
struct CreateVMRequestModel{
    QString sig_api;
    QString session_id;
    QString action;
    QString hostname;
    QString osImage;
    QString memory;
    int cpu;
    QString ports;
    QDateTime timestamp;
};

struct VMInfoModel{
    bool kvm;
    QString hostId;
    QString containerId;
    vmStateEnums state;
    QString link;
};

struct CreateVMResponseModel{
    QString sig_api;
    QString session_id;
    VMInfoModel vm;
    QDateTime timestamp;
};
//base vm model
enum class HostActionsEnum { HostActivate,HostDeactivate, HostDelete, HostRemove};


struct MemoryInfoModel{
    int memAvailable;
    int memFree;
    int memTotal;
};
struct DiskInfoModel{
    //system mount-point
    int free;
    double percentage;
    int total;
    int used;
    //
    int localStorageMb;
};
struct HostInfo{
    CpuInfoModel cpu;
    MemoryInfoModel memory;
    DiskInfoModel disk;
    QString os;

};
struct HostModel{
   QString name;
   QString state;
   QString accountId;
   HostInfo info;
   QList<HostActionsEnum> actions;

};
struct ContainerModel{
    QString id;
    QString name;
    QString state;
    QList<ContainerActionsEnum> actions;
};
struct StatisticModel{
    QString id;
    QString name;
    QString state;
};
struct BaseVmModel{
    QList<HostModel> hosts;
    QList<ContainerModel> containers;
    QList<StatisticModel> stat;
};
#endif // NODEMODEL_H
