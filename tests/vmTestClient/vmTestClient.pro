#-------------------------------------------------
#
# Project created by QtCreator 2018-05-03T19:49:28
#
#-------------------------------------------------

QT += core gui


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_RPATHDIR += /usr/local/lib

TARGET = vmTestClient
TEMPLATE = app

CONFIG +=   c++11

CONFIG(debug, release|debug):DEFINES += _DEBUG
# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH+= ../../include/ \



SOURCES += \
        main.cpp \
        mainwindow.cpp \
    client/vmclient.cpp \
    ../../network/vmserver/vmserver.grpc.pb.cc \
    ../../network/vmserver/vmserver.pb.cc \
#    ../../network/vmserver/sigvmserver.cpp \
#    ../../network/vmserver/sigvmserverimpl.cpp \
    ../../network/vmserver/vmserver.grpc.pb.cc \
    ../../network/vmserver/vmserver.pb.cc \



HEADERS += \
        mainwindow.h \
    client/vmclient.h \
    ../../include/google/protobuf/compiler/cpp/cpp_generator.h \
    ../../include/google/protobuf/compiler/csharp/csharp_generator.h \
    ../../include/google/protobuf/compiler/csharp/csharp_names.h \
    ../../include/google/protobuf/compiler/java/java_generator.h \
    ../../include/google/protobuf/compiler/java/java_names.h \
    ../../include/google/protobuf/compiler/javanano/javanano_generator.h \
    ../../include/google/protobuf/compiler/js/js_generator.h \
    ../../include/google/protobuf/compiler/js/well_known_types_embed.h \
    ../../include/google/protobuf/compiler/objectivec/objectivec_generator.h \
    ../../include/google/protobuf/compiler/objectivec/objectivec_helpers.h \
    ../../include/google/protobuf/compiler/php/php_generator.h \
    ../../include/google/protobuf/compiler/python/python_generator.h \
    ../../include/google/protobuf/compiler/ruby/ruby_generator.h \
    ../../include/google/protobuf/compiler/code_generator.h \
    ../../include/google/protobuf/compiler/command_line_interface.h \
    ../../include/google/protobuf/compiler/importer.h \
    ../../include/google/protobuf/compiler/parser.h \
    ../../include/google/protobuf/compiler/plugin.h \
    ../../include/google/protobuf/compiler/plugin.pb.h \
    ../../include/google/protobuf/io/coded_stream.h \
    ../../include/google/protobuf/io/gzip_stream.h \
    ../../include/google/protobuf/io/printer.h \
    ../../include/google/protobuf/io/strtod.h \
    ../../include/google/protobuf/io/tokenizer.h \
    ../../include/google/protobuf/io/zero_copy_stream.h \
    ../../include/google/protobuf/io/zero_copy_stream_impl.h \
    ../../include/google/protobuf/io/zero_copy_stream_impl_lite.h \
    ../../include/google/protobuf/stubs/atomic_sequence_num.h \
    ../../include/google/protobuf/stubs/atomicops.h \
    ../../include/google/protobuf/stubs/atomicops_internals_arm64_gcc.h \
    ../../include/google/protobuf/stubs/atomicops_internals_arm_gcc.h \
    ../../include/google/protobuf/stubs/atomicops_internals_arm_qnx.h \
    ../../include/google/protobuf/stubs/atomicops_internals_generic_c11_atomic.h \
    ../../include/google/protobuf/stubs/atomicops_internals_generic_gcc.h \
    ../../include/google/protobuf/stubs/atomicops_internals_mips_gcc.h \
    ../../include/google/protobuf/stubs/atomicops_internals_power.h \
    ../../include/google/protobuf/stubs/atomicops_internals_ppc_gcc.h \
    ../../include/google/protobuf/stubs/atomicops_internals_solaris.h \
    ../../include/google/protobuf/stubs/atomicops_internals_tsan.h \
    ../../include/google/protobuf/stubs/atomicops_internals_x86_gcc.h \
    ../../include/google/protobuf/stubs/atomicops_internals_x86_msvc.h \
    ../../include/google/protobuf/stubs/bytestream.h \
    ../../include/google/protobuf/stubs/callback.h \
    ../../include/google/protobuf/stubs/casts.h \
    ../../include/google/protobuf/stubs/common.h \
    ../../include/google/protobuf/stubs/fastmem.h \
    ../../include/google/protobuf/stubs/hash.h \
    ../../include/google/protobuf/stubs/logging.h \
    ../../include/google/protobuf/stubs/macros.h \
    ../../include/google/protobuf/stubs/mutex.h \
    ../../include/google/protobuf/stubs/once.h \
    ../../include/google/protobuf/stubs/platform_macros.h \
    ../../include/google/protobuf/stubs/port.h \
    ../../include/google/protobuf/stubs/scoped_ptr.h \
    ../../include/google/protobuf/stubs/shared_ptr.h \
    ../../include/google/protobuf/stubs/singleton.h \
    ../../include/google/protobuf/stubs/status.h \
    ../../include/google/protobuf/stubs/stl_util.h \
    ../../include/google/protobuf/stubs/stringpiece.h \
    ../../include/google/protobuf/stubs/template_util.h \
    ../../include/google/protobuf/stubs/type_traits.h \
    ../../include/google/protobuf/util/delimited_message_util.h \
    ../../include/google/protobuf/util/field_comparator.h \
    ../../include/google/protobuf/util/field_mask_util.h \
    ../../include/google/protobuf/util/json_util.h \
    ../../include/google/protobuf/util/message_differencer.h \
    ../../include/google/protobuf/util/time_util.h \
    ../../include/google/protobuf/util/type_resolver.h \
    ../../include/google/protobuf/util/type_resolver_util.h \
    ../../include/google/protobuf/any.h \
    ../../include/google/protobuf/any.pb.h \
    ../../include/google/protobuf/api.pb.h \
    ../../include/google/protobuf/arena.h \
    ../../include/google/protobuf/arena_impl.h \
    ../../include/google/protobuf/arenastring.h \
    ../../include/google/protobuf/descriptor.h \
    ../../include/google/protobuf/descriptor.pb.h \
    ../../include/google/protobuf/descriptor_database.h \
    ../../include/google/protobuf/duration.pb.h \
    ../../include/google/protobuf/dynamic_message.h \
    ../../include/google/protobuf/empty.pb.h \
    ../../include/google/protobuf/extension_set.h \
    ../../include/google/protobuf/field_mask.pb.h \
    ../../include/google/protobuf/generated_enum_reflection.h \
    ../../include/google/protobuf/generated_enum_util.h \
    ../../include/google/protobuf/generated_message_reflection.h \
    ../../include/google/protobuf/generated_message_table_driven.h \
    ../../include/google/protobuf/generated_message_util.h \
    ../../include/google/protobuf/has_bits.h \
    ../../include/google/protobuf/map.h \
    ../../include/google/protobuf/map_entry.h \
    ../../include/google/protobuf/map_entry_lite.h \
    ../../include/google/protobuf/map_field.h \
    ../../include/google/protobuf/map_field_inl.h \
    ../../include/google/protobuf/map_field_lite.h \
    ../../include/google/protobuf/map_type_handler.h \
    ../../include/google/protobuf/message.h \
    ../../include/google/protobuf/message_lite.h \
    ../../include/google/protobuf/metadata.h \
    ../../include/google/protobuf/metadata_lite.h \
    ../../include/google/protobuf/reflection.h \
    ../../include/google/protobuf/reflection_ops.h \
    ../../include/google/protobuf/repeated_field.h \
    ../../include/google/protobuf/service.h \
    ../../include/google/protobuf/source_context.pb.h \
    ../../include/google/protobuf/struct.pb.h \
    ../../include/google/protobuf/text_format.h \
    ../../include/google/protobuf/timestamp.pb.h \
    ../../include/google/protobuf/type.pb.h \
    ../../include/google/protobuf/unknown_field_set.h \
    ../../include/google/protobuf/wire_format.h \
    ../../include/google/protobuf/wire_format_lite.h \
    ../../include/google/protobuf/wire_format_lite_inl.h \
    ../../include/google/protobuf/wrappers.pb.h \
    ../../include/grpc/impl/codegen/atm.h \
    ../../include/grpc/impl/codegen/atm_gcc_atomic.h \
    ../../include/grpc/impl/codegen/atm_gcc_sync.h \
    ../../include/grpc/impl/codegen/atm_windows.h \
    ../../include/grpc/impl/codegen/byte_buffer.h \
    ../../include/grpc/impl/codegen/byte_buffer_reader.h \
    ../../include/grpc/impl/codegen/compression_types.h \
    ../../include/grpc/impl/codegen/connectivity_state.h \
    ../../include/grpc/impl/codegen/fork.h \
    ../../include/grpc/impl/codegen/gpr_slice.h \
    ../../include/grpc/impl/codegen/gpr_types.h \
    ../../include/grpc/impl/codegen/grpc_types.h \
    ../../include/grpc/impl/codegen/port_platform.h \
    ../../include/grpc/impl/codegen/propagation_bits.h \
    ../../include/grpc/impl/codegen/slice.h \
    ../../include/grpc/impl/codegen/status.h \
    ../../include/grpc/impl/codegen/sync.h \
    ../../include/grpc/impl/codegen/sync_custom.h \
    ../../include/grpc/impl/codegen/sync_generic.h \
    ../../include/grpc/impl/codegen/sync_posix.h \
    ../../include/grpc/impl/codegen/sync_windows.h \
    ../../include/grpc/support/alloc.h \
    ../../include/grpc/support/atm.h \
    ../../include/grpc/support/atm_gcc_atomic.h \
    ../../include/grpc/support/atm_gcc_sync.h \
    ../../include/grpc/support/atm_windows.h \
    ../../include/grpc/support/cpu.h \
    ../../include/grpc/support/log.h \
    ../../include/grpc/support/log_windows.h \
    ../../include/grpc/support/port_platform.h \
    ../../include/grpc/support/string_util.h \
    ../../include/grpc/support/sync.h \
    ../../include/grpc/support/sync_custom.h \
    ../../include/grpc/support/sync_generic.h \
    ../../include/grpc/support/sync_posix.h \
    ../../include/grpc/support/sync_windows.h \
    ../../include/grpc/support/thd_id.h \
    ../../include/grpc/support/time.h \
    ../../include/grpc/support/workaround_list.h \
    ../../include/grpc/byte_buffer.h \
    ../../include/grpc/byte_buffer_reader.h \
    ../../include/grpc/census.h \
    ../../include/grpc/compression.h \
    ../../include/grpc/fork.h \
    ../../include/grpc/grpc.h \
    ../../include/grpc/grpc_cronet.h \
    ../../include/grpc/grpc_posix.h \
    ../../include/grpc/grpc_security.h \
    ../../include/grpc/grpc_security_constants.h \
    ../../include/grpc/load_reporting.h \
    ../../include/grpc/slice.h \
    ../../include/grpc/slice_buffer.h \
    ../../include/grpc/status.h \
    ../../include/grpc++/ext/health_check_service_server_builder_option.h \
    ../../include/grpc++/ext/proto_server_reflection_plugin.h \
    ../../include/grpc++/generic/async_generic_service.h \
    ../../include/grpc++/generic/generic_stub.h \
    ../../include/grpc++/impl/codegen/security/auth_context.h \
    ../../include/grpc++/impl/codegen/async_stream.h \
    ../../include/grpc++/impl/codegen/async_unary_call.h \
    ../../include/grpc++/impl/codegen/byte_buffer.h \
    ../../include/grpc++/impl/codegen/call.h \
    ../../include/grpc++/impl/codegen/call_hook.h \
    ../../include/grpc++/impl/codegen/channel_interface.h \
    ../../include/grpc++/impl/codegen/client_context.h \
    ../../include/grpc++/impl/codegen/client_unary_call.h \
    ../../include/grpc++/impl/codegen/completion_queue.h \
    ../../include/grpc++/impl/codegen/completion_queue_tag.h \
    ../../include/grpc++/impl/codegen/config.h \
    ../../include/grpc++/impl/codegen/config_protobuf.h \
    ../../include/grpc++/impl/codegen/core_codegen.h \
    ../../include/grpc++/impl/codegen/core_codegen_interface.h \
    ../../include/grpc++/impl/codegen/create_auth_context.h \
    ../../include/grpc++/impl/codegen/grpc_library.h \
    ../../include/grpc++/impl/codegen/metadata_map.h \
    ../../include/grpc++/impl/codegen/method_handler_impl.h \
    ../../include/grpc++/impl/codegen/proto_utils.h \
    ../../include/grpc++/impl/codegen/rpc_method.h \
    ../../include/grpc++/impl/codegen/rpc_service_method.h \
    ../../include/grpc++/impl/codegen/serialization_traits.h \
    ../../include/grpc++/impl/codegen/server_context.h \
    ../../include/grpc++/impl/codegen/server_interface.h \
    ../../include/grpc++/impl/codegen/service_type.h \
    ../../include/grpc++/impl/codegen/slice.h \
    ../../include/grpc++/impl/codegen/status.h \
    ../../include/grpc++/impl/codegen/status_code_enum.h \
    ../../include/grpc++/impl/codegen/string_ref.h \
    ../../include/grpc++/impl/codegen/stub_options.h \
    ../../include/grpc++/impl/codegen/sync_stream.h \
    ../../include/grpc++/impl/codegen/time.h \
    ../../include/grpc++/impl/call.h \
    ../../include/grpc++/impl/channel_argument_option.h \
    ../../include/grpc++/impl/client_unary_call.h \
    ../../include/grpc++/impl/grpc_library.h \
    ../../include/grpc++/impl/method_handler_impl.h \
    ../../include/grpc++/impl/rpc_method.h \
    ../../include/grpc++/impl/rpc_service_method.h \
    ../../include/grpc++/impl/serialization_traits.h \
    ../../include/grpc++/impl/server_builder_option.h \
    ../../include/grpc++/impl/server_builder_plugin.h \
    ../../include/grpc++/impl/server_initializer.h \
    ../../include/grpc++/impl/service_type.h \
    ../../include/grpc++/impl/sync_cxx11.h \
    ../../include/grpc++/impl/sync_no_cxx11.h \
    ../../include/grpc++/security/auth_context.h \
    ../../include/grpc++/security/auth_metadata_processor.h \
    ../../include/grpc++/security/credentials.h \
    ../../include/grpc++/security/server_credentials.h \
    ../../include/grpc++/support/async_stream.h \
    ../../include/grpc++/support/async_unary_call.h \
    ../../include/grpc++/support/byte_buffer.h \
    ../../include/grpc++/support/channel_arguments.h \
    ../../include/grpc++/support/config.h \
    ../../include/grpc++/support/error_details.h \
    ../../include/grpc++/support/slice.h \
    ../../include/grpc++/support/status.h \
    ../../include/grpc++/support/status_code_enum.h \
    ../../include/grpc++/support/string_ref.h \
    ../../include/grpc++/support/stub_options.h \
    ../../include/grpc++/support/sync_stream.h \
    ../../include/grpc++/support/time.h \
    ../../include/grpc++/test/mock_stream.h \
    ../../include/grpc++/test/server_context_test_spouse.h \
    ../../include/grpc++/alarm.h \
    ../../include/grpc++/channel.h \
    ../../include/grpc++/client_context.h \
    ../../include/grpc++/completion_queue.h \
    ../../include/grpc++/create_channel.h \
    ../../include/grpc++/create_channel_posix.h \
    ../../include/grpc++/grpc++.h \
    ../../include/grpc++/health_check_service_interface.h \
    ../../include/grpc++/resource_quota.h \
    ../../include/grpc++/server.h \
    ../../include/grpc++/server_builder.h \
    ../../include/grpc++/server_context.h \
    ../../include/grpc++/server_posix.h \
    ../../include/grpcpp/ext/health_check_service_server_builder_option.h \
    ../../include/grpcpp/ext/proto_server_reflection_plugin.h \
    ../../include/grpcpp/generic/async_generic_service.h \
    ../../include/grpcpp/generic/generic_stub.h \
    ../../include/grpcpp/impl/codegen/security/auth_context.h \
    ../../include/grpcpp/impl/codegen/async_stream.h \
    ../../include/grpcpp/impl/codegen/async_unary_call.h \
    ../../include/grpcpp/impl/codegen/byte_buffer.h \
    ../../include/grpcpp/impl/codegen/call.h \
    ../../include/grpcpp/impl/codegen/call_hook.h \
    ../../include/grpcpp/impl/codegen/channel_interface.h \
    ../../include/grpcpp/impl/codegen/client_context.h \
    ../../include/grpcpp/impl/codegen/client_unary_call.h \
    ../../include/grpcpp/impl/codegen/completion_queue.h \
    ../../include/grpcpp/impl/codegen/completion_queue_tag.h \
    ../../include/grpcpp/impl/codegen/config.h \
    ../../include/grpcpp/impl/codegen/config_protobuf.h \
    ../../include/grpcpp/impl/codegen/core_codegen.h \
    ../../include/grpcpp/impl/codegen/core_codegen_interface.h \
    ../../include/grpcpp/impl/codegen/create_auth_context.h \
    ../../include/grpcpp/impl/codegen/grpc_library.h \
    ../../include/grpcpp/impl/codegen/metadata_map.h \
    ../../include/grpcpp/impl/codegen/method_handler_impl.h \
    ../../include/grpcpp/impl/codegen/proto_utils.h \
    ../../include/grpcpp/impl/codegen/rpc_method.h \
    ../../include/grpcpp/impl/codegen/rpc_service_method.h \
    ../../include/grpcpp/impl/codegen/serialization_traits.h \
    ../../include/grpcpp/impl/codegen/server_context.h \
    ../../include/grpcpp/impl/codegen/server_interface.h \
    ../../include/grpcpp/impl/codegen/service_type.h \
    ../../include/grpcpp/impl/codegen/slice.h \
    ../../include/grpcpp/impl/codegen/status.h \
    ../../include/grpcpp/impl/codegen/status_code_enum.h \
    ../../include/grpcpp/impl/codegen/string_ref.h \
    ../../include/grpcpp/impl/codegen/stub_options.h \
    ../../include/grpcpp/impl/codegen/sync_stream.h \
    ../../include/grpcpp/impl/codegen/time.h \
    ../../include/grpcpp/impl/call.h \
    ../../include/grpcpp/impl/channel_argument_option.h \
    ../../include/grpcpp/impl/client_unary_call.h \
    ../../include/grpcpp/impl/grpc_library.h \
    ../../include/grpcpp/impl/method_handler_impl.h \
    ../../include/grpcpp/impl/rpc_method.h \
    ../../include/grpcpp/impl/rpc_service_method.h \
    ../../include/grpcpp/impl/serialization_traits.h \
    ../../include/grpcpp/impl/server_builder_option.h \
    ../../include/grpcpp/impl/server_builder_plugin.h \
    ../../include/grpcpp/impl/server_initializer.h \
    ../../include/grpcpp/impl/service_type.h \
    ../../include/grpcpp/impl/sync_cxx11.h \
    ../../include/grpcpp/impl/sync_no_cxx11.h \
    ../../include/grpcpp/security/auth_context.h \
    ../../include/grpcpp/security/auth_metadata_processor.h \
    ../../include/grpcpp/security/credentials.h \
    ../../include/grpcpp/security/server_credentials.h \
    ../../include/grpcpp/support/async_stream.h \
    ../../include/grpcpp/support/async_unary_call.h \
    ../../include/grpcpp/support/byte_buffer.h \
    ../../include/grpcpp/support/channel_arguments.h \
    ../../include/grpcpp/support/config.h \
    ../../include/grpcpp/support/error_details.h \
    ../../include/grpcpp/support/slice.h \
    ../../include/grpcpp/support/status.h \
    ../../include/grpcpp/support/status_code_enum.h \
    ../../include/grpcpp/support/string_ref.h \
    ../../include/grpcpp/support/stub_options.h \
    ../../include/grpcpp/support/sync_stream.h \
    ../../include/grpcpp/support/time.h \
    ../../include/grpcpp/test/mock_stream.h \
    ../../include/grpcpp/test/server_context_test_spouse.h \
    ../../include/grpcpp/alarm.h \
    ../../include/grpcpp/channel.h \
    ../../include/grpcpp/client_context.h \
    ../../include/grpcpp/completion_queue.h \
    ../../include/grpcpp/create_channel.h \
    ../../include/grpcpp/create_channel_posix.h \
    ../../include/grpcpp/grpcpp.h \
    ../../include/grpcpp/health_check_service_interface.h \
    ../../include/grpcpp/resource_quota.h \
    ../../include/grpcpp/server.h \
    ../../include/grpcpp/server_builder.h \
    ../../include/grpcpp/server_context.h \
    ../../include/grpcpp/server_posix.h \
#    ../../network/vmserver/sigvmserver.h \
#    ../../network/vmserver/sigvmserverimpl.h \
    ../../network/vmserver/vmserver.grpc.pb.h \
    ../../network/vmserver/vmserver.pb.h \
    default.h \
#    ../../network/vmserver/sigvmserver.h \
#    ../../network/vmserver/sigvmserverimpl.h \
    ../../network/vmserver/vmserver.grpc.pb.h \
    ../../network/vmserver/vmserver.pb.h \



FORMS += \
        mainwindow.ui

DISTFILES += \
    ../../include/grpcpp/impl/README.md \
    ../../include/grpc/module.modulemap \
    ../../include/google/protobuf/compiler/plugin.proto \
    ../../include/google/protobuf/any.proto \
    ../../include/google/protobuf/api.proto \
    ../../include/google/protobuf/descriptor.proto \
    ../../include/google/protobuf/duration.proto \
    ../../include/google/protobuf/empty.proto \
    ../../include/google/protobuf/field_mask.proto \
    ../../include/google/protobuf/source_context.proto \
    ../../include/google/protobuf/struct.proto \
    ../../include/google/protobuf/timestamp.proto \
    ../../include/google/protobuf/type.proto \
    ../../include/google/protobuf/wrappers.proto

#Win32

win32: LIBS += -L$$PWD/../../lib/ -llibprotobuf

INCLUDEPATH += ../../include
DEPENDPATH += ../../include

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libprotobuf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/liblibprotobuf.a


win32: LIBS += -L$$PWD/../../lib/ -lgpr



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/gpr.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgpr.a

win32: LIBS += -L$$PWD/../../lib/ -lzlib



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/zlib.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libzlib.a

win32: LIBS += -L$$PWD/../../lib/ -lcares



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/cares.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libcares.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc_cronet



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc_cronet.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc_cronet.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc++_cronet



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc++_cronet.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc++_cronet.a

win32: LIBS += -L$$PWD/../../lib/ -lcrypto



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/crypto.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libcrypto.a

win32: LIBS += -L$$PWD/../../lib/ -lssl



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/ssl.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libssl.a

win32: LIBS += -L$$PWD/../../lib/ -lrand_extra



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/rand_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/librand_extra.a

win32: LIBS += -L$$PWD/../../lib/ -lbn_extra


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/bn_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libbn_extra.a

win32: LIBS += -L$$PWD/../../lib/ -lchacha


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/chacha.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libchacha.a

win32: LIBS += -L$$PWD/../../lib/ -lcurve25519



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/curve25519.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libcurve25519.a

win32: LIBS += -L$$PWD/../../lib/ -lpem



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/pem.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libpem.a

win32: LIBS += -L$$PWD/../../lib/ -lbase64



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/base64.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libbase64.a

win32: LIBS += -L$$PWD/../../lib/ -lcmac



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/cmac.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libcmac.a

win32: LIBS += -L$$PWD/../../lib/ -lcrypto_base


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/crypto_base.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libcrypto_base.a

win32: LIBS += -L$$PWD/../../lib/ -lbuf



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/buf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libbuf.a

win32: LIBS += -L$$PWD/../../lib/ -lpkcs7



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/pkcs7.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libpkcs7.a

win32: LIBS += -L$$PWD/../../lib/ -lx509


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/x509.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libx509.a

win32: LIBS += -L$$PWD/../../lib/ -lengine


win32: LIBS += -L$$PWD/../../lib/ -lbytestring



win32: LIBS += -L$$PWD/../../lib/ -ldigest_extra



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/digest_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libdigest_extra.a

win32: LIBS += -L$$PWD/../../lib/ -lpoly1305



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/poly1305.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libpoly1305.a

win32: LIBS += -L$$PWD/../../lib/ -lobj



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/obj.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libobj.a

win32: LIBS += -L$$PWD/../../lib/ -lec_extra



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/ec_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libec_extra.a

win32: LIBS += -L$$PWD/../../lib/ -lpkcs8_lib


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/pkcs8_lib.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libpkcs8_lib.a

win32: LIBS += -L$$PWD/../../lib/ -lrc4


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/rc4.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/librc4.a

win32: LIBS += -L$$PWD/../../lib/ -lerr



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/err.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/liberr.a

win32: LIBS += -L$$PWD/../../lib/ -lecdsa_extra



win32: LIBS += -L$$PWD/../../lib/ -levp



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/evp.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libevp.a


win32: LIBS += -L$$PWD/../../lib/ -ldsa



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/dsa.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libdsa.a

win32: LIBS += -L$$PWD/../../lib/ -lpool



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/pool.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libpool.a

win32: LIBS += -L$$PWD/../../lib/ -ldh



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/dh.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libdh.a

win32: LIBS += -L$$PWD/../../lib/ -lecdh


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/ecdh.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libecdh.a

win32: LIBS += -L$$PWD/../../lib/ -lbio



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/bio.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libbio.a

win32: LIBS += -L$$PWD/../../lib/ -lhkdf


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/hkdf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libhkdf.a

win32: LIBS += -L$$PWD/../../lib/ -lfipsmodule



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/fipsmodule.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libfipsmodule.a

win32: LIBS += -L$$PWD/../../lib/ -lconf



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/conf.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libconf.a

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../lib/ -lrsa_extra
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../lib/ -lrsa_extra



win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/librsa_extra.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/librsa_extra.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/rsa_extra.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../../lib/rsa_extra.lib

win32: LIBS += -L$$PWD/../../lib/ -llhash



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/lhash.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/liblhash.a

win32: LIBS += -L$$PWD/../../lib/ -lx509v3


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/x509v3.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libx509v3.a

win32: LIBS += -L$$PWD/../../lib/ -lstack



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/stack.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libstack.a

win32: LIBS += -L$$PWD/../../lib/ -lcipher_extra



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/cipher_extra.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libcipher_extra.a

win32: LIBS += -L$$PWD/../../lib/ -lasn1


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/asn1.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libasn1.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc++


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc++.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc++.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc.a


win32: LIBS += -L$$PWD/../../lib/ -lgrpc_unsecure



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc_unsecure.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc_unsecure.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc++_unsecure



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc++_unsecure.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc++_unsecure.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc++_error_details


win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc++_error_details.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc++_error_details.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc++_reflection



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc++_reflection.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc++_reflection.a

win32: LIBS += -L$$PWD/../../lib/ -lgrpc_plugin_support



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/grpc_plugin_support.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libgrpc_plugin_support.a

win32: LIBS += -L$$PWD/../../lib/ -lAdvAPI32



win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/AdvAPI32.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../lib/libAdvAPI32.a

unix:!macx|win32: LIBS += -L$$PWD/../../lib/ -lAdvAPI32

