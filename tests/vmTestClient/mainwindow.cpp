#include "mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    client= new VMClient(CreateChannel("localhost:6060",grpc::InsecureChannelCredentials()));
}


MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_clicked()
{

    NewSessionRequestModel *req_model= new NewSessionRequestModel();
    req_model->sig_api="1.0";
    req_model->timestamp=QDateTime::currentDateTime();//.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    req_model->blockchain.destination_id="a92c52a99f70f3680f8867731a8027516e06e7e05b9cc17bef7ebb";
    req_model->blockchain.source_id="f954d198370b046b5d54a474e157cfcca3ae24f03db66b2f954dfe";
    qDebug()<<req_model->blockchain.destination_id;
   auto msg= client->NewSession(*req_model);

   qDebug()<< msg;
   QString response=msg;
   ui->textBrowser->clear();
   ui->textBrowser->setText(response);


}
