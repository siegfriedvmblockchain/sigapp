#ifndef VMCLIENT_H
#define VMCLIENT_H
#include <default.h>
#include <../../model/nodemodel.h>
#include <QDebug>
using grpc::Channel;
using namespace std;
using namespace grpc;
using namespace vmserverapi;
class VMClient
{
public:
    VMClient(std::shared_ptr<Channel> channel);
    QString NewSession( NewSessionRequestModel model);
private:
    unique_ptr<vmserverapiSrv::Stub> stub_;
};

#endif // VMCLIENT_H
