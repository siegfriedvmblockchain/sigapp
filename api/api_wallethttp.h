#ifndef API_WALLETHTTP_H
#define API_WALLETHTTP_H
#include <network/sighttp.h>
#include <command.h>
#include <QUrl>
#include <QObject>

class WalletHttpAPI : public QObject
{
    Q_OBJECT
public:
    explicit WalletHttpAPI(QObject *parent = nullptr,SIGConfig *cfg = nullptr);
    ~WalletHttpAPI();
    SIGHttpClient *signet;

    void Ballance();
    void Unlock(QString passphrase,int timeout);
    void NewAddress();
    void SendTo(QString address, double amount);
    void StakeInfo();

private:
    QNetworkRequest request;
    SIGConfig *config;


    Command *command;
signals:
     void requestError(int error_code,QString *message);
     void requestResult(BalanceModel* data);
     void requestResult(SendToModel* data);
     void requestResult(NewAddressModel* data);
     void requestResult(StakeInfoModel* data);
     void requestResult(ErrorModel* data);


public slots:
    void  HttpResult(QString* data);
    void  HttpError(int code,QString *message);
    void onCommandChanged(Command::WalletCommandsEnum cmd);

};

#endif // API_WALLETHTTP_H
