#ifndef API_RANCHER_H
#define API_RANCHER_H
#include "network/sighttp.h"
#include "network/sigws.h"
#include <QObject>
//Enums
enum RancherAPIEnum{Projects,Project,Containers,Hosts,Services,Container,Host,Service,ContainerStats};
class RancherAPI : public QObject
{
    Q_OBJECT
public:
    explicit RancherAPI(QObject *parent = nullptr,SIGConfig *cfg = nullptr);
    ~RancherAPI();

    //ws
     void WsConnect(QUrl url,QString token="");

    // http

    void GET(QUrl url,QNetworkRequest &request);
    void POST(QNetworkRequest &request, QByteArray &data);
    void POST(QNetworkRequest &request, QIODevice *data);
    void POST(QNetworkRequest &request, QHttpMultiPart *data);
    //API
    void GetContainerStats(QString projectId,QString containerId);
    void GetContainer(QString projectId,QString containerId);
    void GetContainers(QString projectId);
    void GetServices(QString projectId);
    void GetService(QString projectId,QString serviceId);
    void GetHosts(QString projectId);
    void GetHost(QString projectId,QString hostId);
    void GetProject(QString projectId);
    void GetProjects();

    SIGHttpClient *sighttp;
    SIGWsClient *sigws;
private:
    QUrl GetCommandUrl(RancherAPIEnum stats,QString projectId="",QString containerId="");
    RancherAPIEnum CurrentCmd;
signals:
      void Response(ResponseTypeEnum type, RancherAPIEnum api,QString *data);
      void ApiError(int code,QString *message);
      void WSStatusUpdate(WSStatusEnum status);
public slots:
   void WSStatus(WSStatusEnum status);
   void WSResponse(QString *response);
   void HttpResponse(QString *data);
   void HttpError(int code,QString *message);
};

#endif // API_RANCHER_H
