#include "api_rancher.h"
#include <vm_commands_masks.h>
RancherAPI::RancherAPI(QObject *parent, SIGConfig *cfg) : QObject(parent)
{
    sighttp= new SIGHttpClient(this,cfg);
    connect(sighttp,SIGNAL(requestResult(QString*)),this,SLOT(HttpResponse(QString*)));
    connect(sighttp,SIGNAL(requestError(int,QString*)),this,SLOT(HttpError(int,QString*)));

    sigws = new SIGWsClient(this);

    connect(sigws,SIGNAL(Status(WSStatusEnum)),this,SLOT(WSStatus(WSStatusEnum)));
    connect(sigws,SIGNAL(Response(QString*)),this,SLOT(WSResponse(QString*)));
}
void RancherAPI::GetProjects(){
    auto url=this->GetCommandUrl(RancherAPIEnum::Projects);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GetProject(QString projectId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Project,projectId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GetHosts(QString projectId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Hosts,projectId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}

void RancherAPI::GetHost(QString projectId,QString hostId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Hosts,projectId,hostId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}

void RancherAPI::GetServices(QString projectId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Services,projectId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GetService(QString projectId,QString serviceId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Services,projectId,serviceId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GetContainers(QString projectId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Containers,projectId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GetContainer(QString projectId,QString containerId){
    auto url=this->GetCommandUrl(RancherAPIEnum::Container,projectId,containerId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GetContainerStats(QString projectId,QString containerId){
    auto url=this->GetCommandUrl(RancherAPIEnum::ContainerStats,projectId,containerId);
    QNetworkRequest request = QNetworkRequest(url);
    request.setHeader(QNetworkRequest::UserAgentHeader, QVariant("Mozilla/5.0"));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
    request.setRawHeader("Accept", "application/json");
    request.setRawHeader("Authorization", "Basic " + QByteArray(QString("%1:%2").arg(sighttp->config->RancherUser).arg(sighttp->config->RancherPass).toLocal8Bit()).toBase64());
    sighttp->SIGHttpGet(request);
}
void RancherAPI::GET(QUrl url,QNetworkRequest &request){
   sighttp->SIGHttpGet(request);
}
void RancherAPI::POST(QNetworkRequest &request, QByteArray &data){
    sighttp->SIGHttpPost(request,data);
}
//void RancherAPI::POST(QNetworkRequest &request, QIODevice *data){
//    sighttp->SIGHttpPost(request,data);
//}
void RancherAPI::POST(QNetworkRequest &request, QHttpMultiPart *data){
    sighttp->SIGHttpPost(request,data);
}

void RancherAPI::WsConnect(QUrl url,QString token){
    if(token.isEmpty()){
       sigws->WsConnect(url);
    }else
    {
     QString newUrl=QString("%1?token=%2").arg(url.toString()).arg(token);
          sigws->WsConnect(newUrl);

    }

}

void RancherAPI::HttpResponse(QString *data){
    //TODO:
    emit Response(ResponseTypeEnum::HTTP,CurrentCmd,data);
}
void RancherAPI::HttpError(int code, QString *message){
    //TODO:
    emit ApiError(code,message);
}

void RancherAPI::WSStatus(WSStatusEnum status){
//TODO:
    emit WSStatusUpdate(status);
}

void RancherAPI::WSResponse(QString *response){
   //TODO:
    emit Response(ResponseTypeEnum::WS,CurrentCmd,response);

}
QUrl RancherAPI::GetCommandUrl(RancherAPIEnum api,QString projectId,QString containerId){
    QUrl res;
    QString host=sighttp->config->RancherHost;
    QString port=sighttp->config->RancherPort;

    QString cmdUrl=QString("http://%1:%2/").arg(host).arg(port);
    switch (api) {
    case RancherAPIEnum::ContainerStats:
        cmdUrl= cmdUrl.append(ContainerStatsCmd);
        res=QUrl(cmdUrl.arg(projectId).arg(containerId));
        break;
    case RancherAPIEnum::Container:
        cmdUrl= cmdUrl.append(ContainerCmd);
        res=QUrl(cmdUrl.arg(projectId).arg(containerId));
        break;
    case RancherAPIEnum::Containers:
        cmdUrl= cmdUrl.append(ContainersCmd);
        res=QUrl(cmdUrl.arg(projectId));
        break;
    case RancherAPIEnum::Services:
        cmdUrl= cmdUrl.append(ServicesCmd);
        res=QUrl(cmdUrl.arg(projectId));
        break;
    case RancherAPIEnum::Hosts:
        cmdUrl= cmdUrl.append(HostsCmd);
        res=QUrl(cmdUrl.arg(projectId));
        break;
    case RancherAPIEnum::Project:
        cmdUrl= cmdUrl.append(ProjectCmd);
        res=QUrl(cmdUrl.arg(projectId));
        break;
    case RancherAPIEnum::Projects:
        cmdUrl= cmdUrl.append(ProjectsCmd);
        res=QUrl(cmdUrl);
        break;
    default:
        break;
    }
    CurrentCmd=api;
    return res;
}
RancherAPI::~RancherAPI(){
sigws->~SIGWsClient();
}
