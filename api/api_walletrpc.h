#ifndef API_WALLETRPC_H
#define API_WALLETRPC_H
#include <grpc++/grpc++.h>
#include "widgets/Wallet/api/api.pb.h"
#include "widgets/Wallet/api/api.grpc.pb.h"
#include "network/sigrpc.h"

#include <QObject>

using namespace grpc;
class WalletRpcAPI : public QObject
{
    Q_OBJECT
public:
    explicit WalletRpcAPI(QObject *parent = nullptr);
    ~WalletRpcAPI();
    SIGRpcClient *rpcClient;

    QString GetBallance();

private:

    ClientContext* ctx;
signals:

public slots:

};

#endif // API_WALLETRPC_H
