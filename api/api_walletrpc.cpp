#include "api_walletrpc.h"

WalletRpcAPI::WalletRpcAPI(QObject *parent) : QObject(parent)
{
    //TODO: Move below functionality to Config class
    SIGConfig *rpc_config=new SIGConfig();
    rpc_config->TLS=true;
    rpc_config->Host="localhost:9111";
    //
    rpcClient = new SIGRpcClient(this,rpc_config);

   if (rpcClient->config->TLS){
       this->rpcClient->SetSSLChannelStub(rpcClient->config->Host);
   }
}

QString WalletRpcAPI::GetBallance()
{
    this->ctx=new ClientContext();
    BalanceRequest request{};
    request.set_account_number(0);
    request.set_required_confirmations(1);
    BalanceResponse response{};
    try{
    Status s = this->rpcClient->stub->Balance(this->ctx, request, &response);
    }catch(...){
        qDebug() <<"Error";
    }
    QString sm;
//    if (!s.ok()) {
//       sm=QString::fromStdString(s.error_message().c_str());
//        ui->lblBallance->setText(QString("Error:").append(sm));
//    }else{
//        sm=QString::fromStdString(response.ShortDebugString().c_str());
//        ui->lblBallance->setText(QString("OK:").append(sm));
        return sm;
}
WalletRpcAPI::~WalletRpcAPI(){
  //  this->rpcClient->~SIGRpcClient();
//
}
