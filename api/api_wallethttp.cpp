#include "api_wallethttp.h"

WalletHttpAPI::WalletHttpAPI(QObject *parent, SIGConfig *cfg) :
    QObject(parent),
    config(cfg)
{
    signet= new SIGHttpClient(this,config);

    connect(signet,SIGNAL(requestResult(QString*)),this,SLOT(HttpResult(QString*)));
    connect(signet,SIGNAL(requestError(int,QString*)),this,SLOT(HttpError(int,QString*)));

    command = new Command(this);

    //Init request

    QString dest;

    if(cfg->TLS){
        // Connection via HTTPS
        QFile certFile(cfg->NodeCfgPath.append("//rpc.cert"));
        certFile.open(QIODevice::ReadOnly);
        QSslCertificate cert(&certFile, QSsl::Pem);
        QSslSocket * sslSocket = new QSslSocket(this);
        sslSocket->addCaCertificate(cert);
        QSslConfiguration sslcfg = sslSocket->sslConfiguration();
        sslcfg.setProtocol(QSsl::TlsV1_2);

        sslSocket->setSslConfiguration(sslcfg);

        signet->SetSslCfg(sslcfg);
        QString url("https://");
        dest=url.append(config->WalletHost);

    }else{
        QString url("http://");
        dest=url.append(config->WalletHost);
    }

    request = QNetworkRequest(QUrl(dest));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,"application/json");
}
void WalletHttpAPI::StakeInfo(){

    auto req=command->Request(Command::WalletCommandsEnum::StakeInfo);

    signet->SIGHttpPost(request,req);

}
void WalletHttpAPI::SendTo(QString address, double amount){

    QList<QString> params;
    params.append(address);
    params.append(QString::number(amount));
    auto req=command->Request(Command::WalletCommandsEnum::SendTo);

    signet->SIGHttpPost(request,req);

}
void WalletHttpAPI::NewAddress(){

    auto req=command->Request(Command::WalletCommandsEnum::NewAddress);

    signet->SIGHttpPost(request,req);

}
void WalletHttpAPI::Unlock(QString passphrase,int timeout){

    QList<QString> params;
    params.append(passphrase);
    params.append(QString::number(timeout));

    auto req=command->Request(Command::WalletCommandsEnum::Unlock,params);

    signet->SIGHttpPost(request,req);

}
void WalletHttpAPI::Ballance(){


    auto req=command->Request(Command::WalletCommandsEnum::Balance);

    signet->SIGHttpPost(request,req);

}


void WalletHttpAPI::HttpResult(QString *result){
    QString resp =result->toStdString().c_str();
    if(!command->IsValidResponse(resp)){
        command->Current=Command::WalletCommandsEnum::Bulk;
    }

    switch(command->Current){
    case Command::WalletCommandsEnum::Balance:
    {
        BalanceModel balance = command->ParseBalanceResponse(resp);
        emit requestResult(&balance);
    }
        break;
    case Command::WalletCommandsEnum::SendTo:
    {
        SendToModel send = command->ParseSendToResponse(resp);
        emit requestResult(&send);
    }
        break;
    case Command::WalletCommandsEnum::NewAddress:
    {
        NewAddressModel addr = command->ParseNewAddressResponse(resp);
        emit requestResult(&addr);
    }
        break;

    case Command::WalletCommandsEnum::StakeInfo:
    {
        StakeInfoModel stake = command->ParseStakeInfoResponse(resp);
        emit requestResult(&stake);
    }
        break;
    case Command::WalletCommandsEnum::Bulk:
    {
        ErrorModel err = command->ParseErrorResponse(resp);
        emit requestResult(&err);
    }
        break;
    }

}
void WalletHttpAPI::HttpError(int code,QString *message){
    qDebug()<<"WalletAPI.Error Code: "<< code <<":"<< &message;
    emit requestError(code,message);
}
void WalletHttpAPI::onCommandChanged(Command::WalletCommandsEnum cmd){
    command->Current=cmd;

}
WalletHttpAPI::~WalletHttpAPI()
{
    delete signet;
}
