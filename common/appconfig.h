#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <QObject>
#include <QVector>
#include <default.h>

class AppConfig : public QObject
{
    Q_OBJECT
public:
    explicit AppConfig(QObject *parent = nullptr);
    SIGConfig *Configuration;
    void Reload();
private:
    void ParseNodeConfig(QString path);
    QString GetPath(QString AppName);
    QString GetAppPath();

    QVector<QString> NodeParams=QVector<QString>()<<"rpcuser"<<"rpcpass";

signals:
    void ConfigurationError(ErrorMsgEnum error,QString *msg);
public slots:
};

#endif // APPCONFIG_H
