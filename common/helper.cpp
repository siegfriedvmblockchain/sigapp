#include "helper.h"
#include <QString>
#include <QFontDatabase>
#include <QWidget>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QRadioButton>
#include <QCheckBox>
#include <QLabel>
#include <QCommandLinkButton>
SIGHelper::SIGHelper()
{

}

void SIGHelper::SetFont(QWidget *obj){

    QString family = QFontDatabase::applicationFontFamilies(0).at(0);
    QFont font(family);

    for(int i = 0; i < obj->children().size(); i++)
    {
            QPushButton *pb = qobject_cast<QPushButton*>(obj->children().at(i));
            if(pb)
               pb->setFont(font);
            QPlainTextEdit *pte = qobject_cast<QPlainTextEdit*>(obj->children().at(i));
            if(pte)
               pte->setFont(font);
            QRadioButton *rb = qobject_cast<QRadioButton*>(obj->children().at(i));
            if(rb)
               rb->setFont(font);
            QCheckBox *cb = qobject_cast<QCheckBox*>(obj->children().at(i));
            if(cb)
               cb->setFont(font);
            QLabel *lb = qobject_cast<QLabel*>(obj->children().at(i));
            if(lb)
               lb->setFont(font);
            QCommandLinkButton *clb = qobject_cast<QCommandLinkButton*>(obj->children().at(i));
            if(clb)
               clb->setFont(font);

    }
}
