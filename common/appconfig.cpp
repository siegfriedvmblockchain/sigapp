#include "appconfig.h"
#include <QApplication>
#include <QStandardPaths>
#include <QDir>
#include <QTextStream>
#include <QSettings>
AppConfig::AppConfig(QObject *parent) : QObject(parent)
{
    Configuration= new SIGConfig();
    this->Reload();
}
QString AppConfig::GetAppPath(){
    QString res =QDir::toNativeSeparators(QApplication::applicationDirPath().append(QDir::separator()).append("app"));
    return res;
}
QString AppConfig::GetPath(QString AppName){
    QString curName=QApplication::applicationDisplayName();
    QString path = QStandardPaths::standardLocations(QStandardPaths::AppLocalDataLocation).at(0);
    path=QDir::toNativeSeparators(path.remove(curName).append(AppName).append(QDir::separator())/*.append("rpc.cert")*/);
    QDir dir(path);
    dir.setFilter( QDir::AllEntries | QDir::NoDotAndDotDot );
    if (dir.exists()){

        if(dir.count()!=0) return path;
    }else{
      return "";
    }
    return "";
}
//
void AppConfig::ParseNodeConfig(QString path){
    QStringList fields;
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly)) {
        QString *msg;
        QString text="Node configuration NOT found";
        emit ConfigurationError(ErrorMsgEnum::FatalError,&text);
    }

    QTextStream in(&file);

    while(!in.atEnd()) {
        QString line = in.readLine();
        if (!line.startsWith(';'))
        {
            fields=line.split("=");
            if (fields.length()!=2) {continue;}
            else{
                QString key=fields[0];
                QString val=fields[1];
                if(this->NodeParams.contains(key)){
                    if (key.contains("rpcuser"))
                            this->Configuration->User=val;
                    if (key.contains("rpcpass"))
                            this->Configuration->Pass=val;
                }
            }
        }

    }

    file.close();
}
void AppConfig::Reload(){
    auto apppath=  QCoreApplication::applicationDirPath() ;
    auto path=QDir::toNativeSeparators(apppath.append(QDir::separator()).append(configPath));
    qDebug()<<path;
     QSettings settings(path, QSettings::IniFormat);
     Configuration->Debug=settings.value("debug").toBool();
     Configuration->TLS=settings.value("tls").toBool();
     Configuration->IsTestNetwork=settings.value("testnetwork").toBool();
     Configuration->AppMinerMode=settings.value("miner").toBool();
     qDebug()<<QString("MinerMode: %1").arg(Configuration->AppMinerMode);
      settings.beginGroup("SIGWallet");
       Configuration->WalletHost=settings.value("host").toString();
       Configuration->WalletPort=settings.value("port").toString();
       Configuration->WalletUser=settings.value("user").toString();
       Configuration->WalletPass=settings.value("pass").toString();
      settings.endGroup();
      settings.beginGroup("VM");
       Configuration->RancherHost=settings.value("host").toString();
       Configuration->RancherPort=settings.value("port").toString();
       Configuration->RancherUser=settings.value("apikey").toString();
       Configuration->RancherPass=settings.value("apipass").toString();
      settings.endGroup();
      settings.beginGroup("SIGVM");
       Configuration->VMPort=settings.value("port").toString();
       Configuration->VMHost=settings.value("host").toString();
      settings.endGroup();
      Configuration->NodePath=this->GetPath(SIGAPP_NODE);
      Configuration->WalletPath=this->GetPath(SIGAPP_WALLET);
      Configuration->WalletCfgPath=this->GetPath(SIGAPP_WALLET);
      Configuration->NodeCfgPath=this->GetPath(SIGAPP_NODE);
      auto nodecfg=QString(SIGAPP_NODE).append(".conf");
      auto walletcfg=QString(SIGAPP_WALLET).append(".conf");
      if(Configuration->NodeCfgPath.count()!=0)
        Configuration->NodeCfgPath.append(nodecfg);
      if(Configuration->WalletCfgPath.count()!=0)
        Configuration->WalletCfgPath.append(walletcfg);
      Configuration->AppPath=GetAppPath();

      ParseNodeConfig(Configuration->NodeCfgPath);
      qDebug()<<Configuration->WalletPath;
}
