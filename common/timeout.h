#ifndef TIMEOUT_H
#define TIMEOUT_H
#include <QNetworkReply>
#include <QBasicTimer>
#include <QTimerEvent>
class ReplyTimeout : public QObject {
  Q_OBJECT
  enum HandleMethod { Abort, Close };
  QBasicTimer m_timer;
  HandleMethod m_method;
public:
explicit ReplyTimeout(QNetworkReply* reply, const int timeout, HandleMethod method = Abort);
    static void set(QNetworkReply* reply, const int timeout, HandleMethod method = Abort);

protected:
    void timerEvent(QTimerEvent * ev);

};
#endif // TIMEOUT_H
