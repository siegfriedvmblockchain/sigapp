#ifndef VMDATA_H
#define VMDATA_H

#include "common/rapidjson_wrapper.h"
#include "default.h"
#include <cstdio>
#include <string>
#include <vector>
#include <model/nodemodel.h>
#include <QObject>
#include <QObject>

class VmData : public QObject
{
    Q_OBJECT
public:

    explicit VmData(QObject *parent = nullptr);
     //
      NewSessionRequestModel  ParseNewSessionRequest(QString jsonrpc);
      NewSessionResponseModel ParseNewSessionResponse(QString jsonrpc);
      QString  CreateNewSessionRequestJson(NewSessionRequestModel model);
     //
      QString  CreateActionRequestJson(ActionVMRequestModel model);
      ActionVMResponseModel ParseActionResponse(QString jsonrpc);
     //
      QString CreateVMRequestJson(CreateVMRequestModel model);
      CreateVMResponseModel ParseCreateVMResponse(QString jsonrpc);
      //
      QString CreateOSImagesRequestJson(GetOSImageRequestModel model);
      GetOSImageResponseModel ParseOSImagesResponse(QString jsonrpc);
      //
      QString CreateVMListRequestRequestJson(GetVMListRequestModel model);
      GetVMListResponseModel ParseVMListResponseModel(QString jsonrpc);
      //
      QString CreateNodeStatusRequestJson(GetNodeStatusRequestModel model);
      GetNodeStatusResponseModel ParseNodeStatusResponseModel(QString jsonrpc);
      //

signals:

public slots:
};

#endif // VMDATA_H
