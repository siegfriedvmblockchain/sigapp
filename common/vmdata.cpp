#include "vmdata.h"
#include <QJsonArray>
#include <QJsonObject>
// Node to VM data serializing & parsing class
VmData::VmData(QObject *parent) : QObject(parent)
{

}
NewSessionResponseModel VmData::ParseNewSessionResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    NewSessionResponseModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.sig_api=d.value("sig_api").toString();
    auto dt=d.value("timestamp").toString();
    res.timestamp=QDateTime::fromString(dt,"yyyy-MM-ddThh:mm:ss.zzzZ");
    res.status=d.value("status").toString();
    res.destination_id=d["destination_id"].toString();
    res.session_id=d["session_id"].toString();
    //gets cpu info
    QJsonArray cpuInfo=d["cpuInfo"].toJsonArray();
    QJsonValue v=cpuInfo.at(0);
    res.cpuInfo.count=v["count"].toInt();
    res.cpuInfo.mhz=v["mhz"].toInt();
    res.cpuInfo.modelName=v["modelName"].toString();
    //gets storage info
    QJsonArray storgeInfo=d["storageInfo"].toJsonArray();
    v=storgeInfo.at(0);
    res.storageInfo.localStorageMb=v["localStorageMb"].toInt();
    res.storageInfo.memoryMb=v["memoryMb"].toInt();
    res.cpuInfo.modelName=v["modelName"].toString();
    //gets containers ids
    QJsonArray containers=d["containers"].toJsonArray();
    v=containers.at(0);
    res.containers.count=v["count"].toInt();

    QJsonArray ids= v["ids"].toArray();
    foreach(QJsonValue itm, ids)
    {
        QString id=itm.toString();
        res.containers.ids.append(id);
    }
    return res;
}
NewSessionRequestModel VmData::ParseNewSessionRequest(QString jsonrpc){

    NewSessionRequestModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());

    res.sig_api=d.value("sig_app").toString();

    QJsonObject blockchain=d["blockchain"].toJsonObject();

    res.blockchain.destination_id=blockchain.value("destination_id").toString();
    res.blockchain.source_id=blockchain.value("source_id").toString();
    return res;
}
QString VmData::CreateNewSessionRequestJson(NewSessionRequestModel model){
    rapidjson::Serialize s;
    s.startObject();
    qDebug()<<model.sig_api;
    s<<"sig_api"<<model.sig_api;
    s<<"timestamp"<<model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    s.startObject("blockchain");
    s<<"destination_id"<<model.blockchain.destination_id;
    s<<"source_id"<<model.blockchain.source_id;
    s.endObject();
    s.endObject();
    qDebug()<<"JSON:"<< s.getString();
    return s.getString();
}
//
QString VmData::CreateActionRequestJson(ActionVMRequestModel model){
    rapidjson::Serialize s;
    s.startObject();
    s<<"sig_api"<<model.sig_api;
    s<<"timestamp"<<model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    s<<"session_id"<<model.session_id;
    s<<"container_id"<<model.container_id;
    s<<"host_id"<<model.host_id;
    s<<"action"<<model.action;
    auto timeout = model.timeout;
    if( timeout!=0)
        s << "timeout" << timeout;
    s.endObject();
    qDebug()<<"JSON:"<< s.getString();
    return s.getString();
}
ActionVMResponseModel VmData::ParseActionResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    ActionVMResponseModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.sig_api=d.value("sig_api").toString();
    auto dt=d.value("timestamp").toString();
    res.timestamp=QDateTime::fromString(dt,"yyyy-MM-ddThh:mm:ss.zzzZ");
    res.session_id=d["session_id"].toString();
    res.state=static_cast<vmStateEnums>(d["state"].toInt());

    return res;

}
//
QString VmData::CreateVMRequestJson(CreateVMRequestModel model){
    rapidjson::Serialize s;
    s.startObject();
    s<<"sig_api"<<model.sig_api;
    s<<"timestamp"<<model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    s<<"action"<<model.action;
    s<<"hostname"<<model.hostname;
    s<<"osImage"<<model.osImage;
    s<<"memory"<<model.memory;
    s<<"cpu"<<model.cpu;
    s<<"ports"<<model.ports;
    s.endObject();
    qDebug()<<"JSON:"<< s.getString();
    return s.getString();
}
CreateVMResponseModel VmData::ParseCreateVMResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    CreateVMResponseModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.sig_api=d.value("sig_api").toString();
    auto dt=d.value("timestamp").toString();
    res.timestamp=QDateTime::fromString(dt,"yyyy-MM-ddThh:mm:ss.zzzZ");
    //gets vm info
    QJsonArray vmInfo=d["vm"].toJsonArray();
    QJsonValue v=vmInfo.at(0);
    res.vm.kvm=v["kvm"].toBool();
    res.vm.hostId=v["hostId"].toString();
    res.vm.containerId=v["containerId"].toString();
    res.vm.state=static_cast<vmStateEnums>(v["state"].toInt());;
    res.vm.link=v["link"].toString();

    return res;
    }
    //

QString VmData::CreateOSImagesRequestJson(GetOSImageRequestModel model){
rapidjson::Serialize s;
s.startObject();
s<<"sig_api"<<model.sig_api;
s<<"timestamp"<<model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
s<<"session_id"<<model.session_id;
s.endObject();
qDebug()<<"JSON:"<< s.getString();
return s.getString();
}


GetOSImageResponseModel VmData::ParseOSImagesResponse(QString jsonrpc){
    qDebug()<<jsonrpc;
    GetOSImageResponseModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.sig_api=d.value("sig_api").toString();
    auto dt=d.value("timestamp").toString();
    res.timestamp=QDateTime::fromString(dt,"yyyy-MM-ddThh:mm:ss.zzzZ");
    //gets vm info
    QJsonArray osImages=d["osImages"].toJsonArray();
    foreach(QJsonValue img, osImages)
    {
        OSImageInfo osm;
        osm.id=img["id"].toString();
        osm.name=img["name"].toString();
        osm.description=img["description"].toString();
        res.osImages.append(osm);
    }

    return res;
}
//
QString VmData::CreateVMListRequestRequestJson(GetVMListRequestModel model){
    rapidjson::Serialize s;
    s.startObject();
    s<<"sig_api"<<model.sig_api;
    s<<"timestamp"<<model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    s<<"session_id"<<model.session_id;
    s<<"host_id"<<model.host_id;
    s<<"session_id"<<model.container_id;
    s.endObject();
    qDebug()<<"JSON:"<< s.getString();
    return s.getString();
}
GetVMListResponseModel VmData::ParseVMListResponseModel(QString jsonrpc){
    qDebug()<<jsonrpc;
    GetVMListResponseModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.sig_api=d.value("sig_api").toString();
    auto dt=d.value("timestamp").toString();
    res.timestamp=QDateTime::fromString(dt,"yyyy-MM-ddThh:mm:ss.zzzZ");
    //gets hosts list info
    QJsonArray hosts=d["hosts"].toJsonArray();
    foreach(QJsonValue itm, hosts)
    {
        HostsVMList hv;
        hv.id=itm["id"].toString();

        foreach(QJsonValue ctr, itm.toArray())
        {
            ContainersVMList cv;
            cv.id=ctr["id"].toString();
            QList<ContainerActionsEnum> cae;
            foreach(QJsonValue ctraction, ctr["actions"].toArray())
            {
                ContainerActionsEnum rs=static_cast<ContainerActionsEnum>(ctraction.toInt());
                cae.append(rs);
            }
            cv.actions=cae;
            hv.containers.append(cv);
        }
        res.hosts.append(hv);
    }

    return res;
}
//
QString VmData::CreateNodeStatusRequestJson(GetNodeStatusRequestModel model){
    rapidjson::Serialize s;
    s.startObject();
    s<<"sig_api"<<model.sig_api;
    s<<"timestamp"<<model.timestamp.toString("yyyy-MM-ddThh:mm:ss.zzzZ");
    s<<"session_id"<<model.session_id;
    s.endObject();
    qDebug()<<"JSON:"<< s.getString();
    return s.getString();
}
GetNodeStatusResponseModel VmData::ParseNodeStatusResponseModel(QString jsonrpc){
    qDebug()<<jsonrpc;
    GetNodeStatusResponseModel res;
    rapidjson::Wrapper w;
    QVariantMap d=w.parse(jsonrpc.toUtf8());
    res.sig_api=d.value("sig_api").toString();
    auto dt=d.value("timestamp").toString();
    res.timestamp=QDateTime::fromString(dt,"yyyy-MM-ddThh:mm:ss.zzzZ");
    //gets projects info
    QJsonArray projects=d["projects"].toJsonArray();
    foreach(QJsonValue itm, projects)
    {
        res.projects.count=itm["count"].toInt();
        foreach(QJsonValue id, itm.toArray())
        {
            res.projects.ids.append(id.toString());
        }
    }
    //gets hosts info
    QJsonArray hosts=d["hosts"].toJsonArray();
    foreach(QJsonValue itm, hosts)
    {
        res.hosts.count=itm["count"].toInt();
        foreach(QJsonValue id, itm.toArray())
        {
            res.hosts.ids.append(id.toString());
        }
}
    //gets hosts info
    QJsonArray containers=d["containers"].toJsonArray();
    foreach(QJsonValue itm, containers)
    {
        res.containers.count=itm["count"].toInt();
        foreach(QJsonValue id, itm.toArray())
        {
            res.containers.ids.append(id.toString());
        }


    }
    return res;
}
//
